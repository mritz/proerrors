﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Threading;
using ErrorHandling.Data;
using ProErrors.Core.Model;
using ProErrors.Service.RcsTester.ProErrorsRcsService;

namespace ProErrors.Service.RcsTester
{
    class Program
    {
        private static DataRecord _dataRecord;
        private static ProErrorsRCSClient _proxy;

        private static void Main(string[] args)
        {
            string input = String.Empty;
            do
            {
                Console.Clear();
                Console.WriteLine("Choose an operation:");
                Console.Write("1. Create error");
                if (_dataRecord != null)
                    Console.WriteLine(": Ready to send");
                else
                    Console.WriteLine();
                Console.WriteLine("2. Request permission to send error");
                Console.WriteLine("3. Send error");
                Console.WriteLine("4. Request solution");
                Console.WriteLine("5. Send random error");
                Console.WriteLine("6. Create random error");
                Console.WriteLine("7. Create error with ExceptionData");
                Console.WriteLine("0. Exit");

                input = Console.ReadKey().KeyChar.ToString();

                Console.Clear();

                switch (input)
                {
                    case "1":
                        Console.Write("EFIN:");
                        var efin = Console.ReadLine();
                        Console.Write("Individual Version:");
                        var iVersion = Console.ReadLine();
                        Console.Write("Business Version:");
                        var bVersion = Console.ReadLine();
                        Console.Write("Program:");
                        var program = Console.ReadLine();
                        Console.Write("Error Number:");
                        var errNum = Console.ReadLine();
                        Console.Write("Line Number:");
                        var lineNum = Console.ReadLine();
                        Console.Write("Message:");
                        var message = Console.ReadLine();
                        Console.Write("Stack:");
                        var stack = Console.ReadLine();
                        Console.Write("Operating System:");
                        var opSys = Console.ReadLine();
                        Console.Write(".NET Version:");
                        var dotNetVer = Console.ReadLine();

                        try
                        {
                            _dataRecord = DataRecord.CreateDataRecord(Guid.NewGuid(), DateTime.Now,
                                efin, iVersion, bVersion, program, errNum, lineNum, message, stack, opSys,
                                Double.Parse(dotNetVer), null, String.Empty, String.Empty, String.Empty);
                            Console.WriteLine("Error created.");
                        }
                        catch (Exception ex)
                        {
                            if (_proxy != null)
                                _proxy.Abort();
                            Console.WriteLine("An error occurred.");
                            Console.WriteLine(ex.ToString());
                        }
                        PressAnyKeyToContinue();
                        break;

                    case "2":
                        try
                        {
                            using (_proxy = new ProErrorsRCSClient())
                            {
                                if (_proxy.CanSend(2012, _dataRecord))
                                    Console.WriteLine("You may send the error.");
                                else
                                    Console.WriteLine("You may not send the error.");
                            }
                        }
                        catch (Exception ex)
                        {
                            if (_proxy != null)
                                _proxy.Abort();
                            Console.WriteLine("An error occurred.");
                            Console.WriteLine(ex.ToString());
                        }
                        PressAnyKeyToContinue();
                        break;

                    case "3":
                        if (_dataRecord != null)
                        {
                            _dataRecord.ID = Guid.NewGuid();
                            try
                            {
                                using (_proxy = new ProErrorsRCSClient())
                                {
                                    _proxy.SendError(2012, _dataRecord);
                                }
                                Console.WriteLine("Error Sent.");
                            }
                            catch (Exception ex)
                            {
                                if (_proxy != null)
                                    _proxy.Abort();
                                Console.WriteLine("An error occurred.");
                                Console.WriteLine(ex.ToString());
                            }
                        }
                        else
                        {
                            Console.WriteLine("You must create an error first.");
                        }
                        PressAnyKeyToContinue();
                        break;

                    case "4":
                        if (_dataRecord != null)
                        {
                            try
                            {
                                using (_proxy = new ProErrorsRCSClient())
                                {
                                    string solution = _proxy.GetSolution(2012, _dataRecord);
                                    Console.WriteLine("Solution returned: ");
                                    Console.WriteLine();
                                    Console.WriteLine(solution);
                                }
                            }
                            catch (Exception ex)
                            {
                                if (_proxy != null)
                                    _proxy.Abort();
                                Console.WriteLine("An error occurred.");
                                Console.WriteLine(ex.ToString());
                            }
                        }
                        else
                        {
                            Console.WriteLine("You must create an error first.");
                        }
                        PressAnyKeyToContinue();
                        break;

                    case "5":
                        try
                        {
                            var dataRecord = DataRecord.CreateDataRecord(Guid.NewGuid(), DateTime.Now,
                                                                         "000001", "12.0", "12/31/12", "TEST Program",
                                                                         "123456", "654321", "TEST Message",
                                                                         "TEST Stack", "Windows",
                                                                         4.0, null, String.Empty, String.Empty,
                                                                         String.Empty);
                            using (_proxy = new ProErrorsRCSClient())
                            {
                                _proxy.SendError(2012, dataRecord);
                                Console.WriteLine("Error sent.");
                            }
                        }
                        catch (Exception ex)
                        {
                            if (_proxy != null)
                                _proxy.Abort();
                            Console.WriteLine("An error occurred.");
                            Console.WriteLine(ex.ToString());
                        }
                        PressAnyKeyToContinue();
                        break;

                    case "6":
                        try
                        {
                            Guid new_id = Guid.NewGuid();
                            DateTime new_timeStamp = DateTime.Now;
                            string new_efin = "000001";
                            string new_iVersion = "12.0";
                            string new_bVersion = "12/31/12";
                            string new_program = "TEST Program";
                            string new_errNum = "123456";
                            string new_lineNum = "654321";
                            string new_message = "TEST Message";
                            string new_stack = "TEST Stack";
                            string new_opSys = "Windows";
                            double new_dotNetVer = 4.0;
                            UnitedStateAbbreviation? new_state = null; //(nullable)
                            string new_category = String.Empty;
                            string new_fix = String.Empty;
                            string new_user = String.Empty;

                            //                            new_id = Guid.Empty;//new Guid("e5f0275e-683f-4678-9260-bf32eae36c85");
                            //                            new_timeStamp = DateTime.Parse("2013-04-04T13:05:33.0753929-04:00");
                            //                            new_efin = "122030";
                            //                            new_iVersion = "12.20i";
                            //                            new_bVersion = "03/28/2013";
                            //                            new_program = "OPENDBF";
                            //                            new_errNum = "1104";
                            //                            new_lineNum = "5810";
                            //                            new_message = @"Error reading file n:\2012\11rcsnet\formlist.dbf.";
                            //                            new_stack = @"Called From: WARN_CHK  at Line: 35
                            //Called From: FORMMEN2  at Line: 385
                            //Called From: GET_DATA  at Line: 536
                            //Called From: MAINMENU  at Line: 1046";
                            //                            new_opSys = null;
                            //                            new_dotNetVer = 0;
                            //                            new_state = null; //(nullable)
                            //                            new_category = String.Empty;
                            //                            new_fix = String.Empty;
                            //                            new_user = String.Empty;


                            new_id = Guid.Empty;//new Guid("e5f0275e-683f-4678-9260-bf32eae36c85");
                            new_timeStamp = DateTime.Now;
                            new_efin = "000001";
                            new_iVersion = "12.370z";
                            new_bVersion = "02/31/2014";
                            new_program = "TESTPROGRAM";
                            new_errNum = "1337";
                            new_lineNum = "1338";
                            new_message = @"I am a test error message.  Fear me, mortals! (Please disregard - this will be deleted)";
                            new_stack = "Called From: THIS_TEST4  at Line: 35\r\n"
                                        + "Called From: THIS_TEST3  at Line: 385\r\n"
                                        + "Called From: THIS_TEST2  at Line: 536\r\n"
                                        + "Called From: THIS_TEST1  at Line: 1046";
                            new_opSys = null;
                            new_dotNetVer = 0;
                            new_state = UnitedStateAbbreviation.PR; //(nullable)
                            new_category = String.Empty;
                            new_fix = String.Empty;
                            new_user = String.Empty;


                            _dataRecord = DataRecord.CreateDataRecord(
                                new_id,
                                new_timeStamp,
                                new_efin,
                                new_iVersion,
                                new_bVersion,
                                new_program,
                                new_errNum,
                                new_lineNum,
                                new_message,
                                new_stack,
                                new_opSys,
                                new_dotNetVer,
                                new_state,
                                new_category,
                                new_fix,
                                new_user);

                            //_dataRecord = DataRecord.CreateDataRecord(Guid.NewGuid(), DateTime.Now,
                            //    "000001", "12.0", "12/31/12", "TEST Program", "123456", "654321", "TEST Message", "TEST Stack", "Windows",
                            //    4.0, null, String.Empty, String.Empty, String.Empty);
                            Console.WriteLine("Error created.");
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Error not created.");
                            Console.WriteLine(ex.ToString());
                        }
                        PressAnyKeyToContinue();
                        break;

                    case "7":
                        try
                        {
                            var exception = GetTestException();

                            Guid new_id = Guid.Empty;
                            DateTime new_timeStamp = DateTime.Now;
                            string new_efin = "000001";
                            string new_iVersion = "13._";
                            string new_bVersion = "13/37/1337";
                            string new_program = Assembly.GetExecutingAssembly().GetName().Name;
                            string new_errNum = null;
                            string new_lineNum = null;
                            string new_message = exception.Message;
                            string new_stack = null;
                            string new_opSys = Environment.OSVersion.VersionString;
                            double new_dotNetVer;
                            Version new_clrVersion = Environment.Version;
                            var parsedDotNetVer = double.TryParse(Environment.Version.ToString(2), out new_dotNetVer);
                            UnitedStateAbbreviation? new_state = null; //(nullable)

                            _dataRecord = DataRecord.CreateDataRecord(
                                new_id,
                                new_timeStamp,
                                new_efin,
                                new_iVersion,
                                new_bVersion,
                                new_program,
                                new_errNum,
                                new_lineNum,
                                new_message,
                                new_stack,
                                new_opSys,
                                new_dotNetVer,
                                new_state);
                            _dataRecord.ExceptionData = ExceptionConverter.CovertToData(exception);
                            _dataRecord.ClrVersion = new_clrVersion;

                            Console.WriteLine("Error created.");
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Error not created.");
                            Console.WriteLine(ex.ToString());
                        }
                        PressAnyKeyToContinue();
                        break;
                }
            } while (input.ToLower() != "0");
        }

        private static void PressAnyKeyToContinue()
        {
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }


        private static Exception GetTestException()
        {
            try
            {
                ThrowException();
            }
            catch (Exception ex)
            {
                return ex;
            }
            return null;
        }

        private static void ThrowException()
        {
            try
            {
                ThrowInnerException1();
            }
            catch (Exception ex)
            {
                throw new Exception("This is an Exception.", ex);
            }
        }

        private static void ThrowInnerException1()
        {
            try
            {
                ThrowInnerException2();
            }
            catch (Exception ex)
            {
                throw new ActionNotSupportedException("This is inner Exception 1.", ex);
            }
        }

        private static void ThrowInnerException2()
        {
            try
            {
                ThrowInnerException3();
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("This is inner Exception 2.", ex);
            }

        }

        private static void ThrowInnerException3()
        {
            throw new WierdNeverHeardOfException("This is inner Exception 3. It is a weird, never before seen, embedded Exception.");
        }

        private static Exception GetStackOverflowException()
        {
            try
            {
                CauseStackOverflowExceptionToThrow();
            }
            catch (StackOverflowException ex)
            {
                return ex;
            }

            return null;
        }

        private static void CauseStackOverflowExceptionToThrow()
        {
            CauseStackOverflowExceptionToThrow();
        }

        [Serializable]
        private class WierdNeverHeardOfException : Exception
        {
            public WierdNeverHeardOfException() : base("This is a wierd, embedded exception.") { }

            public WierdNeverHeardOfException(string message) : base(message) { }

            public WierdNeverHeardOfException(string message, Exception inner) : base(message, inner) { }

            protected WierdNeverHeardOfException(SerializationInfo info, StreamingContext context) : base(info, context) { }
        }   
    }
}

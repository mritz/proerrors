﻿using System.ServiceModel;
using ProErrors.Core.Model;

namespace ProErrors.Service.Contract
{
    [ServiceContract(
        Namespace = "http://ProErrors/services/2011/04")]
    public interface IProErrorsRCS
    {
        [OperationContract]
        bool CanSend(int year, DataRecord dataRecord);
        [OperationContract]
        string GetSolution(int year, DataRecord dataRecord);
        [OperationContract]
        bool SendError(int year, DataRecord dataRecord);
        [OperationContract]
        string TestService();
    }
}
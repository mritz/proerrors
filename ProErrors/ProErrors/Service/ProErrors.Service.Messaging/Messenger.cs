﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using ProErrors.Core;

namespace ProErrors.Service.Messaging
{
    public class Messenger
    {
        public Messenger(int year)
            : this(year.ToString())
        {
        }
        public Messenger(string year)
        {
            this.Path = Config.PRO_ERRORS_ROOT + @"ICtrl" + year + @"\Message\";
        }
      
        public string Path { get; private set; }
        
        public bool PostMessage(Message message)
        {
            if (!this.IsMessageValid(message))
                return false;

            string filename = this.Path + message.Efin + ".msg";

            if (!File.Exists(filename))
                this.CreateMessageFile(filename);

            this.AppendMessageToFile(filename, message);

            return true;
        }
      
        private string AffiliateTypeToString(AffiliateType affiliateType)
        {
            switch (affiliateType)
            {
                case AffiliateType.AllUsers:
                    return "All Users";
                case AffiliateType.TaxSlayerPro:
                    return "TaxSlayer Pro";
                default:
                    return "All Users";
            }
        }
        private void AppendMessageToFile(string filename, Message message)
        {
            using (FileStream fileStream = File.Open(filename, FileMode.Open))
            {
                XmlDocument xmlDocument = new XmlDocument();

                xmlDocument.Load(fileStream);

                XmlNode rootNode = xmlDocument.DocumentElement;
                XmlElement messageElement = this.CreateBlankMessage(xmlDocument);
                XmlNode childNode = messageElement.FirstChild;

                if (message.MessageId != null)
                    childNode.AppendChild(xmlDocument.CreateTextNode(message.MessageId.ToString()));
                childNode = childNode.NextSibling;

                if (message.Read)
                    childNode.AppendChild(xmlDocument.CreateTextNode("True"));
                childNode = childNode.NextSibling;

                if (message.Expiration != DateTime.MinValue)
                    childNode.AppendChild(xmlDocument.CreateTextNode(message.Expiration.ToString()));
                childNode = childNode.NextSibling;

                if (!String.IsNullOrEmpty(message.Title))
                    childNode.AppendChild(xmlDocument.CreateTextNode(message.Title));
                childNode = childNode.NextSibling;

                childNode.AppendChild(xmlDocument.CreateTextNode(this.MessageTypeToString(message.MessageType)));
                childNode = childNode.NextSibling;

                childNode.AppendChild(xmlDocument.CreateTextNode(this.MessagePriorityToString(message.Priority)));
                childNode = childNode.NextSibling;

                if (!String.IsNullOrEmpty(message.SubType))
                    childNode.AppendChild(xmlDocument.CreateTextNode(message.SubType));
                childNode = childNode.NextSibling;

                childNode.AppendChild(xmlDocument.CreateTextNode(this.AffiliateTypeToString(message.Affiliate)));
                childNode = childNode.NextSibling;

                if (message.Timestamp != DateTime.MinValue)
                    childNode.AppendChild(xmlDocument.CreateTextNode(message.Timestamp.ToString()));
                childNode = childNode.NextSibling;

                if (!String.IsNullOrEmpty(message.Header))
                    childNode.AppendChild(xmlDocument.CreateTextNode(message.Header));
                childNode = childNode.NextSibling;

                if (!String.IsNullOrEmpty(message.Body))
                    childNode.AppendChild(xmlDocument.CreateCDataSection(message.Body));

                rootNode.AppendChild(messageElement);

                fileStream.SetLength(0);
                xmlDocument.Save(fileStream);
            }
        }
        private XmlElement CreateBlankMessage(XmlDocument xmlDocument)
        {
            XmlElement messageElement = xmlDocument.CreateElement("message");

            messageElement.AppendChild(xmlDocument.CreateElement("messageid"));
            messageElement.AppendChild(xmlDocument.CreateElement("read"));
            messageElement.AppendChild(xmlDocument.CreateElement("expiration"));
            messageElement.AppendChild(xmlDocument.CreateElement("title"));
            messageElement.AppendChild(xmlDocument.CreateElement("type"));
            messageElement.AppendChild(xmlDocument.CreateElement("priority"));
            messageElement.AppendChild(xmlDocument.CreateElement("subtype"));
            messageElement.AppendChild(xmlDocument.CreateElement("affiliate"));
            messageElement.AppendChild(xmlDocument.CreateElement("datetime"));
            messageElement.AppendChild(xmlDocument.CreateElement("header"));
            messageElement.AppendChild(xmlDocument.CreateElement("body"));

            return messageElement;
        }
        private void CreateMessageFile(string filename)
        {
            using (FileStream fileStream = File.Create(filename))
            {
                XmlDocument xmlDocument = new XmlDocument();

                xmlDocument.AppendChild(xmlDocument.CreateXmlDeclaration("1.0", "utf-8", String.Empty));
                xmlDocument.AppendChild(xmlDocument.CreateElement("messages"));

                xmlDocument.Save(fileStream);
            }
        }
        private bool IsMessageValid(Message message)
        {
            Regex regex = new Regex("[0-9]{6}");
            bool isValid = true;

            if (!regex.IsMatch(message.Efin))
                isValid = false;

            if (String.IsNullOrEmpty(message.Body) ||
                String.IsNullOrEmpty(message.Header) ||
                String.IsNullOrEmpty(message.Title))
                isValid = false;

            if ((message.Expiration != DateTime.MinValue && message.Expiration < DateTime.Now) ||
                message.Timestamp > DateTime.Now)
                isValid = false;

            if (message.MessageId == null)
                isValid = false;

            return isValid;
        }
        private string MessagePriorityToString(PriorityType messagePriority)
        {
            switch (messagePriority)
            {
                case PriorityType.Low:
                    return "Low";
                case PriorityType.Normal:
                    return "Normal";
                case PriorityType.High:
                    return "High";
                default:
                    return "Normal";
            }
        }
        private string MessageTypeToString(MessageType messageType)
        {
            switch (messageType)
            {
                case MessageType.Bank:
                    return "Bank";
                case MessageType.Conversions:
                    return "Conversions";
                case MessageType.Error:
                    return "Error";
                case MessageType.IRS:
                    return "IRS";
                case MessageType.Support:
                    return "Support";
                default:
                    return "Support";
            }
        }
    }
}
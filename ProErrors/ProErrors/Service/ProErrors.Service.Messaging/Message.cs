﻿using System;
using System.Runtime.Serialization;

namespace ProErrors.Service.Messaging
{
    public enum AffiliateType
    {
        AllUsers,
        TaxSlayerPro,
    }

    public enum MessageType
    {
        Bank,
        Conversions,
        Error,
        IRS,
        Support,
    }

    public enum PriorityType
    {
        Low,
        Normal,
        High,
    }

    [DataContract(Namespace = "http://ProErrors/services/2011/04")]
    public class Message
    {
        public Message()
            : this(0, String.Empty, (MessageType)0, (PriorityType)0, (AffiliateType)0)
        { }
        public Message(int year, string efin, MessageType messageType,
            PriorityType priority, AffiliateType affiliate)
            : this(year, efin, String.Empty, messageType, priority, affiliate, String.Empty, String.Empty)
        { }
        public Message(int year, string efin, string title, MessageType messageType,
            PriorityType priority, AffiliateType affiliate, string header, string body)
            : this(year, efin, DateTime.MinValue, title, messageType, priority, affiliate, header, body)
        { }
        public Message(int year, string efin, DateTime expiration, string title, MessageType messageType,
            PriorityType priority, AffiliateType affiliate, string header, string body)
            : this(year, efin, expiration, title, messageType, priority, String.Empty, affiliate, header, body)
        { }
        public Message(int year, string efin, string title, MessageType messageType, PriorityType priority,
            string subType, AffiliateType affiliate, string header, string body)
            : this(year, efin, DateTime.MinValue, title, messageType, priority, subType, affiliate, header, body)
        { }
        public Message(int year, string efin, DateTime expiration, string title, MessageType messageType,
            PriorityType priority, string subType, AffiliateType affiliate, string header, string body)
            : this(year, efin, Guid.NewGuid(), false, expiration, title,
            messageType, priority, subType, affiliate, DateTime.Now, header, body)
        { }
        public Message(int year, string efin, Guid messageId, bool read, DateTime expiration,
            string title, MessageType messageType, PriorityType priority, string subType,
            AffiliateType affiliate, DateTime timestamp, string header, string body)
        {
            this.Year = year;
            this.Efin = efin;
            this.MessageId = messageId;
            this.Read = read;
            this.Expiration = expiration;
            this.Title = title;
            this.MessageType = messageType;
            this.Priority = priority;
            this.SubType = subType;
            this.Affiliate = affiliate;
            this.Timestamp = timestamp;
            this.Header = header;
            this.Body = body;
        }

        [DataMember]
        public AffiliateType Affiliate { get; set; }
        [DataMember]
        public string Body { get; set; }
        [DataMember]
        public string Efin { get; set; }
        [DataMember]
        public DateTime Expiration { get; set; }
        [DataMember]
        public string Header { get; set; }
        [DataMember]
        public Guid MessageId { get; set; }
        [DataMember]
        public MessageType MessageType { get; set; }
        [DataMember]
        public PriorityType Priority { get; set; }
        [DataMember]
        public bool Read { get; set; }
        [DataMember]
        public string SubType { get; set; }
        [DataMember]
        public DateTime Timestamp { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public int Year { get; set; }
    }
}
﻿using System;
using System.ServiceProcess;

namespace ProErrors.Service.Host
{
    internal static class Program
    {
        private static void Main()
        {
            var service = new ProErrorsHost();

            if (Environment.UserInteractive)
            {
                service.OnStartHandler();
                Console.WriteLine("Press any key to stop");
                Console.Read();
                service.OnStopHandler();
            }
            else
            {
                ServiceBase.Run(service);
            }
        }
    }
}
﻿using System;
using System.Collections;
using System.Configuration.Install;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.ServiceProcess;

namespace ProErrors.Service.Host
{
    public enum RecoverAction
    {
        None = 0,
        Restart = 1,
        Reboot = 2,
        RunCommand = 3
    }

    public class FailureAction
    {
        private int _delay = 0;
        private RecoverAction _type = RecoverAction.None;

        public FailureAction()
        {
        }
        public FailureAction(RecoverAction actionType, int actionDelay)
        {
            _type = actionType;
            _delay = actionDelay;
        }

        public int Delay
        {
            get { return _delay; }
            set { _delay = value; }
        }
        public RecoverAction Type
        {
            get { return _type; }
            set { _type = value; }
        }
    }

    public class ProErrorsServiceInstaller : ServiceInstaller
    {
        private const int ERROR_ACCESS_DENIED = 5;
        private const int SC_MANAGER_ALL_ACCESS = 0xF003F;
        private const int SE_PRIVILEGE_ENABLED = 2;
        private const string SE_SHUTDOWN_NAME = "SeShutdownPrivilege";
        private const int SERVICE_ALL_ACCESS = 0xF01FF;
        private const int SERVICE_CONFIG_DESCRIPTION = 0x1;
        private const int SERVICE_CONFIG_FAILURE_ACTIONS = 0x2;
        private const int SERVICE_NO_CHANGE = -1;
        private const int TOKEN_ADJUST_PRIVILEGES = 32;
        private const int TOKEN_QUERY = 8;
       
        public ArrayList FailureActions;
       
        private string _description = "";
        private string _failRebootMsg = "";
        private int _failResetTime = SERVICE_NO_CHANGE;
        private string _failRunCommand = "";
        private string _logMsgBase;
        private bool _setDescription = false;
        private bool _setFailActions = false;
        private bool _startOnInstall = false;
        private int _startTimeout = 15000;
       
        public ProErrorsServiceInstaller()
            : base()
        {
            FailureActions = new ArrayList();
            base.Committed += new InstallEventHandler(this.UpdateServiceConfig);
            base.Committed += new InstallEventHandler(this.StartIfNeeded);
            _logMsgBase = "ProErrorsServiceInstaller : " + base.ServiceName + " : ";
        }
      
        public string Description
        {
            set
            {
                _description = value;
                _setDescription = true;
            }
        }
        public int FailCountResetTime
        {
            set
            {
                _failResetTime = value;
                _setFailActions = true;
            }
        }
        public string FailRebootMsg
        {
            set
            {
                _failRebootMsg = value;
                _setFailActions = true;
            }
        }
        public string FailRunCommand
        {
            set
            {
                _failRunCommand = value;
                _setFailActions = true;
            }
        }
        public bool StartOnInstall
        {
            set { _startOnInstall = value; }
        }
        public int StartTimeout
        {
            set { _startTimeout = value; }
        }
     
        [DllImport("advapi32.dll")]
        public static extern bool
            AdjustTokenPrivileges(IntPtr TokenHandle, bool DisableAllPrivileges,
            [MarshalAs(UnmanagedType.Struct)] ref TOKEN_PRIVILEGES NewState, int BufferLength,
           IntPtr PreviousState, ref int ReturnLength);
        [DllImport("advapi32.dll", EntryPoint = "ChangeServiceConfig2")]
        public static extern bool
            ChangeServiceDescription(IntPtr hService, int dwInfoLevel,
            [MarshalAs(UnmanagedType.Struct)] ref SERVICE_DESCRIPTION lpInfo);
        [DllImport("advapi32.dll", EntryPoint = "ChangeServiceConfig2")]
        public static extern bool
            ChangeServiceFailureActions(IntPtr hService, int dwInfoLevel,
            [MarshalAs(UnmanagedType.Struct)] ref SERVICE_FAILURE_ACTIONS lpInfo);
        [DllImport("kernel32.dll")]
        public static extern bool
            CloseHandle(IntPtr hndl);
        [DllImport("advapi32.dll")]
        public static extern bool
            CloseServiceHandle(IntPtr hSCObject);
        [DllImport("kernel32.dll")]
        public static extern IntPtr
            GetCurrentProcess();
        [DllImport("kernel32.dll")]
        public static extern int
            GetLastError();
        [DllImport("advapi32.dll")]
        public static extern IntPtr
            LockServiceDatabase(IntPtr hSCManager);
        [DllImport("advapi32.dll")]
        public static extern bool
            LookupPrivilegeValue(string lpSystemName, string lpName, ref long lpLuid);
        [DllImport("advapi32.dll")]
        public static extern bool
            OpenProcessToken(IntPtr ProcessHandle, int DesiredAccess, ref IntPtr TokenHandle);
        [DllImport("advapi32.dll")]
        public static extern
            IntPtr OpenSCManager(string lpMachineName, string lpDatabaseName, int dwDesiredAccess);
        [DllImport("advapi32.dll")]
        public static extern IntPtr
            OpenService(IntPtr hSCManager, string lpServiceName, int dwDesiredAccess);
        [DllImport("advapi32.dll")]
        public static extern bool
            UnlockServiceDatabase(IntPtr hSCManager);
       
        private bool GrandShutdownPrivilege()
        {
            // This code mimics the MSDN defined way to adjust privilege for shutdown
            // http://msdn.microsoft.com/library/default.asp?url=/library/en-us/sysinfo/base/shutting_down.asp

            bool retRslt = false;

            IntPtr hToken = IntPtr.Zero;
            IntPtr myProc = IntPtr.Zero;

            TOKEN_PRIVILEGES tkp = new TOKEN_PRIVILEGES();

            long Luid = 0;
            int retLen = 0;

            try
            {
                myProc = GetCurrentProcess();
                bool rslt = OpenProcessToken(myProc, TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, ref hToken);
                if (!rslt) return retRslt;

                LookupPrivilegeValue(null, SE_SHUTDOWN_NAME, ref Luid);

                tkp.PrivilegeCount = 1;
                tkp.Privileges.Luid = Luid;
                tkp.Privileges.Attributes = SE_PRIVILEGE_ENABLED;

                rslt = AdjustTokenPrivileges(hToken, false, ref tkp, 0, IntPtr.Zero, ref retLen);

                if (GetLastError() != 0)
                    throw new Exception("Failed to grant shutdown privilege");

                retRslt = true;
            }
            catch (Exception ex)
            {
                LogInstallMessage(EventLogEntryType.Error, _logMsgBase + ex.Message);
            }
            finally
            {
                if (hToken != IntPtr.Zero)
                    CloseHandle(hToken);
            }

            return retRslt;
        }
        private void LogInstallMessage(EventLogEntryType logLevel, string msg)
        {
            Console.WriteLine(msg);

            try { EventLog.WriteEntry(base.ServiceName, msg, logLevel); }
            catch (Exception exp) { Console.WriteLine(exp.ToString()); }
        }
        private void StartIfNeeded(object sender, InstallEventArgs e)
        {
            // Do we need to do any work?
            if (!_startOnInstall) return;

            try
            {
                TimeSpan waitTo = new TimeSpan(0, 0, _startTimeout);

                // Get a handle to the service
                ServiceController sc = new ServiceController(base.ServiceName);

                // Start the service and wait for it to start
                sc.Start();
                sc.WaitForStatus(ServiceControllerStatus.Running, waitTo);

                // Be good and release our handle
                sc.Close();

                LogInstallMessage(EventLogEntryType.Information, _logMsgBase + " Service Started");
            }
            // Catch all exceptions
            catch (Exception ex) { LogInstallMessage(EventLogEntryType.Error, _logMsgBase + ex.Message); }
        }
        private void UpdateServiceConfig(object sender, InstallEventArgs e)
        {
            // Determine if we need to set fail actions
            _setFailActions = false;
            int numActions = FailureActions.Count;

            if (numActions > 0)
                _setFailActions = true;

            // Do we need to do any work that the base installer did not do already?
            if (!(_setDescription || _setFailActions)) return;

            // We've got work to do
            IntPtr scmHndl = IntPtr.Zero;
            IntPtr svcHndl = IntPtr.Zero;
            IntPtr tmpBuf = IntPtr.Zero;
            IntPtr svcLock = IntPtr.Zero;

            // Err check var
            bool rslt = false;

            // Place all our code in a try block
            try
            {
                // Open the service control manager
                scmHndl = OpenSCManager(null, null, SC_MANAGER_ALL_ACCESS);

                if (scmHndl.ToInt32() <= 0)
                {
                    LogInstallMessage(EventLogEntryType.Error, _logMsgBase + "Failed to Open Service Control Manager");
                    return;
                }

                // Lock the Service Database
                svcLock = LockServiceDatabase(scmHndl);

                if (svcLock.ToInt32() <= 0)
                {
                    LogInstallMessage(EventLogEntryType.Error, _logMsgBase + "Failed to Lock Service Database for Write");
                    return;
                }

                // Open the service
                svcHndl = OpenService(scmHndl, base.ServiceName, SERVICE_ALL_ACCESS);

                if (svcHndl.ToInt32() <= 0)
                {
                    LogInstallMessage(EventLogEntryType.Information, _logMsgBase + "Failed to Open Service ");
                    return;
                }

                // Need to set service failure actions. Note that the API lets us set as many as
                // we want, yet the Service Control Manager GUI only lets us see the first 3.
                // Bill is aware of this and has promised no fixes. Also note that the API allows
                // granularity of seconds whereas GUI only shows days and minutes.
                if (_setFailActions)
                {
                    // We're gonna serialize the SA_ACTION structs into an array of ints
                    // for simplicity in marshalling this variable length ptr to win32

                    int[] actions = new int[numActions * 2];
                    int currInd = 0;
                    bool needShutdownPrivilege = false;

                    foreach (FailureAction fa in FailureActions)
                    {
                        actions[currInd] = (int)fa.Type;
                        actions[++currInd] = fa.Delay;
                        currInd++;

                        if (fa.Type == RecoverAction.Reboot)
                            needShutdownPrivilege = true;
                    }

                    // If we need shutdown privilege, then grant it to this process
                    if (needShutdownPrivilege)
                    {
                        rslt = this.GrandShutdownPrivilege();
                        if (!rslt) return;
                    }

                    // Need to pack 8 bytes per struct
                    tmpBuf = Marshal.AllocHGlobal(numActions * 8);

                    // Move array into marshallable pointer
                    Marshal.Copy(actions, 0, tmpBuf, numActions * 2);

                    // Set the SERVICE_FAILURE_ACTIONS struct
                    SERVICE_FAILURE_ACTIONS sfa = new SERVICE_FAILURE_ACTIONS();

                    sfa.cActions = numActions;
                    sfa.dwResetPeriod = _failResetTime;
                    sfa.lpCommand = _failRunCommand;
                    sfa.lpRebootMsg = _failRebootMsg;
                    sfa.lpsaActions = tmpBuf.ToInt32();

                    // Call the ChangeServiceFailureActions() abstraction of ChangeServiceConfig2()
                    rslt = ChangeServiceFailureActions(svcHndl, SERVICE_CONFIG_FAILURE_ACTIONS, ref sfa);

                    //Check the return
                    if (!rslt)
                    {
                        int err = GetLastError();

                        if (err == ERROR_ACCESS_DENIED)
                            throw new Exception(_logMsgBase + "Access Denied while setting Failure Actions");
                    }

                    // Free the memory
                    Marshal.FreeHGlobal(tmpBuf); tmpBuf = IntPtr.Zero;

                    LogInstallMessage(EventLogEntryType.Information, _logMsgBase + "Successfully configured Failure Actions");
                }

                // Need to set the description field?
                if (_setDescription)
                {
                    SERVICE_DESCRIPTION sd = new SERVICE_DESCRIPTION();
                    sd.lpDescription = _description;

                    // Call the ChangeServiceDescription() abstraction of ChangeServiceConfig2()
                    rslt = ChangeServiceDescription(svcHndl, SERVICE_CONFIG_DESCRIPTION, ref sd);

                    // Error setting description?
                    if (!rslt)
                        throw new Exception(_logMsgBase + "Failed to set description");

                    LogInstallMessage(EventLogEntryType.Information, _logMsgBase + "Successfully set description");
                }
            }
            // Catch all exceptions
            catch (Exception ex) { LogInstallMessage(EventLogEntryType.Error, ex.Message); }
            finally
            {
                if (scmHndl != IntPtr.Zero)
                {
                    // Unlock the service database
                    if (svcLock != IntPtr.Zero)
                    {
                        UnlockServiceDatabase(svcLock);
                        svcLock = IntPtr.Zero;
                    }

                    // Close the service control manager handle
                    CloseServiceHandle(scmHndl);
                    scmHndl = IntPtr.Zero;
                }

                // Close the service handle
                if (svcHndl != IntPtr.Zero)
                {
                    CloseServiceHandle(svcHndl);
                    svcHndl = IntPtr.Zero;
                }

                // Free the memory
                if (tmpBuf != IntPtr.Zero)
                {
                    Marshal.FreeHGlobal(tmpBuf);
                    tmpBuf = IntPtr.Zero;
                }
            } // End try-catch-finally
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct LUID_AND_ATTRIBUTES
        {
            public int Attributes;
            public long Luid;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct SERVICE_DESCRIPTION
        {
            public string lpDescription;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct SERVICE_FAILURE_ACTIONS
        {
            public int cActions;
            public int dwResetPeriod;
            public string lpCommand;
            public string lpRebootMsg;
            public int lpsaActions;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct TOKEN_PRIVILEGES
        {
            public int PrivilegeCount;
            public LUID_AND_ATTRIBUTES Privileges;
        }
    }
}
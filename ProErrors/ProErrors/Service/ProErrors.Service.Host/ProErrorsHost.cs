﻿using System;
using System.Diagnostics;
using System.IO;
using System.ServiceModel;
using System.ServiceProcess;
using System.Threading;
using System.Net.Mail;

namespace ProErrors.Service.Host
{
    public partial class ProErrorsHost : ServiceBase
    {
        internal static ServiceHost _rcsHost;

        private TextWriterTraceListener _listener;
        private bool _messageSent = false;
        private Timer _serviceTimer;

        public ProErrorsHost()
        {
            Trace.WriteLine("Constructing host.", DateTime.Now.ToString());

            InitializeComponent();
            this.ServiceName = "ProErrorsHost";
            this.CanShutdown = true;
            this.CanStop = true;
            AutoLog = false;
            Trace.AutoFlush = true;

            Trace.WriteLine("Finished constructing host.", DateTime.Now.ToString());
        }

        protected override void OnShutdown()
        {
            if (!_messageSent)
            {
                string messageSubject = "Awwwww Snap!";
                string messageBody = String.Format(
                    "{0} on {1} has shut down. {2} is no longer running. Please resolve this as soon as possible",
                    Environment.MachineName,
                    Environment.UserDomainName,
                    AppDomain.CurrentDomain.FriendlyName);

                EmailMessageToProgrammer(messageSubject, messageBody);
                _messageSent = true;
            }

            this.Stop();
            base.OnShutdown();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                this.OnStartHandler();

                string messageSubject = "Awwwww Yeah!";
                string messageBody = String.Format(
                    "{0} on {1} has either restarted or recovered from something. We're really not sure. Either way, {2} is now running.",
                    Environment.MachineName,
                    Environment.UserDomainName,
                    AppDomain.CurrentDomain.FriendlyName);

                EmailMessageToProgrammer(messageSubject, messageBody);
            }
            catch (Exception ex)
            {
                File.WriteAllText("StartError.txt", ex.ToString());
                throw;
            }

        }
        protected override void OnStop()
        {
            this.OnStopHandler();

            if (!_messageSent)
            {
                string messageSubject = "Awwwww Snap!";
                string messageBody = String.Format(
                    "{0} on {1} has encountered an issue. {2} is no longer running. Please resolve this as soon as possible",
                    Environment.MachineName,
                    Environment.UserDomainName,
                    AppDomain.CurrentDomain.FriendlyName);

                EmailMessageToProgrammer(messageSubject, messageBody);
                _messageSent = true;
            }
        }

        private static void EmailMessageToProgrammer(string subject, string body)
        {
            try
            {
                using (MailMessage mailMessage = new MailMessage(
                    String.Format("{0}@taxslayerpro.com", Environment.UserName),
                    "mritz@taxslayerpro.com, wrhyner@taxslayerpro.com",
                    subject, body))
                {
                    using (SmtpClient smtpClient = new SmtpClient(@"exch-hub-01"))
                    {
                        smtpClient.UseDefaultCredentials = true;
                        smtpClient.Send(mailMessage);
                    }
                }
            }
            catch (Exception) { }
        }

        private void InitializeService(Object stateInfo)
        {
            try
            {
                _rcsHost = new ServiceHost(
                    typeof(ProErrorsRCSService));

                _rcsHost.Open();

                string baseAddresses = "";
                foreach (Uri address in _rcsHost.BaseAddresses)
                    baseAddresses += " " + address.AbsoluteUri;
                Trace.WriteLine(String.Format("{0} listening at {1}", this.ServiceName, baseAddresses));
            }
            catch (Exception exp) { Trace.TraceError(exp.ToString()); }
        }

        public void OnStartHandler()
        {
            _serviceTimer = new Timer(InitializeService,
                null, 1000, Timeout.Infinite);
        }
        public void OnStopHandler()
        {
            if (_rcsHost != null)
            {
                _rcsHost.Close();
                //this.EventLog.WriteEntry(String.Format("{0} stopped", this.ServiceName));
            }
            _rcsHost = null;
        }
    }
}

﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace ProErrors.Service.Host
{
    /// <summary>
    /// Installer class for ProErrorsServiceHost.
    /// </summary>
    [RunInstaller(true)]
    public class ProErrorsServiceHostInstaller : Installer
    {
        private ProErrorsServiceInstaller _serviceInstaller;
        private ServiceProcessInstaller _serviceProcessInstaller;

        public ProErrorsServiceHostInstaller()
        {
            this.InitializeComponent();

            _serviceInstaller.DisplayName = "Pro Errors Host";
            _serviceInstaller.Description = "WCF service host for ProErrorsService";
            _serviceInstaller.StartType = ServiceStartMode.Automatic;

            _serviceInstaller.FailCountResetTime = 60 * 60 * 24;

            _serviceInstaller.FailureActions.Add(new FailureAction(RecoverAction.Restart, 60000));
            _serviceInstaller.StartOnInstall = true;
        }

        private void InitializeComponent()
        {
            _serviceProcessInstaller = new ServiceProcessInstaller();
            _serviceInstaller = new ProErrorsServiceInstaller();

            _serviceProcessInstaller.Account = ServiceAccount.User;
            _serviceProcessInstaller.Username = "proerrors@corp.rhodesfs.com";
            _serviceProcessInstaller.Password = "Ph9-MHTV@JH%3jcX";

            _serviceInstaller.ServiceName = "ProErrorsHost";

            Installers.Add(_serviceProcessInstaller);
            Installers.Add(_serviceInstaller);
        }
    }
}
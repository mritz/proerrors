﻿using System;
using System.Linq;
using System.Diagnostics;
using System.ServiceModel;
using ProErrors.Core.Model;
using ProErrors.Service.Contract;
using TaxSlayer.Pro.Errors.Data;
using Database = TaxSlayer.Pro.Errors.Data;
using System.Collections.ObjectModel;

namespace ProErrors.Service
{
    [ServiceBehavior(
        ConcurrencyMode = ConcurrencyMode.Multiple,
        InstanceContextMode = InstanceContextMode.PerCall,
        MaxItemsInObjectGraph = Int32.MaxValue)]
    public class ProErrorsRCSService :
        IProErrorsRCS
    {
        public ProErrorsRCSService()
        {
            Trace.WriteLine("Creating ProErrorsRCSService", DateTime.Now.ToString());
            Trace.Indent();

            Trace.WriteLine("ProErrorsRCSService started.");
            Trace.Unindent();
            Trace.WriteLine("");
        }

        public bool CanSend(int year, DataRecord dataRecord)
        {
            using (var dbContext = new ProErrorsDbContext())
            {
                //attempt to open a connection with the DB to see if it works
                return dbContext.Database.Exists();
            }
            try
            {
                using (var dbContext = new ProErrorsDbContext())
                {
                    //attempt to open a connection with the DB to see if it works
                    return dbContext.Database.Exists();
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(String.Empty);
                Trace.TraceError(DateTime.Now + ": Testing database access (CanSend) failed.\r\n  " + GetExceptionTraceText(ex));
                Trace.WriteLine(String.Empty);

                return false;
            }
        }

        public string GetSolution(int year, DataRecord dataRecord)
        {
            try
            {
                ConvertExceptionData(dataRecord);

                using (var dbContext = new ProErrorsDbContext())
                {
                    //Convert the DataRecord into a database Error entity.
                    FixData solution = FindSolutionAndUpdateDatabase(dataRecord, year, dbContext);

                    if (solution != null)
                    {
                        if (solution.Category == null)
                        {
                            if (!String.IsNullOrEmpty(solution.Details))
                                return solution.Details;
                            else
                                return "The error has been addressed, but there are no details in the database. Please contact technical support for further assistance.";
                        }
                        else if (String.IsNullOrEmpty(solution.Details))
                            return solution.Category.MessageWithoutDetail;
                        else
                            return String.Format(solution.Category.MessageWithDetail, solution.Details);
                    }
                    else
                    {
                        return "No Solution Found";
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(String.Empty);
                Trace.TraceError(DateTime.Now + ": Retrieving solution (GetSolution) failed.\r\n  " + GetExceptionTraceText(ex));
                Trace.WriteLine(String.Empty);

                return "No Solution Found";
            }
        }

        public bool SendError(int year, DataRecord dataRecord)
        {
            try
            {
                ConvertExceptionData(dataRecord);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(String.Empty);
                Trace.TraceError(DateTime.Now + ": Converting ExceptionData (SendError) failed.\r\n  " + GetExceptionTraceText(ex));
                Trace.WriteLine(String.Empty);

                return false;
            }

            try
            {
                using (var dbContext = new ProErrorsDbContext())
                {
                    //Convert the DataRecord into a database Error entity.
                    var dbError = ConvertToDbError(dataRecord, year, dbContext);

                    //Attempt to insert the DataRecord being sent into the database.
                    dbContext.Errors.Add(dbError);
                    dbContext.SaveChanges();

                    if (dbError.Notes.Contains("Sent with no user interaction"))
                    {
                        var errorList = dbContext.Lists.FirstOrDefault(list => list.Name == "Silent SQL Errors");
                        if (errorList != null)
                        {
                            UpdateList.Save(errorList.Id, new[] {dbError.Id});
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(String.Empty);
                Trace.TraceError(DateTime.Now + ": Inserting error (SendError) failed.\r\n  " + GetExceptionTraceText(ex));
                Trace.WriteLine(String.Empty);

                return false;
            }
        }

        public string TestService()
        {
            return "Hello World!";
        }

        private string GetExceptionTraceText(Exception ex)
        {
            return ex.ToString();
        }

        private void ConvertExceptionData(DataRecord dataRecord)
        {
            if (dataRecord.ExceptionData != null)
            {
                var exceptionData = dataRecord.ExceptionData;
                dataRecord.Stack = exceptionData.ToString();
                dataRecord.Message = exceptionData.Message;
                dataRecord.Program = exceptionData.Source;
            }

            double oldVersion;
            if (dataRecord.ClrVersion != null && double.TryParse(dataRecord.ClrVersion.ToString(2), out oldVersion))
                dataRecord.DotNetVersion = oldVersion;
        }

        private Database.Error ConvertToDbError(DataRecord dataRecord, int year, ProErrorsDbContext dbContext)
        {
            Database.Error error = new Database.Error()
            {
                TaxYear = year,
            };
            
            //BVersion
            error.BusinessVersion = dataRecord.BVersion;

            //Category
            if (!String.IsNullOrEmpty(dataRecord.Category))
            {
                if (error.FixData == null)
                    error.FixData = new FixData();

                //Check to see if a category of the same name already exists in the database.
                //TODO: Make sure this String.Compare works with Entity.
                FixCategory dbCategory = dbContext.FixCategories.FirstOrDefault(cat=>String.Compare(cat.Name, dataRecord.Category, true) == 0);

                //Check to see if we found the category in the database.
                if (dbCategory != null)
                    error.FixData.Category = dbCategory;
                
                //Otherwise, create a new category to use.
                else
                    error.FixData.Category = new FixCategory() { Name = dataRecord.Category };
            }

            //DotNetVersion
            error.NetFrameworkVersion = dataRecord.DotNetVersion.ToString();

            //Efin
            int parsedEfin;
            if (int.TryParse(dataRecord.Efin, out parsedEfin))
                error.Efin = parsedEfin;

            //ErrNum
            int parsedErrorNumber;
            if (int.TryParse(dataRecord.ErrNum, out parsedErrorNumber))
                error.ErrorNumber = parsedErrorNumber;

            //FileName
            error.OriginalFilePath = dataRecord.FileName;

            //Fix
            if (!String.IsNullOrEmpty(dataRecord.Fix))
            {
                if (error.FixData == null)
                    error.FixData = new FixData();

                error.FixData.Details = dataRecord.Fix;
            }

            //Fixer
            if (!String.IsNullOrEmpty(dataRecord.Fixer))
            {
                if (error.FixData == null)
                    error.FixData = new FixData();

                error.FixData.Fixer = dataRecord.Fixer;
            }

            //ID
            if (dataRecord.ID != Guid.Empty)
                error.Guid = dataRecord.ID;
            else
                error.Guid = Guid.NewGuid();

            //IsFixed
            if (dataRecord.IsFixed)
            {
                if (error.FixData == null)
                    error.FixData = new FixData();

                error.FixData.Fixed = dataRecord.IsFixed;
            }

            //IVersion
            error.IndividualVersion = dataRecord.IVersion;

            //LineNum
            int parsedLineNumber;
            if (int.TryParse(dataRecord.LineNum, out parsedLineNumber))
                error.LineNumber = parsedLineNumber;

            //Message
            error.Message = dataRecord.Message;

            //OperatingSystem
            error.OperatingSystem = dataRecord.OperatingSystem;

            //Program
            error.ProgramName = dataRecord.Program;

            //Stack
            error.CallStack = dataRecord.Stack;
            
            //TimeStamp
            error.Timestamp = DateTimeOffset.Now;
            
            //Type
            if (dataRecord.Type == DataRecordType.Support)
            {
                if (error.MemberLists == null)
                    error.MemberLists = new Collection<ErrorList>();

                //Get the Support list from the database
                //TODO: Make sure this String.Compare works with Entity.
                ErrorList dbErrorList = dbContext.Lists.FirstOrDefault(list => String.Compare(list.Name, "Support", true) == 0);

                //Check to see if we found the list in the database. If so, then add the error to that list
                if (dbErrorList != null)
                {
                    error.MemberLists.Add(dbErrorList);
                }

                //Otherwise, create a new list to use.
                else
                {
                    ErrorList newList = new ErrorList() { Name = "Support" };
                    error.MemberLists.Add(newList);
                }
            }

            //UserInformed
            if (dataRecord.UserInformed)
            {
                if (error.FixData == null)
                    error.FixData = new FixData();

                error.FixData.UserInformed = dataRecord.UserInformed;
            }

            //StateAbbreviation
            if (dataRecord.StateAbbreviation != null)
            {
                string stateAbbreviation = dataRecord.StateAbbreviation.Value.ToString();

                //Try to get a state with this abbreviation from the database
                //TODO: Make sure this String.Compare works with Entity.
                State dbState = dbContext.States.FirstOrDefault(state => String.Compare(state.Abbreviation, stateAbbreviation, true) == 0);

                //Check to see if we found the state in the database.
                if (dbState != null)
                    error.State = dbState;

                //Otherwise, create a new state to use.
                else
                    error.State = new State() { Abbreviation = stateAbbreviation };
            }

            //Retrieve the type name of the thrown exception that caused this record to be sent.
            if (dataRecord.ExceptionData != null)
            {
                error.ExceptionTypeName = dataRecord.ExceptionData.ExceptionType;
            }

            //Check to see if a new-style CLR Version was sent.
            if (dataRecord.ClrVersion != null)
            {
                error.NetFrameworkVersion = dataRecord.ClrVersion.ToString();
            }

            //Add user notes to the DB record.
            error.Notes = dataRecord.ClientNotes;

            return error;
        }

        private FixData FindSolutionAndUpdateDatabase(DataRecord dataRecord, int year, ProErrorsDbContext dbContext)
        {
            var query = GetEquivalentErrorsQuery(dataRecord, year, dbContext);
            
            //Make sure the error returned is marked fixed and Select dbError.FixData to retrieve the FixData we will be using
            var solutionQuery = 
                query
                    .Where(dbError => (dbError.FixData != null && dbError.FixData.Fixed == true))
                    .Select(dbError => dbError.FixData);
            var solution = solutionQuery.FirstOrDefault();

            //Check to see if we found a solution
            if (solution != null)
            {
                //Update equivalent errors in the database with the solution if those errors are not marked as fixed.
                var equivalentUnfixedErrorsQuery = query.Where(error => error.FixData == null || error.FixData.Fixed == false);

                //Go through each of the equivalent errors from the query
                foreach (Database.Error error in equivalentUnfixedErrorsQuery.ToList())
                {
                    //Update the FixData in the equivalent error
                    if (error.FixData == null)
                        error.FixData = new FixData();

                    error.FixData.Category = solution.Category;
                    error.FixData.DateFixed = solution.DateFixed;
                    error.FixData.Details = solution.Details;
                    error.FixData.Fixer = solution.Fixer;
                    error.FixData.Fixed = true;
                    error.FixData.UserInformed = true;
                }

                //Save changes to the database.
                dbContext.SaveChanges();
            }

            return solution;
        }

        private IQueryable<Database.Error> GetEquivalentErrorsQuery(DataRecord dataRecord, int year, ProErrorsDbContext dbContext)
        {
            int parsedInt;
            int? errorNumber = null;
            int? lineNumber = null;

            if (int.TryParse(dataRecord.ErrNum, out parsedInt))
                errorNumber = parsedInt;

            if (int.TryParse(dataRecord.LineNum, out parsedInt))
                lineNumber = parsedInt;


            //Check the database to see if we have a solution from the same tax year.
            IQueryable<Database.Error> query = dbContext.Errors.Include("FixData.Category")
                                                .Where(dbError => dbError.TaxYear == year);


            //Check for the same BusinessVersion/BVersion; handle the case where it is null.
            if (dataRecord.BVersion == null)
                query = query.Where(dbError => dbError.BusinessVersion == null);
            else
                query = query.Where(dbError => dbError.BusinessVersion == dataRecord.BVersion);


            //Check for the same ErrorNumber/ErrNum; handle the case where it is null.
            if (errorNumber == null)
                query = query.Where(dbError => dbError.ErrorNumber == null);
            else
                query = query.Where(dbError => dbError.ErrorNumber == errorNumber);


            //Check for the same IndividualVersion/IVersion; handle the case where it is null.
            if (dataRecord.IVersion == null)
                query = query.Where(dbError => dbError.IndividualVersion == null);
            else
                query = query.Where(dbError => dbError.IndividualVersion == dataRecord.IVersion);


            //Check for the same LineNumber/LineNum; handle the case where it is null.
            if (lineNumber == null)
                query = query.Where(dbError => dbError.LineNumber == null);
            else
                query = query.Where(dbError => dbError.LineNumber == lineNumber);


            //Check for the same Message; handle the case where it is null.
            if (dataRecord.Message == null)
                query = query.Where(dbError => dbError.Message == null);
            else
                query = query.Where(dbError => dbError.Message == dataRecord.Message);


            //Check for the same ProgramName/Program; handle the case where it is null.
            if (dataRecord.Program == null)
                query = query.Where(dbError => dbError.ProgramName == null);
            else
                query = query.Where(dbError => dbError.ProgramName == dataRecord.Program);


            //Check for the same CallStack/Stack; handle the case where it is null.
            if (dataRecord.Stack == null)
                query = query.Where(dbError => dbError.CallStack == null);
            else
            {
                var linebreakStrippedErrorStack = dataRecord.Stack.Replace("\r", String.Empty).Replace("\n", String.Empty);
                query = query.Where(dbError => dbError.CallStack.Replace("\r", String.Empty).Replace("\n", String.Empty) == linebreakStrippedErrorStack);
            }


            //Check for the same OperatingSystem; handle the case where it is null.
            if (dataRecord.OperatingSystem == null)
                query = query.Where(dbError => dbError.OperatingSystem == null);
            else
                query = query.Where(dbError => dbError.OperatingSystem == dataRecord.OperatingSystem);


            //Check for the same NetFrameworkVersion/DotNetVersion.
            string clrVersionString = null;
            if (dataRecord.ClrVersion != null)
                clrVersionString = dataRecord.ClrVersion.ToString();
            else
                clrVersionString = dataRecord.DotNetVersion.ToString();
            query = query.Where(dbError => dbError.NetFrameworkVersion == clrVersionString);


            return query;
        }
    }
}
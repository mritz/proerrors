﻿using System.Net;
using System.Net.Security;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Web.Services.Protocols;
using System;
using ProErrors.RCS.ProErrorsASMXProxy;
#if DEBUG || TEST
using System.ServiceModel;
using ProErrors.RCS.ProErrorsWCFProxy;
using System.Diagnostics;
#else

using ProErrors.RCS.ProErrorsASMXProxy;

#endif

namespace ProErrors.RCS
{
    /// <summary>
    /// COM object for TaxSlayer Pro used to communicate with ProErrors
    /// through a web service. Currently uses .asmx, but have established
    /// a framework for WCF when available.
    /// </summary>
    [ProgId("ProErrors.RCS.ProErrorsCommunicator")]
    public class ProErrorsCommunicator
    {
        private DataRecord _error;
        private NetworkCredential _networkCredential;
        
        public ProErrorsCommunicator()
        {
            _networkCredential = new NetworkCredential();
        }
       
        public string Password
        {
            get { return _networkCredential.Password; }
            set
            {
                if (_networkCredential.Password == value)
                    return;

                _networkCredential.Password = value;
            }
        }
        public string UserName
        {
            get { return _networkCredential.UserName; }
            set
            {
                if (_networkCredential.UserName == value)
                    return;

                _networkCredential.UserName = value;
            }
        }
        public int Year { get; set; }
        
        public static bool TrustAllCertificatesCallback(object sender, X509Certificate cert, X509Chain chain, SslPolicyErrors errors)
        {
            return true;
        }
        public bool CanSend()
        {
#if DEBUG || TEST
            NetTcpBinding binding = new NetTcpBinding(SecurityMode.None, false);
            EndpointAddress endpoint = new EndpointAddress("net.tcp://localhost:9300/ProErrorsRCSService");

            using (ProErrorsRCSClient proxy = new ProErrorsRCSClient(binding, endpoint))
            {
                if (this.Year == 0 || this.Error == null)
                    throw new ArgumentNullException("Error information must be provided to the COM object.");

                try { return proxy.CanSend(this.Year, this.Error); }
                catch (SoapException) { throw; }
                catch (WebException) { throw; }
            }
#else
            ServicePointManager.ServerCertificateValidationCallback = TrustAllCertificatesCallback;

            using (ProErrorsService proxy = new ProErrorsService())
            {
                if (this.Year == 0 || this._error == null)
                    throw new ArgumentNullException("Error information must be provided to the COM object.");

                if (String.IsNullOrEmpty(this.Password) || String.IsNullOrEmpty(this.UserName))
                    throw new ArgumentNullException("Network credentials must be provided to the COM object.");

                proxy.Credentials = _networkCredential;

                try { return proxy.CanSend(this.Year, this._error); }
                catch (SoapException) { throw; }
                catch (WebException) { throw; }
            }
#endif
        }
        public void CreateError(string efin, string program, string iVersion, string bVersion,
            string errNum, string lineNum, string message, string stack, string state)
        {
            _error = new DataRecord();
            _error.Efin = efin;
            _error.Program = program;
            _error.IVersion = iVersion;
            _error.BVersion = bVersion;
            _error.ErrNum = errNum;
            _error.LineNum = lineNum;
            _error.Message = message;
            _error.Stack = stack;

            if (Enum.IsDefined(typeof(UnitedStateAbbreviation), state))
                _error.StateAbbreviation = (UnitedStateAbbreviation)Enum.Parse(
                    typeof(UnitedStateAbbreviation), state, true);
        }
        public string GetSolution()
        {
#if DEBUG || TEST
            NetTcpBinding binding = new NetTcpBinding(SecurityMode.None, false);
            EndpointAddress endpoint = new EndpointAddress("net.tcp://localhost:9300/ProErrorsRCSService");

            using (ProErrorsRCSClient proxy = new ProErrorsRCSClient(binding, endpoint))
            {
                if (this.Year == 0 || this.Error == null)
                    throw new ArgumentNullException("Error information must be provided to the COM object.");

                try { return proxy.GetSolution(this.Year, this.Error); }
                catch (SoapException) { throw; }
                catch (WebException) { throw; }
            }
#else
            ServicePointManager.ServerCertificateValidationCallback = TrustAllCertificatesCallback;

            using (ProErrorsService proxy = new ProErrorsService())
            {
                if (this.Year == 0 || this._error == null)
                    throw new ArgumentNullException("Error information must be provided to the COM object.");

                if (String.IsNullOrEmpty(this.Password) || String.IsNullOrEmpty(this.UserName))
                    throw new ArgumentNullException("Network credentials must be provided to the COM object.");

                proxy.Credentials = _networkCredential;

                try { return proxy.GetSolution(this.Year, this._error); }
                catch (SoapException) { throw; }
                catch (WebException) { throw; }
            }
#endif
        }
        public bool SendError()
        {
#if DEBUG || TEST
            NetTcpBinding binding = new NetTcpBinding(SecurityMode.None, false);
            EndpointAddress endpoint = new EndpointAddress("net.tcp://localhost:9300/ProErrorsRCSService");

            using (ProErrorsRCSClient proxy = new ProErrorsRCSClient(binding, endpoint))
            {
                if (this.Year == 0 || this.Error == null)
                    throw new ArgumentNullException("Error information must be provided to the COM object.");

                try { return proxy.SendError(this.Year, this.Error); }
                catch (SoapException) { throw; }
                catch (WebException) { throw; }
            }
#else
            ServicePointManager.ServerCertificateValidationCallback = TrustAllCertificatesCallback;

            using (ProErrorsService proxy = new ProErrorsService())
            {
                if (this.Year == 0 || this._error == null)
                    throw new ArgumentNullException("Error information must be provided to the COM object.");

                if (String.IsNullOrEmpty(this.Password) || String.IsNullOrEmpty(this.UserName))
                    throw new ArgumentNullException("Network credentials must be provided to the COM object.");

                proxy.Credentials = _networkCredential;

                try { return proxy.SendError(this.Year, this._error); }
                catch (SoapException) { throw; }
                catch (WebException) { throw; }
            }
#endif
        }
        public string TestService()
        {
#if DEBUG || TEST
            NetTcpBinding binding = new NetTcpBinding(SecurityMode.Transport, false);
            EndpointAddress endpoint = new EndpointAddress("net.tcp://localhost:9300/ProErrorsRCSService");

            using (ProErrorsRCSClient proxy = new ProErrorsRCSClient(binding, endpoint))
            {
                try { return proxy.TestService(); }
                catch (SoapException) { throw; }
                catch (WebException) { throw; }
            }
#else
            ServicePointManager.ServerCertificateValidationCallback = TrustAllCertificatesCallback;

            using (ProErrorsService proxy = new ProErrorsService())
            {
                if (String.IsNullOrEmpty(this.Password) || String.IsNullOrEmpty(this.UserName))
                    throw new ArgumentNullException("Network credentials must be provided to the COM object.");

                proxy.Credentials = _networkCredential;

                try { return proxy.TestService(); }
                catch (SoapException) { throw; }
                catch (WebException) { throw; }
            }
#endif
        }

#if DEBUG || TEST
        public bool CanSend(int year)
        {
            this.Year = year;
            return this.CanSend();
        }
        public bool CanSend(int year, DataRecord dataRecord)
        {
            this.Year = year;
            _error = dataRecord;
            return this.CanSend(year);
        }
        public string GetSolution(int year)
        {
            this.Year = year;
            return this.GetSolution();
        }
        public string GetSolution(int year, DataRecord dataRecord)
        {
            this.Year = year;
            this._error = dataRecord;
            return this.GetSolution(year);
        }
        public bool SendError(int year)
        {
            this.Year = year;
            return this.SendError();
        }
        public bool SendError(int year, DataRecord dataRecord)
        {
            this.Year = year;
            this._error = dataRecord;
            return this.SendError(year);
        }
#endif
    }
}
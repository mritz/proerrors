﻿using System.Windows.Media;
using System.Windows.Documents;
using System.ComponentModel;
using System.Windows;

namespace ProErrors.Client.Adorners
{
    public class SortAdorner : Adorner
    {
        #region Fields

        private readonly static Geometry AscGeometry =
            Geometry.Parse("M 0,0 L 10,0 L 5,5 Z");

        private readonly static Geometry DescGeometry =
            Geometry.Parse("M 0,5 L 10,5 L 5,0 Z");

        #endregion // Fields

        #region Properties

        public ListSortDirection Direction { get; private set; }

        #endregion // Properties

        #region Constructor

        public SortAdorner(UIElement element, ListSortDirection dir)
            : base(element)
        { Direction = dir; }

        #endregion // Constructor

        #region Event Handlers

        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            if (AdornedElement.RenderSize.Width < 20)
                return;

            drawingContext.PushTransform(
                new TranslateTransform(
                    AdornedElement.RenderSize.Width - 15,
                    (AdornedElement.RenderSize.Height - 5) / 2));

            drawingContext.DrawGeometry(Brushes.Black, null,
                Direction == ListSortDirection.Ascending ?
                AscGeometry : DescGeometry);

            drawingContext.Pop();
        }

        #endregion // Event Handlers
    }
}

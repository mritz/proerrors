﻿using System;
using System.Collections.Generic;
using System.Printing;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Xps;
using ProErrors.Client.ViewModel;

namespace ProErrors.Client.Print
{
    /// <summary>
    /// Handles printing for ProErrors
    /// </summary>
    public static class PrintManager
    {
        public static FixedDocument GetFixedDocument(List<DataRecordViewModel> dataRecordViewModelList)
        {
            FixedDocument fixedDocument = new FixedDocument();
            Size pageSize = new Size(8.5 * 96, 11 * 96);
            fixedDocument.DocumentPaginator.PageSize = pageSize;

            foreach (DataRecordViewModel drVM in dataRecordViewModelList)
            {
                PageContent pageContent = new PageContent();
                fixedDocument.Pages.Add(pageContent);

                FixedPage fixedPage = new FixedPage();

                TextBlock textBlock = new TextBlock();
                Thickness margin = new Thickness(0, 0, 10, 0);
                FontFamily fontFamily = new FontFamily("Courier New");

                fixedPage.Children.Add(
                    new TextBlock()
                    {
                        Text = String.Format(
                        "{0} ERROR REPORT",
                        DateTime.Now.ToString()),
                        Margin = margin,
                        FontFamily = fontFamily
                    });

                fixedPage.Children.Add(
                    new TextBlock()
                    {
                        Text = String.Format(
                        "Error Submitted on: {0}",
                        drVM.TimeStamp.ToString()),
                        Margin = margin,
                        FontFamily = fontFamily
                    });

                fixedPage.Children.Add(
                    new TextBlock()
                    {
                        Text = String.Format(
                        "Individual Version: {0}",
                        drVM.IVersion),
                        Margin = margin,
                        FontFamily = fontFamily
                    });

                fixedPage.Children.Add(
                    new TextBlock()
                    {
                        Text = String.Format(
                        "Business Version  : {0}",
                        drVM.BVersion),
                        Margin = margin,
                        FontFamily = fontFamily
                    });

                fixedPage.Children.Add(
                    new TextBlock()
                    {
                        Text = String.Format(
                        "EFIN   : {0}",
                        drVM.Efin),
                        Margin = margin,
                        FontFamily = fontFamily
                    });

                fixedPage.Children.Add(
                    new TextBlock()
                    {
                        Text = String.Format(
                        "Program: {0}   Received Error: {1}   at Line {2}",
                        drVM.Program,
                        drVM.ErrNum,
                        drVM.LineNum),
                        Margin = margin,
                        FontFamily = fontFamily
                    });

                fixedPage.Children.Add(
                    new TextBlock()
                    {
                        Text = String.Format(
                        "Message: {0}",
                        drVM.Message),
                        Margin = margin,
                        FontFamily = fontFamily
                    });

                fixedPage.Children.Add(
                    new TextBlock()
                    {
                        Text = drVM.Stack,
                        Margin = margin,
                        FontFamily = fontFamily
                    });

                ((IAddChild)pageContent).AddChild(fixedPage);
            }

            return fixedDocument;
        }

        public static FlowDocument GetFlowDocument(List<DataRecordViewModel> dataRecordViewModelList)
        {
            bool firstPass = true;
            FlowDocument flowDocument = new FlowDocument()
            {
                FontFamily = new FontFamily("Courier New"),
                IsColumnWidthFlexible = true,
                PagePadding = new Thickness(50),
                ColumnWidth = 800
            };

            foreach (DataRecordViewModel drVM in dataRecordViewModelList)
            {
                Paragraph paragraph = new Paragraph();
                if (firstPass)
                    firstPass = false;
                else
                    paragraph.BreakPageBefore = true;

                paragraph.Inlines.Add(DateTime.Now.ToString() + " ERROR REPORT");
                flowDocument.Blocks.Add(paragraph);

                paragraph = new Paragraph();
                paragraph.Inlines.Add("Error Submitted on: " + drVM.TimeStamp.ToString());
                flowDocument.Blocks.Add(paragraph);

                paragraph = new Paragraph();
                paragraph.Inlines.Add("Individual Version: " + drVM.IVersion);
                flowDocument.Blocks.Add(paragraph);

                paragraph = new Paragraph();
                paragraph.Inlines.Add("Business Version  : " + drVM.BVersion);
                flowDocument.Blocks.Add(paragraph);

                paragraph = new Paragraph();
                paragraph.Inlines.Add("EFIN   : " + drVM.Efin);
                flowDocument.Blocks.Add(paragraph);

                paragraph = new Paragraph();
                paragraph.Inlines.Add("Program: " + drVM.Program +
                    "   Received Error: " + drVM.ErrNum +
                    "   at Line: " + drVM.LineNum);
                flowDocument.Blocks.Add(paragraph);

                paragraph = new Paragraph();
                paragraph.Inlines.Add("Message: " + drVM.Message);
                flowDocument.Blocks.Add(paragraph);

                paragraph = new Paragraph();
                paragraph.Inlines.Add("Stack:\r\n" + drVM.Stack);
                flowDocument.Blocks.Add(paragraph);
            }

            return flowDocument;
        }

        public static PrintQueue GetPrintQueue()
        {
            PrintDialog printDialog = new PrintDialog();
            bool? result = printDialog.ShowDialog();

            if (result.HasValue && result.Value)
                return printDialog.PrintQueue;

            return null;
        }

        public static void PrintFixedDocument(List<DataRecordViewModel> dataRecordViewModelList)
        {
            FixedDocument fixedDocument = GetFixedDocument(dataRecordViewModelList);

            PrintQueue printQueue = GetPrintQueue();

            if (printQueue == null)
                return;

            XpsDocumentWriter writer =
                PrintQueue.CreateXpsDocumentWriter(printQueue);

            PrintTicket printTicket = new PrintTicket();
            printTicket.PageOrientation = PageOrientation.Portrait;
            printTicket.CopyCount = 1;

            PrintCapabilities printCapabilities = printQueue.GetPrintCapabilities(printTicket);

            writer.WriteAsync(fixedDocument, printTicket);
        }

        public static void PrintFlowDocument(List<DataRecordViewModel> dataRecordViewModelList)
        {
            FlowDocument flowDocument = GetFlowDocument(dataRecordViewModelList);

            DocumentPaginator documentPaginator =
                ((IDocumentPaginatorSource)flowDocument).DocumentPaginator;

            PrintQueue printQueue = GetPrintQueue();

            if (printQueue == null)
                return;

            XpsDocumentWriter writer =
                PrintQueue.CreateXpsDocumentWriter(printQueue);

            PrintTicket printTicket = new PrintTicket();
            printTicket.PageOrientation = PageOrientation.Portrait;
            printTicket.CopyCount = 1;

            PrintCapabilities printCapabilities = printQueue.GetPrintCapabilities(printTicket);

            writer.WriteAsync(documentPaginator, printTicket);
        }
    }
}

﻿using System.Diagnostics;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;

[assembly: ContractNamespace(
    "http://ProErrors/services/2011/04",
    ClrNamespace = "ProErrors.Client.Proxies")]

namespace ProErrors.Client.Proxies
{
    [ServiceContract(
        Namespace = "http://ProErrors/services/2011/04",
        ConfigurationName = "IProErrorsStatus",
        CallbackContract = typeof(IProErrorsStatusCallback))]
    public interface IProErrorsStatus
    {
        [OperationContract(
            Action = "http://ProErrors/services/2011/04/IProErrorsStatus/CheckIn",
            ReplyAction = "http://ProErrors/services/2011/04/IProErrorsStatus/CheckInResponse")]
        bool CheckIn();

        [OperationContract(
            Action = "http://ProErrors/services/2011/04/IProErrorsStatus/GetRepositoryStatus",
            ReplyAction = "http://ProErrors/services/2011/04/IProErrorsStatus/GetRepositoryStatusResponse")]
        RepositoryStatus GetRepositoryStatus(int year, DataRecordType type);

        [OperationContract(
            Action = "http://ProErrors/services/2011/04/IProErrorsStatus/GetExemptEfins",
            ReplyAction = "http://ProErrors/services/2011/04/IProErrorsStatus/GetExemptEfinsResponse")]
        string[] GetExemptEfins();

        [OperationContract(
            Action = "http://ProErrors/services/2011/04/IProErrorsStatus/GetTaxYears",
            ReplyAction = "http://ProErrors/services/2011/04/IProErrorsStatus/GetTaxYearsResponse")]
        int[] GetTaxYears();

        [OperationContract(
            IsOneWay = true,
            Action = "http://ProErrors/services/2011/04/IProErrorsStatus/RegisterEfin")]
        void RegisterEfin(string efin);

        [OperationContract(
            Action = "http://ProErrors/services/2011/04/IProErrorsStatus/RegisterForStatusUpdates",
            ReplyAction = "http://ProErrors/services/2011/04/IProErrorsStatus/RegisterForStatusUpdatesResponse")]
        bool RegisterForStatusUpdates();

        [OperationContract(
            Action = "http://ProErrors/services/2011/04/IProErrorsStatus/RepositoryExists",
            ReplyAction = "http://ProErrors/services/2011/04/IProErrorsStatus/RepositoryExistsResponse")]
        bool RepositoryExists(int year, DataRecordType type);

        [OperationContractAttribute(
            IsOneWay = true,
            Action = "http://ProErrors/services/2011/04/IProErrorsStatus/ResetMasterRepository")]
        void ResetMasterRepository();

        [OperationContract(
            IsOneWay = true,
            Action = "http://ProErrors/services/2011/04/IProErrorsStatus/UnregisterEfin")]
        void UnregisterEfin(string efin);

        [OperationContract(
            IsOneWay = true,
            Action = "http://ProErrors/services/2011/04/IProErrorsStatus/UnregisterForStatusUpdates")]
        void UnregisterForStatusUpdates();
    }

    public interface IProErrorsStatusCallback
    {
        [OperationContract(
            IsOneWay = true,
            Action = "http://ProErrors/services/2011/04/IProErrorsStatus/DataRecordRepositoryAdded")]
        void DataRecordRepositoryAdded(int year, DataRecordType type);

        [OperationContract(
            IsOneWay = true,
            Action = "http://ProErrors/services/2011/04/IProErrorsStatus/DataRecordRepositoryRemoved")]
        void DataRecordRepositoryRemoved(int year, DataRecordType type);

        [OperationContract(
            IsOneWay = true,
            Action = "http://ProErrors/services/2011/04/IProErrorsStatus/ExemptEfinAdded")]
        void ExemptEfinAdded(string efin);

        [OperationContract(
            IsOneWay = true,
            Action = "http://ProErrors/services/2011/04/IProErrorsStatus/ExemptEfinRemoved")]
        void ExemptEfinRemoved(string efin);

        [OperationContract(
            IsOneWay = true,
            Action = "http://ProErrors/services/2011/04/IProErrorsStatus/RepositoryActivityChanged")]
        void RepositoryActivityChanged(string activity);

        [OperationContract(
            IsOneWay = true,
            Action = "http://ProErrors/services/2011/04/IProErrorsStatus/RepositoryStatusChanged")]
        void RepositoryStatusChanged(int year, DataRecordType type, RepositoryStatus status);
    }

    public interface IProErrorsStatusChannel :
        IProErrorsStatus,
        IClientChannel
    {
    }

    //[DebuggerStepThrough()]
    public partial class ProErrorsStatusClient :
        DuplexClientBase<IProErrorsStatus>,
        IProErrorsStatus
    {
        public ProErrorsStatusClient(InstanceContext callbackInstance) :
            base(callbackInstance)
        {
        }

        public ProErrorsStatusClient(InstanceContext callbackInstance, string endpointConfigurationName) :
            base(callbackInstance, endpointConfigurationName)
        {
        }

        public ProErrorsStatusClient(InstanceContext callbackInstance, string endpointConfigurationName, string remoteAddress) :
            base(callbackInstance, endpointConfigurationName, remoteAddress)
        {
        }

        public ProErrorsStatusClient(InstanceContext callbackInstance, string endpointConfigurationName, EndpointAddress remoteAddress) :
            base(callbackInstance, endpointConfigurationName, remoteAddress)
        {
        }

        public ProErrorsStatusClient(InstanceContext callbackInstance, Binding binding, EndpointAddress remoteAddress) :
            base(callbackInstance, binding, remoteAddress)
        {
        }

        public bool CheckIn()
        {
            return base.Channel.CheckIn();
        }

        public RepositoryStatus GetRepositoryStatus(int year, DataRecordType type)
        {
            return base.Channel.GetRepositoryStatus(year, type);
        }

        public string[] GetExemptEfins()
        {
            return base.Channel.GetExemptEfins();
        }

        public int[] GetTaxYears()
        {
            return base.Channel.GetTaxYears();
        }

        public void RegisterEfin(string efin)
        {
            base.Channel.RegisterEfin(efin);
        }

        public bool RegisterForStatusUpdates()
        {
            return base.Channel.RegisterForStatusUpdates();
        }

        public bool RepositoryExists(int year, DataRecordType type)
        {
            return base.Channel.RepositoryExists(year, type);
        }

        public void ResetMasterRepository()
        {
            base.Channel.ResetMasterRepository();
        }

        public void UnregisterEfin(string efin)
        {
            base.Channel.UnregisterEfin(efin);
        }

        public void UnregisterForStatusUpdates()
        {
            base.Channel.UnregisterForStatusUpdates();
        }
    }
}
﻿using System.Diagnostics;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;

[assembly: ContractNamespaceAttribute(
    "http://ProErrors/services/2011/04",
    ClrNamespace = "ProErrors.Client.Proxies")]

namespace ProErrors.Client.Proxies
{
    [ServiceContractAttribute(
        Namespace = "http://ProErrors/services/2011/04",
        ConfigurationName = "IProErrorsReport")]
    public interface IProErrorsReport
    {
        [OperationContractAttribute(
            Action = "http://ProErrors/services/2011/04/IProErrorsReport/GetDataRecords",
            ReplyAction = "http://ProErrors/services/2011/04/IProErrorsReport/GetDataRecordsResponse")]
        DataRecord[] GetDataRecords(int year);
    }

    public interface IProErrorsReportChannel :
        IProErrorsReport,
        IClientChannel
    {
    }

    [DebuggerStepThroughAttribute()]
    public partial class ProErrorsReportClient :
        ClientBase<IProErrorsReport>,
        IProErrorsReport
    {
        public ProErrorsReportClient() :
            base()
        {
        }

        public ProErrorsReportClient(string endpointConfigurationName) :
            base(endpointConfigurationName)
        {
        }

        public ProErrorsReportClient(string endpointConfigurationName, string remoteAddress) :
            base(endpointConfigurationName, remoteAddress)
        {
        }

        public ProErrorsReportClient(string endpointConfigurationName, EndpointAddress remoteAddress) :
            base(endpointConfigurationName, remoteAddress)
        {
        }

        public ProErrorsReportClient(Binding binding, EndpointAddress remoteAddress) :
            base(binding, remoteAddress)
        {
        }

        public DataRecord[] GetDataRecords(int year)
        {
            return base.Channel.GetDataRecords(year);
        }
    }
}
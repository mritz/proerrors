﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace ProErrors.Client.Proxies
{
    [DebuggerStepThroughAttribute()]
    [DataContractAttribute(
        Name = "DataRecordRepository",
        Namespace = "http://ProErrors/services/2011/04")]
    public partial class DataRecordRepository :
        object,
        IExtensibleDataObject
    {
        private ExtensionDataObject extensionDataField;
        private DataRecordCollection CollectionField;
        private DataRecordType DataRecordTypeField;
        private RepositoryStatus StatusField;
        private int YearField;

        public ExtensionDataObject ExtensionData
        {
            get { return this.extensionDataField; }
            set { this.extensionDataField = value; }
        }

        [DataMemberAttribute()]
        public DataRecordCollection Collection
        {
            get { return this.CollectionField; }
            set { this.CollectionField = value; }
        }

        [DataMemberAttribute()]
        public DataRecordType DataRecordType
        {
            get { return this.DataRecordTypeField; }
            set { this.DataRecordTypeField = value; }
        }

        [DataMemberAttribute()]
        public RepositoryStatus Status
        {
            get { return this.StatusField; }
            set { this.StatusField = value; }
        }

        [DataMemberAttribute()]
        public int Year
        {
            get { return this.YearField; }
            set { this.YearField = value; }
        }
    }

    [DebuggerStepThroughAttribute()]
    [CollectionDataContractAttribute(
        Name = "DataRecordCollection",
        Namespace = "http://ProErrors/services/2011/04",
        ItemName = "DataRecord")]
    public class DataRecordCollection :
        List<DataRecord>
    {
    }

    [DebuggerStepThroughAttribute()]
    [DataContractAttribute(
        Name = "DataRecord",
        Namespace = "http://ProErrors/services/2011/04")]
    public partial class DataRecord :
        object,
        IExtensibleDataObject
    {
        private ExtensionDataObject extensionDataField;
        private string BVersionField;
        private string CategoryField;
        private double DotNetVersionField;
        private string EfinField;
        private string ErrNumField;
        private string FixField;
        private Guid IDField;
        private bool IsFixedField;
        private string IVersionField;
        private string LineNumField;
        private string MessageField;
        private string OperatingSystemField;
        private string ProgramField;
        private string StackField;
        private Nullable<UnitedStateAbbreviation> StateAbbreviationField;
        private DateTime TimeStampField;
        private DataRecordType TypeField;
        private string UserField;

        public ExtensionDataObject ExtensionData
        {
            get { return this.extensionDataField; }
            set { this.extensionDataField = value; }
        }

        [DataMemberAttribute()]
        public string BVersion
        {
            get { return this.BVersionField; }
            set { this.BVersionField = value; }
        }

        [DataMemberAttribute()]
        public string Category
        {
            get { return this.CategoryField; }
            set { this.CategoryField = value; }
        }

        [DataMemberAttribute()]
        public double DotNetVersion
        {
            get { return this.DotNetVersionField; }
            set { this.DotNetVersionField = value; }
        }

        [DataMemberAttribute()]
        public string Efin
        {
            get { return this.EfinField; }
            set { this.EfinField = value; }
        }

        [DataMemberAttribute()]
        public string ErrNum
        {
            get { return this.ErrNumField; }
            set { this.ErrNumField = value; }
        }

        [DataMemberAttribute()]
        public string Fix
        {
            get { return this.FixField; }
            set { this.FixField = value; }
        }

        [DataMemberAttribute()]
        public Guid ID
        {
            get { return this.IDField; }
            set { this.IDField = value; }
        }

        [DataMemberAttribute()]
        public bool IsFixed
        {
            get { return this.IsFixedField; }
            set { this.IsFixedField = value; }
        }

        [DataMemberAttribute()]
        public string IVersion
        {
            get { return this.IVersionField; }
            set { this.IVersionField = value; }
        }

        [DataMemberAttribute()]
        public string LineNum
        {
            get { return this.LineNumField; }
            set { this.LineNumField = value; }
        }

        [DataMemberAttribute()]
        public string Message
        {
            get { return this.MessageField; }
            set { this.MessageField = value; }
        }

        [DataMemberAttribute()]
        public string OperatingSystem
        {
            get { return this.OperatingSystemField; }
            set { this.OperatingSystemField = value; }
        }

        [DataMemberAttribute()]
        public string Program
        {
            get { return this.ProgramField; }
            set { this.ProgramField = value; }
        }

        [DataMemberAttribute()]
        public string Stack
        {
            get { return this.StackField; }
            set { this.StackField = value; }
        }

        [DataMemberAttribute()]
        public Nullable<UnitedStateAbbreviation> StateAbbreviation
        {
            get { return this.StateAbbreviationField; }
            set { this.StateAbbreviationField = value; }
        }

        [DataMemberAttribute()]
        public DateTime TimeStamp
        {
            get { return this.TimeStampField; }
            set { this.TimeStampField = value; }
        }

        [DataMemberAttribute()]
        public DataRecordType Type
        {
            get { return this.TypeField; }
            set { this.TypeField = value; }
        }

        [DataMemberAttribute()]
        public string User
        {
            get { return this.UserField; }
            set { this.UserField = value; }
        }
    }

    [DataContractAttribute(
        Name = "DataRecordType",
        Namespace = "http://ProErrors/services/2011/04")]
    public enum DataRecordType :
        int
    {
        [EnumMemberAttribute()]
        Error = 0,

        [EnumMemberAttribute()]
        Solution = 1,

        [EnumMemberAttribute()]
        Support = 2,
    }

    [DataContractAttribute(
        Name = "RepositoryStatus",
        Namespace = "http://ProErrors/services/2011/04")]
    public enum RepositoryStatus :
        int
    {
        [EnumMemberAttribute()]
        Busy = 0,

        [EnumMemberAttribute()]
        Created = 1,

        [EnumMemberAttribute()]
        Idle = 2,

        [EnumMemberAttribute()]
        Initializing = 3,

        [EnumMemberAttribute()]
        ReceivingError = 4,

        [EnumMemberAttribute()]
        ReceivingSolution = 5,

        [EnumMemberAttribute()]
        Reconciling = 6,

        [EnumMemberAttribute()]
        Resetting = 7,
    }

    [DataContract(
        Name = "UnitedStateAbbreviation",
        Namespace = "http://ProErrors/services/2011/04")]
    public enum UnitedStateAbbreviation
    {
        [EnumMember]
        AL,
        [EnumMember]
        AK,
        [EnumMember]
        AZ,
        [EnumMember]
        AR,
        [EnumMember]
        CA,
        [EnumMember]
        CO,
        [EnumMember]
        CT,
        [EnumMember]
        DE,
        [EnumMember]
        DC,
        [EnumMember]
        FL,
        [EnumMember]
        GA,
        [EnumMember]
        HI,
        [EnumMember]
        ID,
        [EnumMember]
        IL,
        [EnumMember]
        IN,
        [EnumMember]
        IA,
        [EnumMember]
        KS,
        [EnumMember]
        KY,
        [EnumMember]
        LA,
        [EnumMember]
        ME,
        [EnumMember]
        MD,
        [EnumMember]
        MA,
        [EnumMember]
        MI,
        [EnumMember]
        MN,
        [EnumMember]
        MS,
        [EnumMember]
        MO,
        [EnumMember]
        MT,
        [EnumMember]
        NE,
        [EnumMember]
        NV,
        [EnumMember]
        NH,
        [EnumMember]
        NJ,
        [EnumMember]
        NM,
        [EnumMember]
        NY,
        [EnumMember]
        NC,
        [EnumMember]
        ND,
        [EnumMember]
        OH,
        [EnumMember]
        OK,
        [EnumMember]
        OR,
        [EnumMember]
        PA,
        [EnumMember]
        PR,
        [EnumMember]
        RI,
        [EnumMember]
        SC,
        [EnumMember]
        SD,
        [EnumMember]
        TN,
        [EnumMember]
        TX,
        [EnumMember]
        UT,
        [EnumMember]
        VT,
        [EnumMember]
        VA,
        [EnumMember]
        WA,
        [EnumMember]
        WV,
        [EnumMember]
        WI,
        [EnumMember]
        WY,
    }
}

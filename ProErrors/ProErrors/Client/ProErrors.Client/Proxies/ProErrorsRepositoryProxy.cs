﻿using System.Diagnostics;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;

[assembly: ContractNamespaceAttribute(
    "http://ProErrors/services/2011/04",
    ClrNamespace = "ProErrors.Client.Proxies")]

namespace ProErrors.Client.Proxies
{
    [ServiceContractAttribute(
        Namespace = "http://ProErrors/services/2011/04",
        ConfigurationName = "IProErrorsRepository",
        CallbackContract = typeof(IProErrorsRepositoryCallback))]
    public interface IProErrorsRepository
    {
        [OperationContractAttribute(
            Action = "http://ProErrors/services/2011/04/IProErrorsRepository/CheckIn",
            ReplyAction = "http://ProErrors/services/2011/04/IProErrorsRepository/CheckInResponse")]
        bool CheckIn();

        [OperationContractAttribute(
            Action = "http://ProErrors/services/2011/04/IProErrorsRepository/GetRepository",
            ReplyAction = "http://ProErrors/services/2011/04/IProErrorsRepository/GetRepositoryResponse")]
        DataRecordRepository GetRepository();

        [OperationContractAttribute(
            Action = "http://ProErrors/services/2011/04/IProErrorsRepository/GetStatus",
            ReplyAction = "http://ProErrors/services/2011/04/IProErrorsRepository/GetStatusResponse")]
        RepositoryStatus GetStatus();

        [OperationContractAttribute(
            IsOneWay = true,
            Action = "http://ProErrors/services/2011/04/IProErrorsRepository/MarkDataRecordFixed")]
        void MarkDataRecordFixed(DataRecord dataRecord);

        [OperationContractAttribute(
            IsOneWay = true,
            Action = "http://ProErrors/services/2011/04/IProErrorsRepository/MarkDataRecordsFixed")]
        void MarkDataRecordsFixed(DataRecord[] dataRecords);

        [OperationContractAttribute(
            IsOneWay = true,
            Action = "http://ProErrors/services/2011/04/IProErrorsRepository/MarkDataRecordIrrelevant")]
        void MarkDataRecordIrrelevant(DataRecord dataRecord);

        [OperationContractAttribute(
            IsOneWay = true,
            Action = "http://ProErrors/services/2011/04/IProErrorsRepository/MarkDataRecordsIrrelevant")]
        void MarkDataRecordsIrrelevant(DataRecord[] dataRecords);  
        
        [OperationContractAttribute(
            IsOneWay = true,
            Action = "http://ProErrors/services/2011/04/IProErrorsRepository/MarkDataRecordSupport")]
        void MarkDataRecordSupport(DataRecord dataRecord);

        [OperationContractAttribute(
            IsOneWay = true,
            Action = "http://ProErrors/services/2011/04/IProErrorsRepository/MarkDataRecordsSupport")]
        void MarkDataRecordsSupport(DataRecord[] dataRecords);

        [OperationContractAttribute(
            Action = "http://ProErrors/services/2011/04/IProErrorsRepository/RegisterForDataRecordUpdates",
            ReplyAction = "http://ProErrors/services/2011/04/IProErrorsRepository/RegisterForDataRecordUpdatesResponse")]
        bool RegisterForDataRecordUpdates(int year, DataRecordType type);

        [OperationContractAttribute(
            IsOneWay = true,
            Action = "http://ProErrors/services/2011/04/IProErrorsRepository/UnregisterForDataRecordUpdates")]
        void UnregisterForDataRecordUpdates();
    }

    public interface IProErrorsRepositoryCallback
    {
        [OperationContractAttribute(
            IsOneWay = true,
            Action = "http://ProErrors/services/2011/04/IProErrorsRepository/DataRecordAdded")]
        void DataRecordAdded(int year, DataRecordType type, DataRecord dataRecord);

        [OperationContractAttribute(
            IsOneWay = true,
            Action = "http://ProErrors/services/2011/04/IProErrorsRepository/DataRecordMarkedFixed")]
        void DataRecordMarkedFixed(int year, DataRecord dataRecord);

        [OperationContractAttribute(
            IsOneWay = true,
            Action = "http://ProErrors/services/2011/04/IProErrorsRepository/DataRecordRemoved")]
        void DataRecordRemoved(int year, DataRecordType type, DataRecord dataRecord);
    }

    public interface IProErrorsRepositoryChannel :
        IProErrorsRepository,
        IClientChannel
    {
    }

    [DebuggerStepThroughAttribute()]
    public partial class ProErrorsRepositoryClient :
        DuplexClientBase<IProErrorsRepository>,
        IProErrorsRepository
    {
        public ProErrorsRepositoryClient(InstanceContext callbackInstance) :
            base(callbackInstance)
        {
        }

        public ProErrorsRepositoryClient(InstanceContext callbackInstance, string endpointConfigurationName) :
            base(callbackInstance, endpointConfigurationName)
        {
        }

        public ProErrorsRepositoryClient(InstanceContext callbackInstance, string endpointConfigurationName, string remoteAddress) :
            base(callbackInstance, endpointConfigurationName, remoteAddress)
        {
        }

        public ProErrorsRepositoryClient(InstanceContext callbackInstance, string endpointConfigurationName, EndpointAddress remoteAddress) :
            base(callbackInstance, endpointConfigurationName, remoteAddress)
        {
        }

        public ProErrorsRepositoryClient(InstanceContext callbackInstance, Binding binding, EndpointAddress remoteAddress) :
            base(callbackInstance, binding, remoteAddress)
        {
        }

        public bool CheckIn()
        {
            return base.Channel.CheckIn();
        }

        public DataRecordRepository GetRepository()
        {
            return base.Channel.GetRepository();
        }

        public RepositoryStatus GetStatus()
        {
            return base.Channel.GetStatus();
        }

        public void MarkDataRecordFixed(DataRecord dataRecord)
        {
            base.Channel.MarkDataRecordFixed(dataRecord);
        }

        public void MarkDataRecordsFixed(DataRecord[] dataRecords)
        {
            base.Channel.MarkDataRecordsFixed(dataRecords);
        }

        public void MarkDataRecordIrrelevant(DataRecord dataRecord)
        {
            base.Channel.MarkDataRecordIrrelevant(dataRecord);
        }

        public void MarkDataRecordsIrrelevant(DataRecord[] dataRecords)
        {
            base.Channel.MarkDataRecordsIrrelevant(dataRecords);
        }

        public void MarkDataRecordSupport(DataRecord dataRecord)
        {
            base.Channel.MarkDataRecordSupport(dataRecord);
        }

        public void MarkDataRecordsSupport(DataRecord[] dataRecords)
        {
            base.Channel.MarkDataRecordsSupport(dataRecords);
        }
        
        public bool RegisterForDataRecordUpdates(int year, DataRecordType type)
        {
            return base.Channel.RegisterForDataRecordUpdates(year, type);
        }

        public void UnregisterForDataRecordUpdates()
        {
            base.Channel.UnregisterForDataRecordUpdates();
        }
    }
}
﻿using System.Windows;
using System.Windows.Controls;
using ProErrors.Client.Proxies;
using ProErrors.Client.ViewModel;

namespace ProErrors.Client.Selectors
{
    /// <summary>
    /// Selects between Error or Solution styles.
    /// </summary>
    public class DataRecordStyleSelector : StyleSelector
    {
        /// <summary>
        /// DataRecordView Style (Error)
        /// </summary>
        public Style ErrorStyle { get; set; }
        /// <summary>
        /// DataRecordView Style (Solution)
        /// </summary>
        public Style SolutionStyle { get; set; }

        /// <summary>
        /// Selects between the available DataRecordView Styles.
        /// </summary>
        /// <param name="item">DataRecordViewModel.</param>
        /// <param name="container">Unused.</param>
        /// <returns>The Style to use.</returns>
        public override Style SelectStyle(object item, DependencyObject container)
        {
            if ((item as DataRecordViewModel).Type == DataRecordType.Error)
                return ErrorStyle;
            else
                return SolutionStyle;
        }
    }
}

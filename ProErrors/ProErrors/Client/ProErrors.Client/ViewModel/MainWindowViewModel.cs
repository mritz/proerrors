﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using ProErrors.Client.Library;
using ProErrors.Client.Print;
using ProErrors.Client.Proxies;
using ProErrors.Core;

namespace ProErrors.Client.ViewModel
{
    [CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class MainWindowViewModel : WorkspaceViewModel,
        IProErrorsStatusCallback
    {
        #region Delegates

        public delegate void UpdateServiceActivityDelegate(string activity);
        public delegate void UpdateServiceStatusDelegate(int year, DataRecordType type, RepositoryStatus status);

        #endregion // Delegates

        #region Fields

        // Service
        ProErrorsStatusClient _proxy;
        ViewableCollection<string> _serviceActivities;
        string _serviceActivity;
        readonly List<string> _repositoryKeys = new List<string>();
        readonly Timer _checkInTimer;
        static readonly object StatusLock = new object();

        // Display
        string _exemptEfinToAdd;
        string _exemptEfinToRemove;
        string _searchString;
        int _taxYear;
        SearchOptionsViewModel _searchOptions;

        // Commands
        ICommand _addExemptEfinCommand;
        ICommand _clearCommand;
        ICommand _closeAllCommand;
        ICommand _connectCommand;
        ICommand _copyInfoCommand;
        ICommand _deleteCommand;
        ICommand _deleteAdminCommand;
        ICommand _deleteFixedCommand;
        ICommand _dequeueCommand;
        ICommand _exportToExcelCommand;
        ICommand _openConsoleCommand;
        ICommand _printCommand;
        ICommand _queueSupportAdminCommand;
        ICommand _removeExemptEfinCommand;
        ICommand _reportEfinCommand;
        ICommand _reportErrorCommand;
        ICommand _reportTimeFrameCommand;
        ICommand _reportYearlyTrendCommand;
        ICommand _resetAdminCommand;
        ICommand _searchCommand;
        ICommand _showErrorsCommand;
        ICommand _showSolutionsCommand;
        ICommand _showSupportCommand;
        ICommand _viewCommand;

        // Collections
        List<string> _searchFields;
        readonly ViewableCollection<int> _taxYears;
        ViewableCollection<WorkspaceViewModel> _workspaces;

        #endregion // Fields

        #region Constructors

        public MainWindowViewModel()
        {
            _taxYears = new ViewableCollection<int>();

            ServiceActivities = new ViewableCollection<string>();
            base.DisplayName = Config.PRO_ERRORS_DISPLAY_NAME + ": " + Config.User;
            SearchField = SearchFields[0];

            InitializeRepositoryStatus();

            ExemptEfins = new ViewableCollection<string>();

            try { EstablishProxy(); }
            catch (CommunicationException e) { Trace.TraceWarning(e.ToString()); }
            catch (TimeoutException e) { Trace.TraceWarning(e.ToString()); }

            var keyQueue = new Queue<string>();
            foreach (string key in RepositoryStatuses.Keys)
                keyQueue.Enqueue(key);

            while (keyQueue.Count != 0)
            {
                string key = keyQueue.Dequeue();
                int year;
                Int32.TryParse(key.Substring(0, 4), out year);
                var type = (DataRecordType)Enum.Parse(typeof(DataRecordType), key.Substring(5));
                RepositoryStatuses[key] = _proxy.GetRepositoryStatus(year, type);
            }

            _checkInTimer = new Timer(ServiceCheckIn, null,
                new TimeSpan(0, 5, 0), new TimeSpan(0, 5, 0));

            #region Arbiter Registration

            #region CopyInfo

            Arbiter.Instance.Register(
                obj =>
                {
                    if (CopyInfoCommand.CanExecute(null))
                        CopyInfoCommand.Execute(null);
                }, ViewModelMessage.CopyDataRecordInformation);

            #endregion // CopyInfo

            #region Update ServiceActivity

            //Arbiter.Instance.Register(
            //    (Object o) =>
            //    {
            //        Dispatcher.FromThread(_parentThread).Invoke(
            //            new UpdateServiceActivityDelegate(this.UpdateServiceActivity),
            //            DispatcherPriority.Render,
            //            o);
            //    }, ViewModelMessage.UpdateServiceActivity);

            #endregion // Update ServiceActivity

            #region Update ServiceStatus

            //Arbiter.Instance.Register(
            //    (object o) =>
            //    {
            //        object[] args = (object[])o;
            //        Dispatcher.FromThread(_parentThread).Invoke(
            //            new UpdateServiceStatusDelegate(this.UpdateServiceStatus),
            //            DispatcherPriority.Render,
            //            args[0], args[1], args[2]);
            //    }, ViewModelMessage.UpdateServiceStatus);

            #endregion // Update ServiceStatus

            #region View DataRecordViewModel

            Arbiter.Instance.Register(
                obj =>
                {
                    if (CanView())
                        View();
                }, ViewModelMessage.ViewDataRecordViewModel);

            #endregion // View DataRecordViewModel

            #endregion // Arbiter Registration
        }

        #endregion // Constructors

        #region Public Interface

        public CommunicationState ConnectionStatus
        { get { return (_proxy != null ? _proxy.State : CommunicationState.Closed); } }

        public string ExemptEfinToAdd
        {
            get { return _exemptEfinToAdd; }
            set
            {
                if (_exemptEfinToAdd == value)
                    return;

                _exemptEfinToAdd = value;

                base.OnPropertyChanged("ExemptEfinToAdd");
            }
        }

        public string ExemptEfinToRemove
        {
            get { return _exemptEfinToRemove; }
            set
            {
                if (_exemptEfinToRemove == value)
                    return;

                _exemptEfinToRemove = value;

                base.OnPropertyChanged("ExemptEfinToRemove");
            }
        }

        public ViewableCollection<string> ExemptEfins { get; private set; }

        public ObservableDictionary<string, RepositoryStatus> RepositoryStatuses { get; private set; }

        public ViewableCollection<KeyValuePair<string, RepositoryStatus>> RepositoryStatusesError { get; set; }

        public ViewableCollection<KeyValuePair<string, RepositoryStatus>> RepositoryStatusesSolution { get; set; }

        public string SearchField { get; set; }

        public List<string> SearchFields
        {
            get
            {
                return _searchFields ?? (_searchFields =
                    new List<string>
                    {
                        "EFIN",
                        "Program",
                        "State",
                        "Error Message",
                        "Call Stack"
                    });
            }
        }

        public SearchOptionsViewModel SearchOptions
        {
            get
            {
                return _searchOptions ?? (_searchOptions =
                    new SearchOptionsViewModel(new SearchOptions()));
            }
        }

        public string SearchString
        {
            get { return _searchString; }
            set
            {
                if (value == _searchString)
                    return;

                _searchString = value;

                base.OnPropertyChanged("SearchString");
            }
        }

        public ViewableCollection<string> ServiceActivities
        {
            get { return _serviceActivities; }
            set
            {
                if (_serviceActivities == value)
                    return;

                _serviceActivities = value;

                base.OnPropertyChanged("ServiceActivities");
            }
        }

        public string ServiceActivity
        {
            get { return _serviceActivity; }
            set
            {
                if (_serviceActivity == value)
                    return;

                _serviceActivity = value;
                _serviceActivities.Add(_serviceActivity);
                //if (_serviceActivities.Count > 100)
                //    _serviceActivities.RemoveAt(0);

                base.OnPropertyChanged("ServiceActivity");
            }
        }

        public int TaxYear
        {
            get { return _taxYear; }
            set
            {
                if (_taxYear == value)
                    return;

                _taxYear = value;

                base.OnPropertyChanged("TaxYear");
            }
        }

        public ViewableCollection<int> TaxYears
        {
            get { return _taxYears; }
        }

        public string User
        {
            get { return Config.User; }
        }

        #endregion // Public Interface

        #region Commands

        #region AddExemptEfinCommand

        public ICommand AddExemptEfinCommand
        {
            get
            {
                return _addExemptEfinCommand ?? (_addExemptEfinCommand =
                    new RelayCommand(
                        param => AddExemptEfin(),
                        param => CanAddExemptEfin()));
            }
        }

        void AddExemptEfin()
        {
            Trace.TraceInformation(DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Add Exempt Efin " + ExemptEfinToAdd);

            try
            {
                _proxy.RegisterEfin(ExemptEfinToAdd);
                ExemptEfinToAdd = String.Empty;
            }
            catch (CommunicationException e) { Trace.TraceWarning(e.ToString()); }
            catch (TimeoutException e) { Trace.TraceWarning(e.ToString()); }
        }

        bool CanAddExemptEfin()
        {
            if (String.IsNullOrEmpty(ExemptEfinToAdd))
                return false;

            if (!Regex.IsMatch(ExemptEfinToAdd, "[0-9]{6}") ||
                ExemptEfins.Contains(ExemptEfinToAdd))
                return false;

            return true;
        }

        #endregion // AddExemptEfinCommand

        #region ClearCommand

        public ICommand ClearCommand
        {
            get
            {
                return _clearCommand ?? (_clearCommand =
                    new RelayCommand(
                        param => Clear(),
                        param => CanClear()));
            }
        }

        void Clear()
        {
            Trace.TraceInformation(DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Clear Search");

            SearchString = String.Empty;
        }

        bool CanClear()
        {
            return !String.IsNullOrEmpty(SearchString);
        }

        #endregion // ClearCommand

        #region CloseAllCommand

        public ICommand CloseAllCommand
        {
            get
            {
                return _closeAllCommand ?? (_closeAllCommand =
                    new RelayCommand(
                        param => CloseAll(),
                        param => CanCloseAll()));
            }
        }

        void CloseAll()
        {
            Trace.TraceInformation(DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Close All");

            for (int x = Workspaces.Count - 1; x >= 0; x--)
                if (Workspaces[x].CloseCommand.CanExecute(null))
                    Workspaces[x].CloseCommand.Execute(null);
        }

        bool CanCloseAll()
        {
            return Workspaces.Count > 0;
        }

        #endregion // CloseAllCommand

        #region ConnectCommand

        public ICommand ConnectCommand
        {
            get
            {
                return _connectCommand ?? (_connectCommand =
                    new RelayCommand(
                        param => Connect(),
                        param => CanConnect()));
            }
        }

        void Connect()
        {
            Trace.TraceInformation(DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Connect to Service");

            EstablishProxy();
        }

        bool CanConnect()
        {
            if (ConnectionStatus == CommunicationState.Closed ||
                ConnectionStatus == CommunicationState.Closing ||
                ConnectionStatus == CommunicationState.Faulted)
                return true;

            return false;
        }

        #endregion // ConnectCommand

        #region CopyInfoCommand

        public ICommand CopyInfoCommand
        {
            get
            {
                return _copyInfoCommand ?? (_copyInfoCommand =
                    new RelayCommand(
                        param => CopyInfo(),
                        param => CanCopyInfo()));
            }
        }

        void CopyInfo()
        {
            WorkspaceViewModel workspace = GetActiveWorkspace();
            DataRecordViewModel drVM;

            if (workspace is AllDataRecordsViewModel)
                drVM = (DataRecordViewModel)(workspace as AllDataRecordsViewModel).FilteredDataRecords.CurrentItem;
            else
                drVM = (workspace as DataRecordViewModel);

            Debug.Assert(drVM != null, "drVM != null");
            Trace.TraceInformation(DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Copy Info for " + drVM.Efin);

            string copy =
                "Program: " +
                drVM.Program + "\r\n" +
                "Error: " +
                drVM.ErrNum + "\r\n" +
                "Line: " +
                drVM.LineNum + "\r\n" +
                "Message: " +
                drVM.Message + "\r\n" +
                "Stack:\r\n" +
                drVM.Stack;

            Clipboard.SetText(copy);
        }

        bool CanCopyInfo()
        {
            WorkspaceViewModel workspace = GetActiveWorkspace();

            if (workspace == null)
                return false;

            if (workspace is AllDataRecordsViewModel ||
                workspace is DataRecordViewModel)
                return true;

            return false;
        }

        #endregion // CopyInfoCommand

        #region DeleteCommand

        public ICommand DeleteCommand
        {
            get
            {
                return _deleteCommand ?? (_deleteCommand =
                    new RelayCommand(
                        param => Delete(),
                        param => CanDelete()));
            }
        }

        void Delete()
        {
            Trace.TraceInformation(DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Delete");

            WorkspaceViewModel workspace = GetActiveWorkspace();

            var deletionList = new List<DataRecordViewModel>();

            Trace.Indent();
            foreach (DataRecordViewModel drVM in ((AllDataRecordsViewModel)workspace).AllDataRecords)
            {
                if (drVM.IsSelected)
                {
                    deletionList.Add(drVM);
                    Trace.TraceInformation(drVM.Efin);
                }
                if (deletionList.Count >= 10)
                    break;
            }
            Trace.IndentLevel = 0;

            for (int x = deletionList.Count - 1; x >= 0; x--)
            {
                if (deletionList[x].IsFixed)
                {
                    ((AllDataRecordsViewModel)workspace).MarkFixed(deletionList[x]);
                    //Arbiter.Instance.NotifyColleagues(
                    //    ViewModelMessage.MarkFixedDataRecordViewModel,
                    //    new object[3] { year, type, deletionList[x] });
                }
                else
                {
                    var drVM =
                        Workspaces.FirstOrDefault(vm => vm is DataRecordViewModel &&
                            (vm as DataRecordViewModel).Equals(deletionList[x]))
                            as DataRecordViewModel;

                    Debug.Assert(drVM != null, "drVM != null");
                    if (!Workspaces.Contains(drVM))
                    {
                        deletionList[x].BeginEdit();
                        Workspaces.Add(deletionList[x]);
                        SetActiveWorkspace(deletionList[x]);
                    }
                }
            }
        }

        bool CanDelete()
        {
            WorkspaceViewModel workspace = GetActiveWorkspace();

            if (workspace == null)
                return false;

            if (!(workspace is AllDataRecordsViewModel))
                return false;

            string key = (workspace as AllDataRecordsViewModel).Year.ToString(CultureInfo.InvariantCulture) + " " +
                (workspace as AllDataRecordsViewModel).Type.ToString();

            if (key.Contains("Search"))
                key = key.Split(new[] { "Search" }, StringSplitOptions.None)[0];

            if (key.EndsWith("s"))
                key = key.TrimEnd('s');

            if (RepositoryStatuses[key] == RepositoryStatus.Idle &&
                Config.IsUserProgrammer() &&
                (((int)(workspace as AllDataRecordsViewModel).Type) / 3) == 0 &&
                (workspace as AllDataRecordsViewModel).AllDataRecords.Any(
                    drVM => drVM.IsSelected))
                return true;

            return false;
        }

        #endregion // DeleteCommand

        #region DeleteAdminCommand

        public ICommand DeleteAdminCommand
        {
            get
            {
                return _deleteAdminCommand ?? (_deleteAdminCommand =
                    new RelayCommand(
                        param => DeleteAdmin(),
                        param => CanDeleteAdmin()));
            }
        }

        void DeleteAdmin()
        {
            Trace.TraceInformation(DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": DeleteAdmin");

            WorkspaceViewModel workspace = GetActiveWorkspace();

            var deletionList = new List<DataRecordViewModel>();

            Trace.Indent();
            foreach (DataRecordViewModel drVM in ((AllDataRecordsViewModel)workspace).AllDataRecords)
            {
                if (drVM.IsSelected)
                {
                    deletionList.Add(drVM);
                    Trace.TraceInformation(drVM.Efin);
                }
            }
            Trace.IndentLevel = 0;

            int year = ((AllDataRecordsViewModel)workspace).Year;

            if (year <= 2010)
                ((AllDataRecordsViewModel)workspace).MarkFixed(deletionList);
            else
                ((AllDataRecordsViewModel)workspace).MarkIrrelevant(deletionList);
            //Arbiter.Instance.NotifyColleagues(
            //    ViewModelMessage.MarkFixedDataRecordViewModel,
            //    new object[3] { year, type, deletionList });
        }

        bool CanDeleteAdmin()
        {
            WorkspaceViewModel workspace = GetActiveWorkspace();

            if (workspace == null)
                return false;

            if (!(workspace is AllDataRecordsViewModel))
                return false;

            string key = (workspace as AllDataRecordsViewModel).Year.ToString(CultureInfo.InvariantCulture) + " " +
                (workspace as AllDataRecordsViewModel).Type.ToString();

            if (key.Contains("Search"))
                key = key.Split(new[] { "Search" }, StringSplitOptions.None)[0];

            if (key.EndsWith("s"))
                key = key.TrimEnd('s');

            if (RepositoryStatuses[key] == RepositoryStatus.Idle &&
                Config.IsUserAdministrator() &&
                //(workspace as AllDataRecordsViewModel).Year <= 2010 &&
                ((((int)(workspace as AllDataRecordsViewModel).Type) / 3) == 0 ||
                ((int)(workspace as AllDataRecordsViewModel).Type / 3) == 2) &&
                (workspace as AllDataRecordsViewModel).AllDataRecords.Any(
                    drVM => drVM.IsSelected))
                return true;

            return false;
        }

        #endregion // DeleteAdminCommand

        #region DeleteFixedCommand

        public ICommand DeleteFixedCommand
        {
            get
            {
                return _deleteFixedCommand ?? (_deleteFixedCommand =
                    new RelayCommand(
                        param => DeleteFixed(),
                        param => CanDeleteFixed()));
            }
        }

        void DeleteFixed()
        {
            Trace.TraceInformation(DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": DeleteFixed");

            WorkspaceViewModel workspace = GetActiveWorkspace();

            var deletionList = new List<DataRecordViewModel>();

            Trace.Indent();
            foreach (DataRecordViewModel drVM in ((AllDataRecordsViewModel)workspace).FilteredDataRecords)
            {
                if (drVM.IsFixed && drVM.Type == DataRecordType.Error)
                {
                    deletionList.Add(drVM);
                    Trace.TraceInformation(drVM.Efin);
                }
            }
            Trace.IndentLevel = 0;

            //WorkspaceViewModel mainWorkspace =
            //    this.Workspaces.FirstOrDefault(vm => vm is AllDataRecordsViewModel &&
            //        (vm as AllDataRecordsViewModel).Year == year &&
            //        (vm as AllDataRecordsViewModel).Type == DataRecordRepositoryType.Errors);

            ((AllDataRecordsViewModel)workspace).MarkFixed(deletionList);

            //Arbiter.Instance.NotifyColleagues(
            //    ViewModelMessage.MarkFixedDataRecordViewModel,
            //    new object[3] { year, type, deletionList });

            //for (int x = deletionList.Count - 1; x >= 0; x--)
            //    deletionList[x].Delete();
        }

        bool CanDeleteFixed()
        {
            WorkspaceViewModel workspace = GetActiveWorkspace();

            if (workspace == null)
                return false;

            if (!(workspace is AllDataRecordsViewModel))
                return false;

            string key = (workspace as AllDataRecordsViewModel).Year.ToString(CultureInfo.InvariantCulture) + " " +
                (workspace as AllDataRecordsViewModel).Type.ToString();

            if (key.Contains("Search"))
                key = key.Split(new[] { "Search" }, StringSplitOptions.None)[0];

            if (key.EndsWith("s"))
                key = key.TrimEnd('s');

            if (RepositoryStatuses[key] == RepositoryStatus.Idle &&
                Config.IsUserProgrammer() &&
                (((int)(workspace as AllDataRecordsViewModel).Type) / 3) == 0 &&
                (workspace as AllDataRecordsViewModel).AllDataRecords.Any(
                    drVM => drVM.IsFixed && drVM.Type == DataRecordType.Error))
                return true;

            return false;
        }

        #endregion // DeleteFixedCommand

        #region DequeueCommand

        public ICommand DequeueCommand
        {
            get
            {
                return _dequeueCommand ?? (_dequeueCommand =
                    new RelayCommand(
                        param => Dequeue(),
                        param => CanDequeue()));
            }
        }

        void Dequeue()
        {
            DeleteAdmin();
        }

        bool CanDequeue()
        {
            WorkspaceViewModel workspace = GetActiveWorkspace();

            if (workspace == null)
                return false;

            if (!(workspace is AllDataRecordsViewModel))
                return false;

            string key = (workspace as AllDataRecordsViewModel).Year.ToString(CultureInfo.InvariantCulture) + " " +
                (workspace as AllDataRecordsViewModel).Type.ToString();

            if (key.Contains("Search"))
                key = key.Split(new[] { "Search" }, StringSplitOptions.None)[0];

            if (key.EndsWith("s"))
                key = key.TrimEnd('s');

            if (RepositoryStatuses[key] == RepositoryStatus.Idle &&
                //(workspace as AllDataRecordsViewModel).Year <= 2010 &&
                (((int)(workspace as AllDataRecordsViewModel).Type / 3) == 2) &&
                (workspace as AllDataRecordsViewModel).AllDataRecords.Any(
                    drVM => drVM.IsSelected))
                return true;

            return false;
        }

        #endregion

        #region ExportToExcelCommand

        public ICommand ExportToExcelCommand
        {
            get
            {
                return _exportToExcelCommand ?? (_exportToExcelCommand =
                    new RelayCommand(
                        param => ExportToExcel(),
                        param => CanExportToExcel()));
            }
        }

        void ExportToExcel()
        {
            var workspace = (AllDataRecordsViewModel)GetActiveWorkspace();

            var data = new string[workspace.AllDataRecords.Count + 1, 7];
            int itr = 1;

            data[0, 0] = "EFIN";
            data[0, 1] = "Timestamp";
            data[0, 2] = "I. Version";
            data[0, 3] = "B. Version";
            data[0, 4] = "Program";
            data[0, 5] = "State";
            data[0, 6] = "Message";

            foreach (DataRecordViewModel drVM in workspace.AllDataRecords)
            {
                data[itr, 0] = drVM.Efin;
                data[itr, 1] = drVM.TimeStamp.ToString(CultureInfo.InvariantCulture);
                data[itr, 2] = drVM.IVersion;
                data[itr, 3] = drVM.BVersion;
                data[itr, 4] = drVM.Program;
                data[itr, 5] = drVM.StateAbbreviation.ToString();
                data[itr, 6] = drVM.Message;
                itr++;
            }

            var sfd = new System.Windows.Forms.SaveFileDialog { AddExtension = true, Filter = "Excel Files (*.xls)|*.xls|All files (*.*)|*.*" };

            if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string file = sfd.FileName;

                var excel = new ExcelExporter(file);
                excel.CreateSpreadsheet(data);
                excel.AddExcelBellsAndWhistles(file);
            }
        }

        bool CanExportToExcel()
        {
            WorkspaceViewModel workspace = GetActiveWorkspace();

            if (workspace == null)
                return false;

            return workspace is AllDataRecordsViewModel &&
                ((workspace as AllDataRecordsViewModel).Type == DataRecordRepositoryType.Errors ||
                (workspace as AllDataRecordsViewModel).Type == DataRecordRepositoryType.ErrorsCluster ||
                (workspace as AllDataRecordsViewModel).Type == DataRecordRepositoryType.Solutions ||
                (workspace as AllDataRecordsViewModel).Type == DataRecordRepositoryType.SolutionsCluster);
        }

        #endregion // ExportToExcelCommand

        #region OpenConsoleCommand

        public ICommand OpenConsoleCommand
        {
            get
            {
                return _openConsoleCommand ?? (_openConsoleCommand =
                    new RelayCommand(
                        param => OpenConsole()));
            }
        }

        void OpenConsole()
        {
            throw new NotImplementedException();
        }

        #endregion // OpenConsoleCommand

        #region PrintCommand

        public ICommand PrintCommand
        {
            get
            {
                return _printCommand ?? (_printCommand =
                    new RelayCommand(
                        param => Print(),
                        param => CanPrint()));
            }
        }

        void Print()
        {
            Trace.TraceInformation(DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Print");

            WorkspaceViewModel workspace = GetActiveWorkspace();

            if (workspace is AllDataRecordsViewModel)
            {
                var drVMList = (workspace as AllDataRecordsViewModel).AllDataRecords.Where(drVM => drVM.IsSelected).ToList();

                PrintManager.PrintFlowDocument(drVMList);
            }
            else if (workspace is DataRecordViewModel)
            {
                PrintManager.PrintFlowDocument(
                    new List<DataRecordViewModel> { (workspace as DataRecordViewModel) });
            }
        }

        bool CanPrint()
        {
            WorkspaceViewModel workspace = GetActiveWorkspace();

            if (workspace == null)
                return false;

            if (workspace is AllDataRecordsViewModel &&
                (workspace as AllDataRecordsViewModel).AllDataRecords.Any(
                    drVM => drVM.IsSelected))
                return true;

            if (workspace is DataRecordViewModel)
                return true;

            return false;
        }

        #endregion // PrintCommand

        #region QueueSupportAdminCommand

        public ICommand QueueSupportAdminCommand
        {
            get
            {
                return _queueSupportAdminCommand ?? (_queueSupportAdminCommand =
                    new RelayCommand(
                        param => QueueSupportAdmin(),
                        param => CanQueueSupportAdmin()));
            }
        }

        void QueueSupportAdmin()
        {
            Trace.TraceInformation(DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": QueueSupportAdmin");

            WorkspaceViewModel workspace = GetActiveWorkspace();

            var deletionList = new List<DataRecordViewModel>();

            Trace.Indent();
            foreach (DataRecordViewModel drVM in ((AllDataRecordsViewModel)workspace).AllDataRecords)
            {
                if (drVM.IsSelected)
                {
                    deletionList.Add(drVM);
                    Trace.TraceInformation(drVM.Efin);
                }
            }
            Trace.IndentLevel = 0;

            int year = ((AllDataRecordsViewModel)workspace).Year;

            if (year <= 2010)
                ((AllDataRecordsViewModel)workspace).MarkFixed(deletionList);
            else
                ((AllDataRecordsViewModel)workspace).MarkSupport(deletionList);
        }

        bool CanQueueSupportAdmin()
        {
            WorkspaceViewModel workspace = GetActiveWorkspace();

            if (workspace == null)
                return false;

            if (!(workspace is AllDataRecordsViewModel))
                return false;

            string key = (workspace as AllDataRecordsViewModel).Year.ToString(CultureInfo.InvariantCulture) + " " +
                (workspace as AllDataRecordsViewModel).Type.ToString();

            if (key.Contains("Search"))
                key = key.Split(new[] { "Search" }, StringSplitOptions.None)[0];

            if (key.EndsWith("s"))
                key = key.TrimEnd('s');

            if (RepositoryStatuses[key] == RepositoryStatus.Idle &&
                Config.IsUserAdministrator() &&
                //(workspace as AllDataRecordsViewModel).Year <= 2010 &&
                (((int)(workspace as AllDataRecordsViewModel).Type) / 3) == 0 &&
                (workspace as AllDataRecordsViewModel).AllDataRecords.Any(
                    drVM => drVM.IsSelected))
                return true;

            return false;
        }

        #endregion

        #region RemoveExemptEfinCommand

        public ICommand RemoveExemptEfinCommand
        {
            get
            {
                return _removeExemptEfinCommand ?? (_removeExemptEfinCommand =
                    new RelayCommand(
                        param => RemoveExemptEfin(),
                        param => CanRemoveExemptEfin()));
            }
        }

        void RemoveExemptEfin()
        {
            Trace.TraceInformation(DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Remove Exempt Efin " + ExemptEfinToRemove);

            try { _proxy.UnregisterEfin(ExemptEfinToRemove); }
            catch (CommunicationException e) { Trace.TraceWarning(e.ToString()); }
            catch (TimeoutException e) { Trace.TraceWarning(e.ToString()); }
        }

        bool CanRemoveExemptEfin()
        {
            if (!String.IsNullOrEmpty(ExemptEfinToRemove))
                return true;

            return false;
        }

        #endregion // RemoveExemptEfinCommand

        #region ReportEfinCommand

        public ICommand ReportEfinCommand
        {
            get
            {
                return _reportEfinCommand ?? (_reportEfinCommand =
                    new RelayCommand(
                        param => ReportEfin(),
                        param => CanReportEfin()));
            }
        }

        void ReportEfin()
        {
            Trace.TraceInformation(DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Report Efin");

            var workspace =
                Workspaces.FirstOrDefault(vm => vm is ReportEfinViewModel &&
                    (vm as ReportEfinViewModel).Year == TaxYear)
                    as ReportEfinViewModel;

            try
            {
                if (workspace == null)
                {
                    if (_proxy.RepositoryExists(TaxYear, DataRecordType.Error) &&
                        _proxy.RepositoryExists(TaxYear, DataRecordType.Solution))
                        workspace = new ReportEfinViewModel(TaxYear);

                    Workspaces.Add(workspace);
                }

                SetActiveWorkspace(workspace);
            }
            catch (CommunicationException e) { Trace.TraceWarning(e.ToString()); }
            catch (TimeoutException e) { Trace.TraceWarning(e.ToString()); }
        }

        bool CanReportEfin()
        {
            if (TaxYear == 0 ||
                ConnectionStatus != CommunicationState.Opened ||
                RepositoryStatuses[TaxYear.ToString(CultureInfo.InvariantCulture) + " Error"] != RepositoryStatus.Idle ||
                RepositoryStatuses[TaxYear.ToString(CultureInfo.InvariantCulture) + " Solution"] != RepositoryStatus.Idle)
                return false;

            var workspace =
                Workspaces.FirstOrDefault(vm => vm is ReportEfinViewModel &&
                    (vm as ReportEfinViewModel).Year == TaxYear)
                    as ReportEfinViewModel;

            Debug.Assert(workspace != null, "workspace != null");
            return !Workspaces.Contains(workspace);
        }

        #endregion // ReportEfinCommand

        #region ReportErrorCommand

        public ICommand ReportErrorCommand
        {
            get
            {
                return _reportErrorCommand ?? (_reportErrorCommand =
                    new RelayCommand(
                        param => ReportError(),
                        param => CanReportError()));
            }
        }

        void ReportError()
        {
            Trace.TraceInformation(DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Report Error");

            var workspace =
                Workspaces.FirstOrDefault(vm => vm is ReportErrorViewModel &&
                    (vm as ReportErrorViewModel).Year == TaxYear)
                    as ReportErrorViewModel;

            try
            {
                if (workspace == null)
                {
                    if (_proxy.RepositoryExists(TaxYear, DataRecordType.Error) &&
                        _proxy.RepositoryExists(TaxYear, DataRecordType.Solution))
                        workspace = new ReportErrorViewModel(TaxYear);

                    Workspaces.Add(workspace);
                }

                SetActiveWorkspace(workspace);
            }
            catch (CommunicationException e) { Trace.TraceWarning(e.ToString()); }
            catch (TimeoutException e) { Trace.TraceWarning(e.ToString()); }
        }

        bool CanReportError()
        {
            if (TaxYear == 0 ||
                ConnectionStatus != CommunicationState.Opened ||
                RepositoryStatuses[TaxYear.ToString(CultureInfo.InvariantCulture) + " Error"] != RepositoryStatus.Idle ||
                RepositoryStatuses[TaxYear.ToString(CultureInfo.InvariantCulture) + " Solution"] != RepositoryStatus.Idle)
                return false;

            var workspace =
                Workspaces.FirstOrDefault(vm => vm is ReportErrorViewModel &&
                    (vm as ReportErrorViewModel).Year == TaxYear)
                    as ReportErrorViewModel;

            Debug.Assert(workspace != null, "workspace != null");
            return !Workspaces.Contains(workspace);
        }

        #endregion // ReportErrorCommand

        #region ReportTimeFrameCommand

        public ICommand ReportTimeFrameCommand
        {
            get
            {
                return _reportTimeFrameCommand ?? (_reportTimeFrameCommand =
                    new RelayCommand(
                        param => ReportTimeFrame(),
                        param => CanReportTimeFrame()));
            }
        }

        void ReportTimeFrame()
        {
            Trace.TraceInformation(DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Report Time Frame");

            var workspace =
                 Workspaces.FirstOrDefault(vm => vm is ReportTimeFrameViewModel &&
                     (vm as ReportTimeFrameViewModel).Year == TaxYear)
                     as ReportTimeFrameViewModel;

            try
            {
                if (workspace == null)
                {
                    if (_proxy.RepositoryExists(TaxYear, DataRecordType.Error) &&
                        _proxy.RepositoryExists(TaxYear, DataRecordType.Solution))
                        workspace = new ReportTimeFrameViewModel(TaxYear);

                    Workspaces.Add(workspace);
                }

                SetActiveWorkspace(workspace);
            }
            catch (CommunicationException e) { Trace.TraceWarning(e.ToString()); }
            catch (TimeoutException e) { Trace.TraceWarning(e.ToString()); }
        }

        bool CanReportTimeFrame()
        {
            if (TaxYear == 0 ||
                ConnectionStatus != CommunicationState.Opened ||
                RepositoryStatuses[TaxYear.ToString(CultureInfo.InvariantCulture) + " Error"] != RepositoryStatus.Idle ||
                RepositoryStatuses[TaxYear.ToString(CultureInfo.InvariantCulture) + " Solution"] != RepositoryStatus.Idle)
                return false;

            var workspace =
                Workspaces.FirstOrDefault(vm => vm is ReportTimeFrameViewModel &&
                    (vm as ReportTimeFrameViewModel).Year == TaxYear)
                    as ReportTimeFrameViewModel;

            Debug.Assert(workspace != null, "workspace != null");
            return !Workspaces.Contains(workspace);
        }

        #endregion // ReportTimeFrameCommand

        #region ReportYearlyTrendCommand

        public ICommand ReportYearlyTrendCommand
        {
            get
            {
                return _reportYearlyTrendCommand ?? (_reportYearlyTrendCommand =
                    new RelayCommand(
                        param => ReportYearlyTrend(),
                        param => CanReportYearlyTrend()));
            }
        }

        void ReportYearlyTrend()
        {
            throw new NotImplementedException();
        }

        bool CanReportYearlyTrend()
        {
            return true;
        }

        #endregion // ReportYearlyTrendCommand

        #region ResetAdminCommand

        public ICommand ResetAdminCommand
        {
            get
            {
                return _resetAdminCommand ?? (_resetAdminCommand =
                    new RelayCommand(
                        param => ResetAdmin(),
                        param => CanResetAdmin()));
            }
        }

        void ResetAdmin()
        {
            Trace.TraceInformation(DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": ResetAdmin");

            if (CanCloseAll())
                CloseAll();

            try { _proxy.ResetMasterRepository(); }
            catch (CommunicationException e) { Trace.TraceWarning(e.ToString()); }
            catch (TimeoutException e) { Trace.TraceWarning(e.ToString()); }
        }

        bool CanResetAdmin()
        {
            if (Config.IsUserAdministrator() &&
                _proxy.State == CommunicationState.Opened)
                return true;

            return false;
        }

        #endregion

        #region SearchCommand

        public ICommand SearchCommand
        {
            get
            {
                return _searchCommand ?? (_searchCommand =
                    new RelayCommand(
                        param => Search(),
                        param => CanSearch()));
            }
        }

        void Search()
        {
            Trace.TraceInformation(DateTime.Now.ToString(CultureInfo.InvariantCulture) +
                ": Search " + SearchField + " -> " + SearchString);

            var workspace = (AllDataRecordsViewModel)GetActiveWorkspace();

            AllDataRecordsViewModel searchWorkspace =
                workspace.CreatedSearchWorkspace(SearchField, SearchString, SearchOptions);

            //AllDataRecordsViewModel searchWorkspace =
            //    new AllDataRecordsViewModel(
            //        workspace.Year,
            //        (DataRecordType)(((int)workspace.Type) / 3),
            //        workspace.AllDataRecords,
            //        this.SearchField,
            //        this.SearchString);

            Workspaces.Add(searchWorkspace);
            SetActiveWorkspace(searchWorkspace);
        }

        bool CanSearch()
        {
            WorkspaceViewModel workspace = GetActiveWorkspace();

            if (workspace == null)
                return false;

            return !String.IsNullOrEmpty(SearchField) &&
                !String.IsNullOrEmpty(SearchString) &&
                workspace is AllDataRecordsViewModel &&
                ((workspace as AllDataRecordsViewModel).Type == DataRecordRepositoryType.Errors ||
                (workspace as AllDataRecordsViewModel).Type == DataRecordRepositoryType.ErrorsCluster ||
                (workspace as AllDataRecordsViewModel).Type == DataRecordRepositoryType.Solutions ||
                (workspace as AllDataRecordsViewModel).Type == DataRecordRepositoryType.SolutionsCluster);
        }

        #endregion // SearchCommand

        #region ShowErrorsCommand

        public ICommand ShowErrorsCommand
        {
            get
            {
                return _showErrorsCommand ?? (_showErrorsCommand =
                    new RelayCommand(
                        param => ShowErrors(),
                        param => CanShowErrors()));
            }
        }

        void ShowErrors()
        {
            Trace.TraceInformation(DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Show Errors " + TaxYear.ToString(CultureInfo.InvariantCulture));

            var workspace =
                Workspaces.FirstOrDefault(vm => vm is AllDataRecordsViewModel &&
                    (vm as AllDataRecordsViewModel).Year == TaxYear &&
                    (vm as AllDataRecordsViewModel).Type == DataRecordRepositoryType.Errors)
                    as AllDataRecordsViewModel;

            try
            {
                if (workspace == null)
                {
                    if (_proxy.RepositoryExists(TaxYear, DataRecordType.Error))
                        workspace = new AllDataRecordsViewModel(
                            TaxYear, DataRecordType.Error);

                    Workspaces.Add(workspace);
                }

                SetActiveWorkspace(workspace);
            }
            catch (CommunicationException e) { Trace.TraceWarning(e.ToString()); }
            catch (TimeoutException e) { Trace.TraceWarning(e.ToString()); }
        }

        bool CanShowErrors()
        {
            try
            {
                string key = TaxYear.ToString(CultureInfo.InvariantCulture) + " Error";

                if (TaxYear == 0 ||
                    !_repositoryKeys.Contains(key) ||
                    RepositoryStatuses[key] != RepositoryStatus.Idle ||
                    ConnectionStatus != CommunicationState.Opened)
                    return false;

                var workspace =
                    Workspaces.FirstOrDefault(vm => vm is AllDataRecordsViewModel &&
                        (vm as AllDataRecordsViewModel).Year == TaxYear &&
                        (vm as AllDataRecordsViewModel).Type == DataRecordRepositoryType.Errors)
                        as AllDataRecordsViewModel;

                Debug.Assert(workspace != null, "workspace != null");
                return !Workspaces.Contains(workspace);
            }
            catch (CommunicationException e) { Trace.TraceWarning(e.ToString()); }
            catch (TimeoutException e) { Trace.TraceWarning(e.ToString()); }

            return false;
        }

        #endregion // ShowErrorsCommand

        #region ShowSolutionsCommand

        public ICommand ShowSolutionsCommand
        {
            get
            {
                return _showSolutionsCommand ?? (_showSolutionsCommand =
                    new RelayCommand(
                        param => ShowSolutions(),
                        param => CanShowSolutions()));
            }
        }

        void ShowSolutions()
        {
            Trace.TraceInformation(DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Show Solutions " + TaxYear.ToString(CultureInfo.InvariantCulture));

            var workspace =
                Workspaces.FirstOrDefault(vm => vm is AllDataRecordsViewModel &&
                    (vm as AllDataRecordsViewModel).Year == TaxYear &&
                    (vm as AllDataRecordsViewModel).Type == DataRecordRepositoryType.Solutions)
                    as AllDataRecordsViewModel;

            try
            {
                if (workspace == null)
                {
                    if (_proxy.RepositoryExists(TaxYear, DataRecordType.Solution))
                        workspace = new AllDataRecordsViewModel(
                            TaxYear, DataRecordType.Solution);

                    Workspaces.Add(workspace);
                }

                SetActiveWorkspace(workspace);
            }
            catch (CommunicationException e) { Trace.TraceWarning(e.ToString()); }
            catch (TimeoutException e) { Trace.TraceWarning(e.ToString()); }
        }

        bool CanShowSolutions()
        {
            try
            {
                string key = TaxYear.ToString(CultureInfo.InvariantCulture) + " Solution";

                if (TaxYear == 0 ||
                    !_repositoryKeys.Contains(key) ||
                    RepositoryStatuses[key] != RepositoryStatus.Idle ||
                    ConnectionStatus != CommunicationState.Opened)
                    return false;

                var workspace =
                    Workspaces.FirstOrDefault(vm => vm is AllDataRecordsViewModel &&
                        (vm as AllDataRecordsViewModel).Year == TaxYear &&
                        (vm as AllDataRecordsViewModel).Type == DataRecordRepositoryType.Solutions)
                        as AllDataRecordsViewModel;

                Debug.Assert(workspace != null, "workspace != null");
                return !Workspaces.Contains(workspace);
            }
            catch (CommunicationException e) { Trace.TraceWarning(e.ToString()); }
            catch (TimeoutException e) { Trace.TraceWarning(e.ToString()); }

            return false;
        }

        #endregion // ShowSolutionsCommand

        #region ShowSupportCommand

        public ICommand ShowSupportCommand
        {
            get
            {
                return _showSupportCommand ?? (_showSupportCommand =
                    new RelayCommand(
                        param => ShowSupport(),
                        param => CanShowSupport()));
            }
        }

        void ShowSupport()
        {
            Trace.TraceInformation(DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": Show Support " + TaxYear.ToString(CultureInfo.InvariantCulture));

            var workspace =
                Workspaces.FirstOrDefault(vm => vm is AllDataRecordsViewModel &&
                    (vm as AllDataRecordsViewModel).Year == TaxYear &&
                    (vm as AllDataRecordsViewModel).Type == DataRecordRepositoryType.Support)
                    as AllDataRecordsViewModel;

            try
            {
                if (workspace == null)
                {
                    if (_proxy.RepositoryExists(TaxYear, DataRecordType.Support))
                        workspace = new AllDataRecordsViewModel(
                            TaxYear, DataRecordType.Support);

                    Workspaces.Add(workspace);
                }

                SetActiveWorkspace(workspace);
            }
            catch (CommunicationException e) { Trace.TraceWarning(e.ToString()); }
            catch (TimeoutException e) { Trace.TraceWarning(e.ToString()); }
        }

        bool CanShowSupport()
        {
            try
            {
                string key = TaxYear.ToString(CultureInfo.InvariantCulture) + " Support";

                if (TaxYear == 0 ||
                    !_repositoryKeys.Contains(key) ||
                    RepositoryStatuses[key] != RepositoryStatus.Idle ||
                    ConnectionStatus != CommunicationState.Opened)
                    return false;

                var workspace =
                    Workspaces.FirstOrDefault(vm => vm is AllDataRecordsViewModel &&
                        (vm as AllDataRecordsViewModel).Year == TaxYear &&
                        (vm as AllDataRecordsViewModel).Type == DataRecordRepositoryType.Support)
                        as AllDataRecordsViewModel;

                Debug.Assert(workspace != null, "workspace != null");
                return !Workspaces.Contains(workspace);
            }
            catch (CommunicationException e) { Trace.TraceWarning(e.ToString()); }
            catch (TimeoutException e) { Trace.TraceWarning(e.ToString()); }

            return false;
        }

        #endregion // ShowSupportCommand

        #region ViewCommand

        public ICommand ViewCommand
        {
            get
            {
                return _viewCommand ?? (_viewCommand =
                    new RelayCommand(
                        param => View(),
                        param => CanView()));
            }
        }

        void View()
        {
            WorkspaceViewModel workspace = GetActiveWorkspace();

            if (workspace.GetType() == typeof(AllDataRecordsViewModel))
            {
                foreach (DataRecordViewModel drVM in ((AllDataRecordsViewModel)workspace).FilteredDataRecords)
                {
                    if (drVM.IsSelected)
                    {
                        Trace.TraceInformation(DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": View " + drVM.Efin);

                        if (drVM.Type == DataRecordType.Error)
                            drVM.BeginEdit();
                        Workspaces.Add(drVM);
                        SetActiveWorkspace(drVM);
                    }
                }
            }
        }

        bool CanView()
        {
            WorkspaceViewModel workspace = GetActiveWorkspace();

            if (workspace == null)
                return false;

            if (workspace.GetType() == typeof(AllDataRecordsViewModel))
                return ((AllDataRecordsViewModel)workspace).AllDataRecords.Any(drVM => drVM.IsSelected);

            return false;
        }

        #endregion // ViewCommand

        #endregion // Commands

        #region Workspaces

        public ViewableCollection<WorkspaceViewModel> Workspaces
        {
            get
            {
                if (_workspaces == null)
                {
                    _workspaces = new ViewableCollection<WorkspaceViewModel>();
                    _workspaces.CollectionChanged += OnWorkspacesChanged;
                }
                return _workspaces;
            }
        }

        void OnWorkspacesChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            // New Workspace added
            if (e.NewItems != null && e.NewItems.Count != 0)
                foreach (WorkspaceViewModel workspace in e.NewItems)
                    workspace.RequestClose += OnWorkspaceRequestClose;

            // Old Workspace removed
            if (e.OldItems != null && e.OldItems.Count != 0)
                foreach (WorkspaceViewModel workspace in e.OldItems)
                    workspace.RequestClose -= OnWorkspaceRequestClose;
        }

        void OnWorkspaceRequestClose(object sender, EventArgs e)
        {
            var workspace = sender as WorkspaceViewModel;
            if (workspace != null)
            {
                workspace.Dispose();
                Workspaces.Remove(workspace);
            }
        }

        WorkspaceViewModel GetActiveWorkspace()
        {
            ICollectionView collectionView = CollectionViewSource.GetDefaultView(Workspaces);
            if (collectionView != null)
                return (WorkspaceViewModel)collectionView.CurrentItem;

            return null;
        }

        void SetActiveWorkspace(WorkspaceViewModel workspace)
        {
            Debug.Assert(Workspaces.Contains(workspace));

            ICollectionView collectionView = CollectionViewSource.GetDefaultView(Workspaces);
            if (collectionView != null)
                collectionView.MoveCurrentTo(workspace);
        }

        #endregion // Workspaces

        #region Event Handlers

        protected override void OnRequestClose()
        {
            try { _proxy.UnregisterForStatusUpdates(); }
            catch (CommunicationException e) { Trace.TraceWarning(e.ToString()); }
            catch (TimeoutException e) { Trace.TraceWarning(e.ToString()); }

            if (_checkInTimer != null)
                _checkInTimer.Dispose();

            for (int i = Workspaces.Count - 1; i >= 0; i--)
                if (Workspaces[i] != null)
                    Workspaces[i].Dispose();

            base.OnRequestClose();
        }

        void ProxyClosed(object sender, EventArgs e)
        {
            base.OnPropertyChanged("ConnectionStatus");
        }

        void ProxyClosing(object sender, EventArgs e)
        {
            base.OnPropertyChanged("ConnectionStatus");
        }

        void ProxyFaulted(object sender, EventArgs e)
        {
            base.OnPropertyChanged("ConnectionStatus");

            RepositoryStatuses.CollectionChanged +=
                RepositoryStatusesCollectionChanged;

            ExemptEfins.Clear();
            TaxYear = 0;
            TaxYears.Clear();
            _repositoryKeys.Clear();
            RepositoryStatuses.Clear();
            RepositoryStatusesError.Clear();
            RepositoryStatusesSolution.Clear();

            base.OnPropertyChanged("ExemptEfins");
        }

        void ProxyOpened(object sender, EventArgs e)
        {
            base.OnPropertyChanged("ConnectionStatus");
        }

        void ProxyOpening(object sender, EventArgs e)
        {
            base.OnPropertyChanged("ConnectionStatus");
        }

        void RepositoryStatusesCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
            {
                foreach (KeyValuePair<string, RepositoryStatus> item in e.OldItems)
                {
                    if (item.Key.Contains("Error"))
                        RepositoryStatusesError.Remove(item);
                    if (item.Key.Contains("Solution"))
                        RepositoryStatusesSolution.Remove(item);
                }
            }

            if (e.NewItems != null)
            {
                foreach (KeyValuePair<string, RepositoryStatus> item in e.NewItems)
                {
                    if (item.Key.Contains("Error") && !RepositoryStatusesError.Any(kvp => kvp.Key == item.Key))
                        RepositoryStatusesError.Add(item);
                    if (item.Key.Contains("Solution") && !RepositoryStatusesSolution.Any(kvp => kvp.Key == item.Key))
                        RepositoryStatusesSolution.Add(item);
                }
            }

            RepositoryStatusesError.Sort(item => item.Key, SortDirection.Descending);
            RepositoryStatusesSolution.Sort(item => item.Key, SortDirection.Descending);
        }

        #endregion // Event Handlers

        #region IProErrorsServiceCallback Members

        /// <summary>
        /// Called when a DataRecordRepository has been added to the
        /// MasterRepository.
        /// </summary>
        /// <param name="year">Year of the repository.</param>
        /// <param name="type">Type of the repository.</param>
        public void DataRecordRepositoryAdded(int year, DataRecordType type)
        {
            AddTaxYear(year);

            //Arbiter.Instance.NotifyColleagues(
            //    ViewModelMessage.UpdateServiceActivity,
            //    "Repository Added: " + year.ToString() + " " + type.ToString() + "s");
        }

        /// <summary>
        /// Called when a DataRecordRepository has been removed from the
        /// MasterRepository.
        /// </summary>
        /// <param name="year">Year of the repository.</param>
        /// <param name="type">Type of the repository.</param>
        public void DataRecordRepositoryRemoved(int year, DataRecordType type)
        {
            RemoveTaxYear(year);

            List<WorkspaceViewModel> workspaces = Workspaces.ToList();

            foreach (WorkspaceViewModel workspace in workspaces)
                if (workspace is AllDataRecordsViewModel &&
                    (workspace as AllDataRecordsViewModel).Year == year)
                    Workspaces.Remove(workspace);

            //Arbiter.Instance.NotifyColleagues(
            //    ViewModelMessage.UpdateServiceActivity,
            //    "Repository Removed: " + year.ToString() + " " + type.ToString() + "s");
        }

        public void ExemptEfinAdded(string efin)
        {
            AddLocalExemptEfin(efin);
        }

        public void ExemptEfinRemoved(string efin)
        {
            RemoveLocalExemptEfin(efin);
        }

        public void RepositoryActivityChanged(string activity)
        {
            ServiceActivity = activity;
        }

        /// <summary>
        /// Called when the status of the MasterRepository changes.
        /// </summary>
        /// <param name="type">Type of Record</param>
        /// <param name="status">The new status.</param>
        /// <param name="year">Tax year.</param>
        public void RepositoryStatusChanged(int year, DataRecordType type, RepositoryStatus status)
        {
            //object[] args = new object[] { year, type, status };
            //Dispatcher.FromThread(_parentThread).Invoke(
            //    new UpdateServiceStatusDelegate(this.UpdateServiceStatus),
            //    DispatcherPriority.Render,
            //    year, type, status);
            new Thread(() => UpdateServiceStatus(year, type, status)).Start();
            //Arbiter.Instance.NotifyColleagues(
            //    ViewModelMessage.UpdateServiceStatus,
            //    args);
        }

        #endregion // IProErrorsServiceCallback

        #region Utilities

        void AddLocalExemptEfin(string efin)
        {
            if (!ExemptEfins.Contains(efin))
            {
                ExemptEfins.Add(efin);
                ExemptEfins.Sort(exemptEfin => exemptEfin, SortDirection.Ascending);

                base.OnPropertyChanged("ExemptEfins");
            }
        }

        void AddTaxYear(int year)
        {
            if (!TaxYears.Contains(year))
            {
                TaxYears.Add(year);
                TaxYears.Sort(taxYear => taxYear, SortDirection.Descending);

                if (TaxYears.Count > 0)
                    TaxYear = TaxYears.Max();

                base.OnPropertyChanged("TaxYears");
                base.OnPropertyChanged("TaxYear");
            }

            string eKey = year.ToString(CultureInfo.InvariantCulture) + " Error";

            if (!_repositoryKeys.Contains(eKey))
                _repositoryKeys.Add(eKey);
            if (!RepositoryStatuses.ContainsKey(eKey))
                RepositoryStatuses.Add(eKey, RepositoryStatus.Idle);

            string sKey = year.ToString(CultureInfo.InvariantCulture) + " Solution";

            if (!_repositoryKeys.Contains(sKey))
                _repositoryKeys.Add(sKey);
            if (!RepositoryStatuses.ContainsKey(sKey))
                RepositoryStatuses.Add(sKey, RepositoryStatus.Idle);

            string qKey = year.ToString(CultureInfo.InvariantCulture) + " Support";

            if (!_repositoryKeys.Contains(qKey))
                _repositoryKeys.Add(qKey);
            if (!RepositoryStatuses.ContainsKey(qKey))
                RepositoryStatuses.Add(qKey, RepositoryStatus.Idle);
        }

        void EstablishProxy()
        {
            try
            {
                if (_proxy != null)
                {
                    (_proxy as ICommunicationObject).Closed -= ProxyClosed;
                    (_proxy as ICommunicationObject).Closing -= ProxyClosing;
                    (_proxy as ICommunicationObject).Faulted -= ProxyFaulted;
                    (_proxy as ICommunicationObject).Opened -= ProxyOpened;
                    (_proxy as ICommunicationObject).Opening -= ProxyOpening;
                }

                _proxy = new ProErrorsStatusClient(
                    new InstanceContext(this));

                base.OnPropertyChanged("ConnectionStatus");

                (_proxy as ICommunicationObject).Closed += ProxyClosed;
                (_proxy as ICommunicationObject).Closing += ProxyClosing;
                (_proxy as ICommunicationObject).Faulted += ProxyFaulted;
                (_proxy as ICommunicationObject).Opened += ProxyOpened;
                (_proxy as ICommunicationObject).Opening += ProxyOpening;

                _proxy.RegisterForStatusUpdates();

                List<int> years = _proxy.GetTaxYears().ToList();
                foreach (int year in years)
                    AddTaxYear(year);

                List<string> efins = _proxy.GetExemptEfins().ToList();
                foreach (string efin in efins)
                    AddLocalExemptEfin(efin);
            }
            catch (CommunicationException) { MessageBox.Show("Communication Faulted: Could not connect to server."); }
            catch (TimeoutException) { MessageBox.Show("Timed out: Could not connect to server."); }
        }

        void InitializeRepositoryStatus()
        {
            RepositoryStatuses = new ObservableDictionary<string, RepositoryStatus>();
            RepositoryStatuses.CollectionChanged += RepositoryStatusesCollectionChanged;

            RepositoryStatusesError = new ViewableCollection<KeyValuePair<string, RepositoryStatus>>();
            RepositoryStatusesSolution = new ViewableCollection<KeyValuePair<string, RepositoryStatus>>();
        }

        void RemoveLocalExemptEfin(string efin)
        {
            if (ExemptEfins.Contains(efin))
            {
                ExemptEfins.Remove(efin);
                ExemptEfins.Sort(exemptEfin => exemptEfin, SortDirection.Ascending);

                base.OnPropertyChanged("ExemptEfins");
            }
        }

        void RemoveTaxYear(int year)
        {
            if (TaxYears.Contains(year))
            {
                TaxYears.Remove(year);
                TaxYears.Sort(taxYear => taxYear, SortDirection.Descending);

                if (TaxYears.Count > 0)
                    TaxYear = TaxYears.Max();

                base.OnPropertyChanged("TaxYears");
                base.OnPropertyChanged("TaxYear");
            }

            string eKey = year.ToString(CultureInfo.InvariantCulture) + " Error";

            if (_repositoryKeys.Contains(eKey))
                _repositoryKeys.Remove(eKey);
            if (RepositoryStatuses.ContainsKey(eKey))
                RepositoryStatuses.Remove(eKey);

            string sKey = year.ToString(CultureInfo.InvariantCulture) + " Solution";

            if (_repositoryKeys.Contains(sKey))
                _repositoryKeys.Remove(sKey);
            if (RepositoryStatuses.ContainsKey(sKey))
                RepositoryStatuses.Remove(sKey);

            string qKey = year.ToString(CultureInfo.InvariantCulture) + " Support";

            if (_repositoryKeys.Contains(qKey))
                _repositoryKeys.Remove(qKey);
            if (RepositoryStatuses.ContainsKey(qKey))
                RepositoryStatuses.Remove(qKey);
        }

        void ServiceCheckIn(object autoResetEvent)
        {
            try { _proxy.CheckIn(); }
            catch (CommunicationException e) { Trace.TraceWarning(e.ToString()); }
            catch (TimeoutException e) { Trace.TraceWarning(e.ToString()); }
        }

        void UpdateServiceStatus(int year, DataRecordType type, RepositoryStatus status)
        {
            lock (StatusLock)
            {
                string key = year.ToString(CultureInfo.InvariantCulture) + " " + type.ToString();

                if (RepositoryStatuses.ContainsKey(key))
                    RepositoryStatuses[key] = status;

                base.OnPropertyChanged("RepositoryStatusesError");
                base.OnPropertyChanged("RepositoryStatusesSolution");
            }
        }

        #endregion // Utilities
    }
}

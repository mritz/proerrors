﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;
using System.Windows.Input;
using ProErrors.Client.Library;
using ProErrors.Client.Proxies;
using ProErrors.Core;
using System.Windows;

namespace ProErrors.Client.ViewModel
{
    public class DataRecordViewModel : WorkspaceViewModel,
        IEditableObject
    {
        #region Fields

        ProErrorsRepositoryClient _proxy;

        DataRecord _dataRecord;
        DataRecordType _type;
        int _year;

        bool _isSelected;

        ICommand _cancelCommand;
        ICommand _copyCommand;
        ICommand _deleteCommand;
        ICommand _previewCommand;

        List<string> _categories;

        #endregion // Fields

        #region Constructors
        // MOD: Changed constructor
        public DataRecordViewModel(int year, DataRecordType type,
            DataRecord dataRecord, ProErrorsRepositoryClient proxy)
        {
            _dataRecord = dataRecord;
            _year = year;
            _type = type;
            _proxy = proxy;

            base.DisplayName = this.Key;

            _dataRecordInformation = new DataRecordInformation();
            _dataRecordInformation.Fix = dataRecord.Fix;
            _dataRecordInformation.Category = dataRecord.Category;
            _dataRecordInformation.User = Config.User;

            #region Arbiter Registration

            #region CloseOpenRecords

            Arbiter.Instance.Register(
                (Object o) =>
                {
                    ViewableCollection<DataRecordViewModel> collection =
                        (ViewableCollection<DataRecordViewModel>)o;

                    if (collection.Contains(this))
                        this.CloseCommand.Execute(null);
                }, ViewModelMessage.CloseOpenRecords);

            #endregion // CloseOpenRecords

            #endregion // Arbiter Registration
        }

        #endregion // Constructors

        #region IDataRecord Properties

        public string BVersion { get { return _dataRecord.BVersion; } }
        public string Category
        {
            get { return _dataRecordInformation.Category; }
            set
            {
                if (value == _dataRecordInformation.Category)
                    return;

                _dataRecordInformation.Category = value;

                this.OnDataRecordChanged();
            }
        }
        public double DotNetVersion { get { return _dataRecord.DotNetVersion; } }
        public string Efin { get { return _dataRecord.Efin; } }
        public string ErrNum { get { return _dataRecord.ErrNum; } }
        public string Fix
        {
            get { return _dataRecordInformation.Fix; }
            set
            {
                if (value == _dataRecordInformation.Fix)
                    return;

                _dataRecordInformation.Fix = value;

                this.OnDataRecordChanged();
            }
        }
        public Guid ID { get { return _dataRecord.ID; } }
        public bool IsFixed
        {
            get { return _dataRecord.IsFixed; }
            set
            {
                if (value == _dataRecord.IsFixed)
                    return;

                _dataRecord.IsFixed = value;

                base.OnPropertyChanged("IsFixed");
            }
        }
        public string IVersion { get { return _dataRecord.IVersion; } }
        public string LineNum { get { return _dataRecord.LineNum; } }
        public string Message { get { return _dataRecord.Message; } }
        public string OperatingSystem { get { return _dataRecord.OperatingSystem; } }
        public string Program { get { return _dataRecord.Program; } }
        public string Stack { get { return _dataRecord.Stack; } }
        public Nullable<UnitedStateAbbreviation> StateAbbreviation { get { return _dataRecord.StateAbbreviation; } }
        public DateTime TimeStamp { get { return _dataRecord.TimeStamp; } }
        public string User
        {
            get { return _dataRecordInformation.User; }
            set
            {
                if (value == _dataRecordInformation.User)
                    return;

                _dataRecordInformation.User = value;

                this.OnDataRecordChanged();
            }
        }

        #endregion // IDataRecord Properties

        #region Public Interface

        public List<string> Categories
        {
            get
            {
                if (_categories == null)
                {
                    _categories = new List<string>()
                    {
                        ".NET Framework",
                        "Antivirus",
                        "Configuration",
                        "Development",
                        "Download Specific",
                        "Network",
                        "Operating System",
                        "Other",
                        "Register DLL's",
                        "Reindex",
                        "Update Federal Individual",
                        "Update Federal Business",
                        "Update State Individual",
                        "Update State Business",
                        "Update Update Utility",
                        "Virus/Malware"
                    };
                }
                return _categories;
            }
        }

        public DataRecord DataRecord
        {
            get { return _dataRecord; }
        }

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (value == _isSelected)
                    return;

                _isSelected = value;

                base.OnPropertyChanged("IsSelected");
            }
        }

        public string Key
        {
            get { return this.Efin + "; " + this.Program + "; " + this.TimeStamp; }
        }

        public DataRecordType Type
        {
            get { return _dataRecord.Type; }
        }

        #endregion // Public Interface

        #region Commands

        #region CancelCommand

        public ICommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                    _cancelCommand = new RelayCommand(
                        param => this.Cancel());

                return _cancelCommand;
            }
        }

        void Cancel()
        {
            Trace.TraceInformation(DateTime.Now.ToString() + ": Cancel " + this.DataRecord.Efin);

            this.CloseCommand.Execute(null);
        }

        #endregion // CancelCommand

        #region CopyCommand

        public ICommand CopyInfoCommand
        {
            get
            {
                if (_copyCommand == null)
                    _copyCommand = new RelayCommand(
                        param => this.CopyInfo(),
                        param => this.CanCopyInfo());

                return _copyCommand;
            }
        }

        void CopyInfo()
        {
            Trace.TraceInformation(DateTime.Now.ToString() + ": CopyInfo " + this.DataRecord.Efin);

            Arbiter.Instance.NotifyColleagues(
                ViewModelMessage.CopyDataRecordInformation,
                null);
        }

        bool CanCopyInfo()
        {
            return true;
        }

        #endregion // CopyCommand

        #region DeleteCommand

        public ICommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                    _deleteCommand = new RelayCommand(
                        param => this.Delete(),
                        param => this.CanDelete());

                return _deleteCommand;
            }
        }

        public void Delete()
        {
            Trace.TraceInformation(DateTime.Now.ToString() + ": Delete " + this.DataRecord.Efin);

            this.EndEdit();
            this.DataRecord.Category = this.Category;
            this.DataRecord.Fix = this.Fix;
            this.DataRecord.User = this.User;

            // MOD: Change deletion method
            //Arbiter.Instance.NotifyColleagues(
            //    ViewModelMessage.MarkFixedDataRecordViewModel,
            //    new object[3] { _year, _type, _dataRecord });

            try { _proxy.MarkDataRecordFixed(_dataRecord); }
            catch (CommunicationException e) { Trace.TraceWarning(e.ToString()); }
            catch (TimeoutException e) { Trace.TraceWarning(e.ToString()); }

            this.CloseCommand.Execute(null);
        }

        bool CanDelete()
        {
            return !String.IsNullOrEmpty(this.Category);
        }

        #endregion // DeleteCommand

        #region PreviewCommand

        public ICommand PreviewCommand
        {
            get
            {
                if (_previewCommand == null)
                    _previewCommand = new RelayCommand(
                        param => this.Preview(),
                        param => this.CanPreview());

                return _previewCommand;
            }
        }

        public void Preview()
        {
            MessageBox.Show(GetMessage());
        }

        public bool CanPreview()
        {
            return !String.IsNullOrEmpty(this.Category);
        }

        public string GetMessage()
        {
            if (!String.IsNullOrEmpty(this.Category) && !String.IsNullOrEmpty(this.Fix))
            {
                switch (this.Category)
                {
                    case ".NET Framework":
                        return "There is an issue with the .NET Framework: " + Environment.NewLine + this.Fix;
                    case "Antivirus":
                        return "Your antivirus program is causing conflicts: " + Environment.NewLine + this.Fix;
                    case "Configuration":
                        return "There is a configuration issue: " + Environment.NewLine + this.Fix;
                    case "Development":
                        return "The issue has been resolved in a newer build of the program: " + Environment.NewLine + this.Fix;
                    case "Download Specific":
                        return "A specific file is required to address your issue: " + Environment.NewLine + this.Fix;
                    case "Network":
                        return "There is a network issue: " + Environment.NewLine + this.Fix;
                    case "Operating System":
                        return "Your current operating system is causing issues: " + Environment.NewLine + this.Fix;
                    case "Other":
                        return this.Fix;
                    case "Register DLL's":
                        return "Re-register DLL's: " + Environment.NewLine + this.Fix;
                    case "Reindex":
                        return "Reindex databases: " + Environment.NewLine + this.Fix;
                    case "Update Federal Individual":
                        return "Your program is not up to date: " + Environment.NewLine + this.Fix;
                    case "Update Federal Business":
                        return "Your program is not up to date: " + Environment.NewLine + this.Fix;
                    case "Update State Individual":
                        return "Your program is not up to date: " + Environment.NewLine + this.Fix;
                    case "Update State Business":
                        return "Your program is not up to date: " + Environment.NewLine + this.Fix;
                    case "Update Update Utility":
                        return "Your Update Utility is not up to date: " + Environment.NewLine + this.Fix;
                    case "Virus/Malware":
                        return "Your system may be infected with malware: " + Environment.NewLine + this.Fix;
                }
            }
            else
            {
                switch (this.Category)
                {
                    case ".NET Framework":
                        return "There is an issue with the .NET Framework. Please contact technical support for further assistance.";
                    case "Antivirus":
                        return "Your antivirus program is causing conflicts. Please contact technical support for further assistance.";
                    case "Configuration":
                        return "There is a configuration issue. Please contact technical support for further assistance.";
                    case "Development":
                        return "The issue has been resolved in a newer build of the program.";
                    case "Download Specific":
                        return "A specific file is required to address your issue. Please contact technical support for further assistance.";
                    case "Network":
                        return "There is a network issue. Please contact technical support for further assistance.";
                    case "Operating System":
                        return "Your current operating system is causing issues. Please contact technical support for further assistance.";
                    case "Other":
                        return "The error has been addressed, but there are no details in the database. Please contact technical support for further assistance.";
                    case "Register DLL's":
                        return "Re-register DLL's: From the main menu, select Configuration > Configuration Utilities > Register DLL Files.";
                    case "Reindex":
                        return "Reindex databases: From the main menu, select Utilities > Reindex Files.";
                    case "Update Federal Individual":
                        return "Your program is not up to date. Please download the latest Federal Individual updates.";
                    case "Update Federal Business":
                        return "Your program is not up to date. Please download the latest Federal Business updates.";
                    case "Update State Individual":
                        return "Your program is not up to date. Please download the latest applicable State Individual updates.";
                    case "Update State Business":
                        return "Your program is not up to date. Please download the latest applicable State Business updates.";
                    case "Update Update Utility":
                        return "Your Update Utility is not up to date. Please download the latest Update Utility updates.";
                    case "Virus/Malware":
                        return "Your system may be infected with malware. Please contact technical support for further assistance.";
                }
            }

            return "No Solution Found";
        }

        #endregion // PreviewCommand

        #endregion // Commands

        #region Event Handlers

        protected override void Dispose(bool disposing)
        {
            if (!this.Disposed)
            {
                this.Disposed = true;
            }

            base.Dispose(disposing);
        }

        protected override void OnRequestClose()
        {
            this.CancelEdit();
            base.OnRequestClose();
        }

        #endregion // Event Handlers

        #region IEditableObject Members

        public void BeginEdit()
        {
            if (!_editing)
            {
                _dataRecordBackup = _dataRecordInformation;
                _editing = true;
            }
        }

        public void CancelEdit()
        {
            if (_editing)
            {
                _dataRecordInformation = _dataRecordBackup;
                _editing = false;
            }
        }

        public void EndEdit()
        {
            if (_editing)
            {
                _dataRecordBackup = new DataRecordInformation();
                _editing = false;
            }
        }

        #endregion

        #region IEditableObject Helpers

        struct DataRecordInformation
        {
            internal string Category;
            internal string Fix;
            internal string User;
        }

        DataRecordInformation _dataRecordInformation;
        DataRecordInformation _dataRecordBackup;
        bool _editing = false;

        void OnDataRecordChanged()
        {
            if (!_editing)
            {
                base.OnPropertyChanged("Category");
                base.OnPropertyChanged("Fix");
                base.OnPropertyChanged("User");
            }
        }

        #endregion // IEditableObject Helpers
    }
}

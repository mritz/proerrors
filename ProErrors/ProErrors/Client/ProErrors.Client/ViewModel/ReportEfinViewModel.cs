﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Windows;
using ProErrors.Client.Library;
using ProErrors.Client.Proxies;
using ProErrors.Core;
using System.Diagnostics;

namespace ProErrors.Client.ViewModel
{
    public class ReportEfinViewModel : WorkspaceViewModel
    {
        #region Fields

        ProErrorsReportClient _proxy;

        EfinViewModel _currentSelection;

        #endregion // Fields

        #region Constructors

        public ReportEfinViewModel(int year)
        {
            this.Year = year;

            base.DisplayName = this.Year.ToString() + " EFIN Report";

            try
            {
                _proxy = new ProErrorsReportClient();

                List<DataRecord> AllDataRecords =
                    (from record in _proxy.GetDataRecords(this.Year)
                     select record).ToList();

                var efinCounts =
                    from record in AllDataRecords
                    group record by record.Efin into efins
                    select new KeyValuePair<string, int>(efins.Key, efins.Count());

                var orderedEfinCounts = efinCounts.OrderByDescending(i => i.Value);
                this.EfinCollection = new ViewableCollection<EfinViewModel>();

                foreach (KeyValuePair<string, int> efin in orderedEfinCounts)
                    this.EfinCollection.Add(new EfinViewModel(efin.Key, efin.Value));
            }
            catch (CommunicationException) { MessageBox.Show("Communication faulted: Could not connect to server."); }
            catch (TimeoutException) { MessageBox.Show("Timed out: Could not connect to server."); }
        }

        #endregion // Constructors

        #region Public Properties

        public EfinViewModel CurrentSelection
        {
            get { return _currentSelection; }
            set
            {
                if (_currentSelection == value)
                    return;

                _currentSelection = value;

                base.OnPropertyChanged("CurrentSelection");
            }
        }

        public ViewableCollection<EfinViewModel> EfinCollection { get; private set; }

        public int Year { get; private set; }

        #endregion // Public Properties

        #region Overrides

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            try { _proxy.Close(); }
            catch (CommunicationException e) { Trace.TraceWarning(e.ToString()); }
            catch (TimeoutException e) { Trace.TraceWarning(e.ToString()); }
        }

        #endregion
    }

    public class EfinViewModel : ViewModelBase
    {
        #region Constructors

        public EfinViewModel(string efin, int count)
        {
            this.EfinCountPair = new KeyValuePair<string, int>(efin, count);
        }

        #endregion // Constructors

        #region Properties

        public KeyValuePair<string, int> EfinCountPair { get; set; }

        #endregion // Properties
    }
}

﻿using ProErrors.Client.Library;
using ProErrors.Core;

namespace ProErrors.Client.ViewModel
{
    public class SearchOptionsViewModel : ViewModelBase
    {
        SearchOptions _searchOptions;

        public SearchOptionsViewModel(SearchOptions searchOptions)
        {
            _searchOptions = searchOptions;
        }

        public bool IgnoreCase
        {
            get { return _searchOptions.IgnoreCase; }
            set
            {
                if (_searchOptions.IgnoreCase == value)
                    return;

                _searchOptions.IgnoreCase = value;

                base.OnPropertyChanged("IgnoreCase");
            }
        }

        public bool IsRegex
        {
            get { return _searchOptions.IsRegex; }
            set
            {
                if (_searchOptions.IsRegex == value)
                    return;

                _searchOptions.IsRegex = value;

                base.OnPropertyChanged("IsRegex");
            }
        }
    }
}

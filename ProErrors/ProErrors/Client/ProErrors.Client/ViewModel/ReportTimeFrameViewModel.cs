﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Windows;
using ProErrors.Client.Library;
using ProErrors.Client.Proxies;
using ProErrors.Core;
using System.Diagnostics;

namespace ProErrors.Client.ViewModel
{
    public class ReportTimeFrameViewModel : WorkspaceViewModel
    {
        #region Fields

        ProErrorsReportClient _proxy;

        #endregion // Fields

        #region Constructors

        public ReportTimeFrameViewModel(int year)
        {
            this.Year = year;

            base.DisplayName = this.Year.ToString() + " Time Frame Report";

            try
            {
                _proxy = new ProErrorsReportClient();

                List<DataRecord> AllDataRecords =
                    (from record in _proxy.GetDataRecords(this.Year)
                     select record).ToList();

                var dateCounts =
                    from record in AllDataRecords
                    group record by record.TimeStamp.Date into dates
                    select new KeyValuePair<DateTime, int>(dates.Key, dates.Count());

                var orderedDateCounts = dateCounts.OrderBy(i => i.Key);
                this.DateCollection = new ViewableCollection<DateViewModel>();

                foreach (KeyValuePair<DateTime, int> date in orderedDateCounts)
                    this.DateCollection.Add(new DateViewModel(date.Key, date.Value));
            }
            catch (CommunicationException) { MessageBox.Show("Communication faulted: Could not connect to server."); }
            catch (TimeoutException) { MessageBox.Show("Timed out: Could not connect to server."); }
        }

        #endregion // Constructors

        #region Public Properties

        public ViewableCollection<DateViewModel> DateCollection { get; private set; }

        public int Year { get; private set; }

        #endregion // Public Properties

        #region Overrides

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            try { _proxy.Close(); }
            catch (CommunicationException e) { Trace.TraceWarning(e.ToString()); }
            catch (TimeoutException e) { Trace.TraceWarning(e.ToString()); }
        }

        #endregion
    }

    public class DateViewModel : ViewModelBase
    {
        #region Constructors

        public DateViewModel(DateTime date, int count)
        {
            this.DateCountPair = new KeyValuePair<DateTime, int>(date, count);
        }

        #endregion // Constructors

        #region Properties

        public KeyValuePair<DateTime, int> DateCountPair { get; set; }

        #endregion // Properties
    }
}

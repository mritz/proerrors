using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using System.Windows;
using System.Windows.Data;
using System.Windows.Threading;
using ProErrors.Client.Library;
using ProErrors.Client.Proxies;
using ProErrors.Core;
using System.Text.RegularExpressions;

namespace ProErrors.Client.ViewModel
{
    public class AllDataRecordsViewModel : WorkspaceViewModel,
        IProErrorsRepositoryCallback
    {
        #region Delegates

        public delegate void RefreshDelegate();
        public delegate void ViewModelDelegate(DataRecordViewModel drVM);

        #endregion // Delegates

        #region Fields

        ProErrorsRepositoryClient _proxy;
        Timer _checkInTimer;

        Thread _parentThread;

        string _searchField;
        string _searchString;

        #endregion // Fields

        #region Constructors

        /// <summary>
        /// Create a standard AllDataRecordsViewModel for errors/solutions.
        /// </summary>
        public AllDataRecordsViewModel(int year, DataRecordType type)
        {
            _parentThread = Thread.CurrentThread;

            this.Year = year;
            this.Type = (DataRecordRepositoryType)((int)type * 3);

            base.DisplayName = this.Key;

            try { EstablishProxy(); }
            catch (CommunicationException e) { Trace.TraceWarning(e.ToString()); }
            catch (TimeoutException e) { Trace.TraceWarning(e.ToString()); }

            _checkInTimer = new Timer(ServiceCheckIn, null,
                new TimeSpan(0, 5, 0), new TimeSpan(0, 5, 0));

            this.SetFilter(delegate(object o)
            { return (o as DataRecordViewModel).Type == type; });

            #region Arbiter Registration

            #region MarkFixed DataRecordViewModel(s)

            //Arbiter.Instance.Register(
            //    (Object o) =>
            //    {
            //        object[] args = (object[])o;

            //        if ((int)args[0] != this.Year)
            //            return;
            //        if ((DataRecordType)args[1] != (DataRecordType)(((int)this.Type) / 3))
            //            return;

            //        if (args[2] is DataRecord)
            //            this.MarkFixed((DataRecord)args[2]);
            //        if (args[2] is DataRecordViewModel)
            //            this.MarkFixed((args[2] as DataRecordViewModel).DataRecord);
            //        if (args[2] is IEnumerable<DataRecord>)
            //            this.MarkFixed((IEnumerable<DataRecord>)args[2]);
            //        if (args[2] is IEnumerable<DataRecordViewModel>)
            //        {
            //            IEnumerable<DataRecord> dataRecords =
            //                (args[2] as IEnumerable<DataRecordViewModel>)
            //                .Select<DataRecordViewModel, DataRecord>(drVM => drVM.DataRecord);
            //            this.MarkFixed(dataRecords);

            //            //List<DataRecord> deletionList = new List<DataRecord>();
            //            //foreach (DataRecordViewModel drVM in (args[2] as IEnumerable<DataRecordViewModel>))
            //            //    deletionList.Add(drVM.DataRecord);
            //            //this.MarkFixed(deletionList);
            //        }
            //    }, ViewModelMessage.MarkFixedDataRecordViewModel);

            #endregion // MarkFixed DataRecordViewModel(s)

            #endregion // Arbiter Registration
        }

        /// <summary>
        /// Create a search AllDataRecordsViewModel for errors/solutions.
        /// </summary>
        public AllDataRecordsViewModel(int year, DataRecordType type,
            ViewableCollection<DataRecordViewModel> originalRecords,
            string searchField, string searchString,
            SearchOptionsViewModel searchOptions,
            ProErrorsRepositoryClient proxy)
        {
            _parentThread = Thread.CurrentThread;

            this.Year = year;
            this.Type = (DataRecordRepositoryType)(((int)type * 3) + 2);
            _searchField = searchField;
            _searchString = searchString;
            _proxy = proxy;

            base.DisplayName = this.Key;

            this.AllDataRecords = originalRecords;
            this.FilteredDataRecords = new ListCollectionView(this.AllDataRecords);

            this.SetFilter(delegate(object o)
            {
                string toSearch;
                switch (_searchField)
                {
                    case "EFIN":
                        toSearch = (o as DataRecordViewModel).Efin; break;
                    case "Program":
                        toSearch = (o as DataRecordViewModel).Program; break;
                    case "State":
                        toSearch = (o as DataRecordViewModel).StateAbbreviation.ToString(); break;
                    case "Error Message":
                        toSearch = (o as DataRecordViewModel).Message; break;
                    case "Call Stack":
                        toSearch = (o as DataRecordViewModel).Stack; break;
                    default:
                        return false;
                }

                if (searchOptions.IsRegex)
                {
                    Regex regex;
                    if (searchOptions.IgnoreCase)
                        regex = new Regex(_searchString, RegexOptions.IgnoreCase);
                    else
                        regex = new Regex(_searchString);

                    return regex.IsMatch(toSearch);
                }
                else
                {
                    if (searchOptions.IgnoreCase)
                        return toSearch.ToLower().Contains(_searchString.ToLower());
                    else
                        return toSearch.Contains(_searchString);
                }
            });

            searchOptions.Dispose();

            #region Arbiter Registration

            #region CloseSearchWorkspaces

            Arbiter.Instance.Register(
                (Object o) =>
                {
                    object[] args = (object[])o;

                    if ((int)args[0] != this.Year)
                        return;
                    if ((DataRecordRepositoryType)args[1] !=
                        (DataRecordRepositoryType)(((int)this.Type) - 2))
                        return;

                    this.CloseCommand.Execute(null);
                }, ViewModelMessage.CloseSearchWorkspaces);

            #endregion // CloseSearchWorkspaces

            #endregion // Arbiter Registration
        }

        #endregion // Constructors

        #region Public Interface

        public ViewableCollection<DataRecordViewModel> AllDataRecords { get; private set; }

        public SortDescription CurrentSort { get; set; }

        public ListCollectionView FilteredDataRecords { get; set; }

        public string Key
        {
            get
            {
                string append = String.Empty;

                switch (this.Type)
                {
                    case DataRecordRepositoryType.Errors:
                    case DataRecordRepositoryType.ErrorsCluster:
                        append = "Errors";
                        break;
                    case DataRecordRepositoryType.ErrorsSearch:
                        append = "Errors; " + _searchField + ": " + _searchString;
                        break;
                    case DataRecordRepositoryType.Solutions:
                    case DataRecordRepositoryType.SolutionsCluster:
                        append = "Solutions";
                        break;
                    case DataRecordRepositoryType.SolutionsSearch:
                        append = "Solutions; " + _searchField + ": " + _searchString;
                        break;
                    case DataRecordRepositoryType.Support:
                    case DataRecordRepositoryType.SupportCluster:
                        append = "Support";
                        break;
                    case DataRecordRepositoryType.SupportSearch:
                        append = "Support; " + _searchField + ": " + _searchString;
                        break;
                }
                return this.Year.ToString() + " " + append;
            }
        }

        public int RecordCount
        {
            get { return FilteredDataRecords.Count; }
        }

        public DataRecordRepositoryType Type { get; private set; }

        public int Year { get; private set; }

        #endregion // Public Interface

        #region Public Methods

        internal AllDataRecordsViewModel CreatedSearchWorkspace(string searchField, string searchString, SearchOptionsViewModel searchOptions)
        {
            return new AllDataRecordsViewModel(
                this.Year, (DataRecordType)(((int)this.Type) / 3),
                this.AllDataRecords, searchField, searchString, searchOptions, _proxy);
        }

        public void MarkFixed(DataRecord dataRecord)
        {
            //if (this.Type == DataRecordRepositoryType.Errors)
            //{
            try { _proxy.MarkDataRecordFixed(dataRecord); }
            catch (CommunicationException e) { Trace.TraceWarning(e.ToString()); }
            catch (TimeoutException e) { Trace.TraceWarning(e.ToString()); }
            //}
            //else
            //{
            //    Arbiter.Instance.NotifyColleagues(
            //        ViewModelMessage.MarkFixedDataRecordViewModel,
            //        new object[3]
            //        {
            //            this.Year,
            //            (DataRecordType)(((int)this.Type)/3),
            //            dataRecord
            //        });
            //}
        }

        public void MarkFixed(IEnumerable<DataRecord> dataRecords)
        {
            //if (this.Type == DataRecordRepositoryType.Errors)
            //{
            try { _proxy.MarkDataRecordsFixed(dataRecords.ToArray()); }
            catch (CommunicationException e) { Trace.TraceWarning(e.ToString()); }
            catch (TimeoutException e) { Trace.TraceWarning(e.ToString()); }
            //}
            //else
            //{
            //    Arbiter.Instance.NotifyColleagues(
            //        ViewModelMessage.MarkFixedDataRecordViewModel,
            //        new object[3]
            //        {
            //            this.Year,
            //            (DataRecordType)(((int)this.Type)/3),
            //            dataRecords
            //        });
            //}
        }

        public void MarkFixed(DataRecordViewModel dataRecordViewModel)
        {
            this.MarkFixed(dataRecordViewModel.DataRecord);
        }

        public void MarkFixed(IEnumerable<DataRecordViewModel> dataRecordViewModels)
        {
            IEnumerable<DataRecord> dataRecords =
                dataRecordViewModels.Select<DataRecordViewModel, DataRecord>(drVM => drVM.DataRecord);
            this.MarkFixed(dataRecords);
        }

        public void MarkIrrelevant(IEnumerable<DataRecord> dataRecords)
        {
            try { _proxy.MarkDataRecordsIrrelevant(dataRecords.ToArray()); }
            catch (CommunicationException e) { Trace.TraceWarning(e.ToString()); }
            catch (TimeoutException e) { Trace.TraceWarning(e.ToString()); }
        }

        public void MarkIrrelevant(DataRecordViewModel dataRecordViewModel)
        {
            this.MarkFixed(dataRecordViewModel.DataRecord);
        }

        public void MarkIrrelevant(IEnumerable<DataRecordViewModel> dataRecordViewModels)
        {
            IEnumerable<DataRecord> dataRecords =
                dataRecordViewModels.Select<DataRecordViewModel, DataRecord>(drVM => drVM.DataRecord);
            this.MarkIrrelevant(dataRecords);
        }

        public void MarkSupport(IEnumerable<DataRecord> dataRecords)
        {
            try { _proxy.MarkDataRecordsSupport(dataRecords.ToArray()); }
            catch (CommunicationException e) { Trace.TraceWarning(e.ToString()); }
            catch (TimeoutException e) { Trace.TraceWarning(e.ToString()); }
        }

        public void MarkSupport(DataRecordViewModel dataRecordViewModel)
        {
            this.MarkFixed(dataRecordViewModel.DataRecord);
        }

        public void MarkSupport(IEnumerable<DataRecordViewModel> dataRecordViewModels)
        {
            IEnumerable<DataRecord> dataRecords =
                dataRecordViewModels.Select<DataRecordViewModel, DataRecord>(drVM => drVM.DataRecord);
            this.MarkSupport(dataRecords);
        }

        public void Refresh()
        {
            Dispatcher.FromThread(_parentThread).Invoke(
                new RefreshDelegate(this.FilteredDataRecords.Refresh),
                DispatcherPriority.Normal,
                null);

            base.OnPropertyChanged("RecordCount");
        }

        #endregion // Public Methods

        #region Event Handlers

        protected override void OnRequestClose()
        {
            if (this.Type == DataRecordRepositoryType.Errors ||
                this.Type == DataRecordRepositoryType.Solutions)
            {
                Arbiter.Instance.NotifyColleagues(
                    ViewModelMessage.CloseSearchWorkspaces,
                    new object[] { this.Year, this.Type });

                Arbiter.Instance.NotifyColleagues(
                    ViewModelMessage.CloseOpenRecords,
                    this.AllDataRecords);

                try { _proxy.UnregisterForDataRecordUpdates(); }
                catch (CommunicationException e) { Trace.TraceWarning(e.ToString()); }
                catch (TimeoutException e) { Trace.TraceWarning(e.ToString()); }

                for (int i = this.AllDataRecords.Count - 1; i >= 0; i--)
                    if (this.AllDataRecords[i] != null)
                        this.AllDataRecords[i].Dispose();

                if (_checkInTimer != null)
                    _checkInTimer.Dispose();

                this.AllDataRecords.Clear();
                this.AllDataRecords.CollectionChanged -= this.OnCollectionChanged;
            }

            base.OnRequestClose();
        }

        void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null && e.NewItems.Count != 0)
                foreach (DataRecordViewModel drVM in e.NewItems)
                    drVM.PropertyChanged += this.OnDataRecordViewModelPropertyChanged;

            if (e.OldItems != null && e.OldItems.Count != 0)
                foreach (DataRecordViewModel drVM in e.OldItems)
                    drVM.PropertyChanged += this.OnDataRecordViewModelPropertyChanged;
        }

        void OnDataRecordViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            (sender as DataRecordViewModel).VerifyPropertyName(e.PropertyName);
        }

        void Proxy_Closed(object sender, EventArgs e)
        {
            this.CloseCommand.Execute(null);
        }

        void Proxy_Faulted(object sender, EventArgs e)
        {
            this.CloseCommand.Execute(null);
        }

        #endregion // Event Handlers

        #region IProErrorsRepositoryCallback Members

        /// <summary>
        /// Called when a DataRecord has been added to the DataRecordRepository.
        /// </summary>
        /// <param name="year">Year of the repository.</param>
        /// <param name="type">Type of the repository.</param>
        /// <param name="dataRecord">DataRecord added.</param>
        public void DataRecordAdded(int year, DataRecordType type, DataRecord dataRecord)
        {
            if (this.Year != year ||
                (DataRecordType)(((int)this.Type) / 3) != type)
                return;

            using (var viewModel = new DataRecordViewModel(
                Convert.ToInt32(year), type, dataRecord, _proxy))
            {
                Dispatcher.FromThread(_parentThread).Invoke(
                    new ViewModelDelegate(this.AllDataRecords.Add),
                    DispatcherPriority.Send,
                    viewModel);
            }

            this.Refresh();

            //Arbiter.Instance.NotifyColleagues(
            //    ViewModelMessage.UpdateServiceActivity,
            //    dataRecord.Type.ToString() + " added to " +
            //    this.Year.ToString() + "; EFIN: " + dataRecord.Efin);
        }

        /// <summary>
        /// Called when a DataRecord has been marked as fixed.
        /// </summary>
        /// <param name="year">Year of the repository.</param>
        /// <param name="dataRecord">DataRecord marked fixed.</param>
        public void DataRecordMarkedFixed(int year, DataRecord dataRecord)
        {
            if (this.Year != year ||
                this.Type != DataRecordRepositoryType.Errors ||
                dataRecord == null)
                return;

            var viewModel = AllDataRecords.FirstOrDefault(
                delegate(DataRecordViewModel drVM)
                {
                    return dataRecord.ID == drVM.ID;
                });

            if (viewModel != null)
            {
                viewModel.Category = dataRecord.Category;
                viewModel.IsFixed = dataRecord.IsFixed;
                viewModel.Fix = dataRecord.Fix;
            }

            this.Refresh();

            //Arbiter.Instance.NotifyColleagues(
            //    ViewModelMessage.UpdateServiceActivity,
            //    dataRecord.Type.ToString() + " marked as 'fixed' in " +
            //    this.Year.ToString() + "; EFIN: " + dataRecord.Efin);
        }

        /// <summary>
        /// Called when a DataRecord has been removed from the DataRecordRepository.
        /// </summary>
        /// <param name="year">Year of the repository.</param>
        /// <param name="type">Type of the repository.</param>
        /// <param name="dataRecord">DataRecord removed.</param>
        public void DataRecordRemoved(int year, DataRecordType type, DataRecord dataRecord)
        {
            if (this.Year != year ||
                (DataRecordType)(((int)this.Type) / 3) != type)
                return;

            var viewModel = AllDataRecords.FirstOrDefault(
                delegate(DataRecordViewModel drVM)
                {
                    return dataRecord.ID == drVM.ID;
                });

            if (viewModel != null)
                this.AllDataRecords.Remove(viewModel);

            this.Refresh();

            //Arbiter.Instance.NotifyColleagues(
            //    ViewModelMessage.UpdateServiceActivity,
            //    dataRecord.Type.ToString() + " removed from " +
            //    this.Year.ToString() + "; EFIN: " + dataRecord.Efin);
        }

        #endregion // IProErrorsRepositoryCallback Members

        #region Utilities

        void CreateAllDataRecords(DataRecordRepository repository)
        {
            // MOD: Changed constructor
            List<DataRecordViewModel> all =
                (from record in repository.Collection
                 select new DataRecordViewModel(
                     repository.Year,
                     repository.DataRecordType,
                     record,
                     _proxy)).ToList();

            foreach (DataRecordViewModel drVM in all)
                drVM.PropertyChanged += this.OnDataRecordViewModelPropertyChanged;

            this.AllDataRecords = new ViewableCollection<DataRecordViewModel>(all);
            this.AllDataRecords.CollectionChanged += this.OnCollectionChanged;

            this.FilteredDataRecords = new ListCollectionView(this.AllDataRecords);
        }

        void EstablishProxy()
        {
            try
            {
                if (_proxy != null)
                {
                    (_proxy as ICommunicationObject).Closed -= new EventHandler(Proxy_Closed);
                    (_proxy as ICommunicationObject).Faulted -= new EventHandler(Proxy_Faulted);
                }

                _proxy = new ProErrorsRepositoryClient(
                    new InstanceContext(this));

                (_proxy as ICommunicationObject).Closed += new EventHandler(Proxy_Closed);
                (_proxy as ICommunicationObject).Faulted += new EventHandler(Proxy_Faulted);

                _proxy.RegisterForDataRecordUpdates(
                    this.Year, (DataRecordType)(((int)this.Type) / 3));

                DataRecordRepository repository = _proxy.GetRepository();
                this.CreateAllDataRecords(repository);
            }
            catch (CommunicationException exp) { Trace.TraceError(exp.ToString()); }
            catch (TimeoutException exp) { Trace.TraceError(exp.ToString()); }
        }

        void ServiceCheckIn(object autoResetEvent)
        {
            try { _proxy.CheckIn(); }
            catch (CommunicationException e) { Trace.TraceWarning(e.ToString()); }
            catch (TimeoutException e) { Trace.TraceWarning(e.ToString()); }
        }

        void SetFilter(Predicate<object> searchFilter)
        {
            this.FilteredDataRecords.Filter = searchFilter;
            this.CurrentSort = new SortDescription("TimeStamp", ListSortDirection.Descending);
            this.FilteredDataRecords.SortDescriptions
                .Add(this.CurrentSort);
            this.FilteredDataRecords.Refresh();
        }

        #endregion // Utilities
    }

    public enum DataRecordRepositoryType
    {
        Errors,
        ErrorsCluster,
        ErrorsSearch,
        Solutions,
        SolutionsCluster,
        SolutionsSearch,
        Support,
        SupportCluster,
        SupportSearch,
    }
}

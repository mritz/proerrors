﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel;
using System.Windows;
using System.Windows.Input;
using ProErrors.Client.Library;
using ProErrors.Client.Proxies;
using ProErrors.Core;

namespace ProErrors.Client.ViewModel
{
    public class ReportErrorViewModel : WorkspaceViewModel
    {
        #region Fields

        ProcedureViewModel _currentSelection;

        ICommand _collapseAllCommand;
        ICommand _expandAllCommand;

        #endregion // Fields

        #region Constructors

        public ReportErrorViewModel(int year)
        {
            this.Year = year;

            base.DisplayName = this.Year.ToString() + " Error Report";

            try
            {
                ProErrorsReportClient proxy = new ProErrorsReportClient();
                List<DataRecord> records = proxy.GetDataRecords(year).ToList();

                Procedure root = new Procedure("Root");

                foreach (DataRecord record in records)
                {
                    List<string> procs = SplitCallStack(record);

                    Procedure current = root;
                    Procedure child;

                    for (int i = procs.Count - 1; i >= 0; i--)
                    {
                        child = current.Children.FirstOrDefault(
                            childProc => childProc.Name == procs[i]);

                        if (child == null)
                        {
                            child = new Procedure(procs[i], current);
                            current.Children.Add(child);
                        }

                        current = child;
                    }

                    child = current.Children.FirstOrDefault(
                        childProc => childProc.Name == record.Program);

                    if (child == null)
                    {
                        child = new Procedure(record.Program, current);
                        current.Children.Add(child);
                    }

                    current = child;
                    current.Count++;
                }

                RecursiveSort(root);

                this.ProcedureTree = new ProcedureTreeViewModel(root);
                this.ProcedureTree.FirstTier[0].IsExpanded = true;

                var programCounts =
                    from record in records
                    group record by record.Program into programs
                    select new Procedure(programs.Key) { Count = programs.Count() };

                var orderedProgramCounts = programCounts.OrderByDescending(i => i.Count);
                this.ProcedureCollection = new ViewableCollection<ProcedureViewModel>();

                foreach (Procedure procedure in orderedProgramCounts)
                    this.ProcedureCollection.Add(new ProcedureViewModel(procedure));
            }
            catch (CommunicationException) { MessageBox.Show("Communication faulted: Could not connect to server."); }
            catch (TimeoutException) { MessageBox.Show("Timed out: Could not connect to server."); }

            Arbiter.Instance.Register(
                (Object o) =>
                {
                    ProcedureViewModel vm = (o as ProcedureViewModel);
                    var Procs = from procedure in this.ProcedureCollection
                                where procedure.Name == vm.Name
                                select procedure;

                    if (Procs.Count() > 0)
                        this.CurrentSelection = Procs.ToList()[0];
                }, ViewModelMessage.ChangeProcedureSelection);
        }

        #endregion // Constructors

        #region Public Properties

        public ProcedureViewModel CurrentSelection
        {
            get { return _currentSelection; }
            set
            {
                if (_currentSelection == value)
                    return;

                _currentSelection = value;

                base.OnPropertyChanged("CurrentSelection");

                Arbiter.Instance.NotifyColleagues(
                    ViewModelMessage.ScrollToProcedure, _currentSelection);
            }
        }

        public ViewableCollection<ProcedureViewModel> ProcedureCollection { get; private set; }

        public ProcedureTreeViewModel ProcedureTree { get; private set; }

        public int Year { get; private set; }

        #endregion // Public Properties

        #region Commands

        #region CollapseAllCommand

        public ICommand CollapseAllCommand
        {
            get
            {
                if (_collapseAllCommand == null)
                    _collapseAllCommand = new RelayCommand(
                        param => this.CollapseAll());

                return _collapseAllCommand;
            }
        }

        void CollapseAll()
        {
            foreach (ProcedureViewModel vm in this.ProcedureTree.FirstTier)
                RecursiveCollapse(vm);
        }

        #endregion // CollapseAllCommand

        #region ExpandAllCommand

        public ICommand ExpandAllCommand
        {
            get
            {
                if (_expandAllCommand == null)
                    _expandAllCommand = new RelayCommand(
                        param => this.ExpandAll());

                return _expandAllCommand;
            }
        }

        void ExpandAll()
        {
            foreach (ProcedureViewModel vm in this.ProcedureTree.FirstTier)
                RecursiveExpand(vm);
        }

        #endregion // ExpandAllCommand

        #endregion // Commands

        #region Utilities

        static int CompareProceduresByName(Procedure x, Procedure y)
        {
            if (x.Name == null)
            {
                if (y.Name == null)
                    return 0;
                else
                    return -1;
            }
            else
            {
                if (y.Name == null)
                    return 1;
                else
                    return x.Name.CompareTo(y.Name);
            }
        }

        static void RecursiveCollapse(ProcedureViewModel root)
        {
            if (root.Children.Count > 0)
                foreach (ProcedureViewModel vm in root.Children)
                    RecursiveCollapse(vm);

            root.IsExpanded = false;
        }

        static void RecursiveExpand(ProcedureViewModel root)
        {
            if (root.Children.Count > 0)
                foreach (ProcedureViewModel vm in root.Children)
                    RecursiveExpand(vm);

            root.IsExpanded = true;
        }

        static void RecursiveSort(Procedure root)
        {
            if (root.Children.Count > 0)
            {
                foreach (Procedure child in root.Children)
                    if (child.Children.Count > 0)
                        RecursiveSort(child);

                root.Children.Sort(CompareProceduresByName);
            }
        }

        static List<string> SplitCallStack(DataRecord record)
        {
            if (!record.Stack.Contains("Called From: ") ||
                record.Program == ".NET Component")
                return new List<string>();

            List<string> procs = new List<string>();
            string[] proc1 = record.Stack.Split(new string[] { "Called From: " }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string proc2 in proc1)
            {
                string[] proc3 = proc2.Split(new char[] { ' ' });
                procs.Add(proc3[0]);
            }
            return procs;
        }

        #endregion // Utilities
    }

    public class ProcedureTreeViewModel : ViewModelBase
    {
        #region Fields

        ProcedureViewModel _rootProcedure;
        ReadOnlyCollection<ProcedureViewModel> _firstTier;

        #endregion // Fields

        #region Constructors

        public ProcedureTreeViewModel(Procedure rootProcedure)
        {
            _rootProcedure = new ProcedureViewModel(rootProcedure);

            _firstTier = new ReadOnlyCollection<ProcedureViewModel>(
                new ProcedureViewModel[]
                {
                    _rootProcedure
                });
        }

        #endregion // Constructors

        #region Public Properties

        public ReadOnlyCollection<ProcedureViewModel> FirstTier
        {
            get { return _firstTier; }
            set
            {
                if (_firstTier == value)
                    return;

                _firstTier = value;

                base.OnPropertyChanged("FirstTier");
            }
        }

        #endregion // Public Properties
    }

    public class ProcedureViewModel : ViewModelBase
    {
        #region Fields

        Procedure _procedure;

        readonly ReadOnlyCollection<ProcedureViewModel> _children;
        readonly ProcedureViewModel _parent;

        bool _isExpanded;
        bool _isSelected;

        #endregion // Fields

        #region Constructors

        public ProcedureViewModel(Procedure procedure)
            : this(procedure, null)
        {
        }

        public ProcedureViewModel(Procedure procedure, ProcedureViewModel parent)
        {
            _procedure = procedure;
            _parent = parent;

            _children = new ReadOnlyCollection<ProcedureViewModel>(
                (from child in _procedure.Children
                 select new ProcedureViewModel(child, this))
                     .ToList<ProcedureViewModel>());
        }

        #endregion // Constructors

        #region Public Properties

        public ReadOnlyCollection<ProcedureViewModel> Children
        {
            get { return _children; }
        }

        public int Count
        {
            get { return _procedure.Count; }
        }

        public bool IsExpanded
        {
            get { return _isExpanded; }
            set
            {
                if (_isExpanded != value)
                {
                    _isExpanded = value;
                    base.OnPropertyChanged("IsExpanded");
                }

                if (_isExpanded && _parent != null)
                    _parent.IsExpanded = true;
            }
        }

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected != value)
                {
                    _isSelected = value;
                    base.OnPropertyChanged("IsSelected");
                }
            }
        }

        public string Name
        {
            get { return _procedure.Name; }
        }

        public ProcedureViewModel Parent
        {
            get { return _parent; }
        }

        #endregion // Public Properties
    }

    public class Procedure
    {
        #region Constructors

        public Procedure(string name)
        {
            this.Name = name;
            this.Children = new List<Procedure>();
        }

        public Procedure(string name, Procedure parent)
        {
            this.Name = name;
            this.Parent = parent;
            this.Children = new List<Procedure>();
        }

        public Procedure(string name, Procedure parent, IEnumerable<Procedure> children)
        {
            this.Name = name;
            this.Parent = parent;
            this.Children = children.ToList();
        }

        #endregion // Constructors

        #region Public Interface

        public List<Procedure> Children { get; set; }

        public int Count { get; set; }

        public string Name { get; set; }

        public Procedure Parent { get; set; }

        #endregion // Public Interface
    }
}

<UserControl x:Class="ProErrors.Client.View.DataRecordView"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:converters="clr-namespace:ProErrors.Client.Converters">

    <UserControl.Resources>
        <converters:StateAbbreviationToFriendlyConverter x:Key="StateAbbreviationToFriendlyConverter" />

        <Style x:Key="DatumStyle">
            <Setter Property="StackPanel.Margin"
                    Value="5" />
        </Style>
        <Style TargetType="Button">
            <Setter Property="FontFamily"
                    Value="Consolas" />
        </Style>
        <Style TargetType="GroupBox">
            <Setter Property="FontFamily"
                    Value="Consolas" />
            <Setter Property="FontWeight"
                    Value="Bold" />
            <Setter Property="BorderBrush"
                    Value="Black" />
            <Setter Property="BorderThickness"
                    Value="2" />
        </Style>
        <Style TargetType="Label">
            <Setter Property="FontFamily"
                    Value="Consolas" />
            <Setter Property="FontWeight"
                    Value="Bold" />
            <Setter Property="Height"
                    Value="25" />
        </Style>
        <Style TargetType="TextBlock">
            <Setter Property="FontFamily"
                    Value="Consolas" />
            <Setter Property="FontWeight"
                    Value="Normal" />
        </Style>
        <Style TargetType="TextBox">
            <Setter Property="FontFamily"
                    Value="Consolas" />
        </Style>
    </UserControl.Resources>

    <Grid>
        <Grid.ColumnDefinitions>
            <ColumnDefinition />
            <ColumnDefinition />
            <ColumnDefinition />
        </Grid.ColumnDefinitions>
        <Grid.RowDefinitions>
            <RowDefinition Height="75" />
            <RowDefinition Height="*" />
            <RowDefinition Height="50" />
        </Grid.RowDefinitions>

        <!--
        User Information
        -->
        <GroupBox Grid.Column="0"
                  Grid.Row="0"
                  Grid.ColumnSpan="3"
                  Header="User">
            <Grid>
                <Grid.ColumnDefinitions>
                    <ColumnDefinition />
                    <ColumnDefinition />
                    <ColumnDefinition />
                    <ColumnDefinition />
                    <ColumnDefinition />
                </Grid.ColumnDefinitions>

                <DockPanel Grid.Column="0"
                           Style="{StaticResource DatumStyle}">
                    <Label DockPanel.Dock="Top"
                           Content="EFIN:" />
                    <TextBlock Text="{Binding Path=Efin}" />
                </DockPanel>

                <DockPanel Grid.Column="1"
                           Style="{StaticResource DatumStyle}">
                    <Label DockPanel.Dock="Top"
                           Content="Individual Version:" />
                    <TextBlock Text="{Binding Path=IVersion}" />
                </DockPanel>

                <DockPanel Grid.Column="2"
                           Style="{StaticResource DatumStyle}">
                    <Label DockPanel.Dock="Top"
                           Content="Business Version:" />
                    <TextBlock Text="{Binding Path=BVersion}" />
                </DockPanel>

                <DockPanel Grid.Column="3"
                           Style="{StaticResource DatumStyle}">
                    <Label DockPanel.Dock="Top"
                           Content=".NET Version:" />
                    <TextBlock Text="{Binding Path=DotNetVersion}" />
                </DockPanel>

                <DockPanel Grid.Column="4"
                           Style="{StaticResource DatumStyle}">
                    <Label DockPanel.Dock="Top"
                           Content="Operating System:" />
                    <TextBlock Text="{Binding Path=OperatingSystem}" />
                </DockPanel>
            </Grid>
        </GroupBox>

        <!--
        Error Information
        -->
        <GroupBox Grid.Column="0"
                  Grid.Row="1"
                  Grid.ColumnSpan="2"
                  Grid.RowSpan="2"
                  Header="Error">
            <Grid>
                <Grid.ColumnDefinitions>
                    <ColumnDefinition />
                    <ColumnDefinition />
                    <ColumnDefinition />
                    <ColumnDefinition />
                </Grid.ColumnDefinitions>
                <Grid.RowDefinitions>
                    <RowDefinition MaxHeight="50" />
                    <RowDefinition MaxHeight="100" />
                    <RowDefinition Height="*" />
                </Grid.RowDefinitions>

                <DockPanel Grid.Column="0"
                           Grid.Row="0"
                           MaxHeight="50"
                           Style="{StaticResource DatumStyle}">
                    <Label DockPanel.Dock="Top"
                           Content="Program:" />
                    <TextBlock Text="{Binding Path=Program}" />
                </DockPanel>

                <DockPanel Grid.Column="1"
                           Grid.Row="0"
                           MaxHeight="50"
                           Style="{StaticResource DatumStyle}">
                    <Label DockPanel.Dock="Top"
                           Content="Error Number:" />
                    <TextBlock Text="{Binding Path=ErrNum}" />
                </DockPanel>

                <DockPanel Grid.Column="2"
                           Grid.Row="0"
                           MaxHeight="50"
                           Style="{StaticResource DatumStyle}">
                    <Label DockPanel.Dock="Top"
                           Content="Line Number:" />
                    <TextBlock Text="{Binding Path=LineNum}" />
                </DockPanel>

                <DockPanel Grid.Column="3"
                           Grid.Row="0"
                           MaxHeight="50"
                           Style="{StaticResource DatumStyle}">
                    <Label DockPanel.Dock="Top"
                           Content="State/Province:" />
                    <TextBlock Text="{Binding Path=StateAbbreviation,Converter={StaticResource StateAbbreviationToFriendlyConverter}}" />
                </DockPanel>

                <DockPanel Grid.Column="0"
                           Grid.Row="1"
                           Grid.ColumnSpan="4"
                           MaxHeight="100"
                           Style="{StaticResource DatumStyle}">
                    <Label DockPanel.Dock="Top"
                           Content="Message:" />
                    <ScrollViewer HorizontalScrollBarVisibility="Auto"
                                  VerticalScrollBarVisibility="Auto">
                        <TextBlock Text="{Binding Path=Message}" />
                    </ScrollViewer>
                </DockPanel>

                <DockPanel Grid.Column="0"
                           Grid.Row="2"
                           Grid.ColumnSpan="4"
                           Style="{StaticResource DatumStyle}">
                    <Label DockPanel.Dock="Top"
                           Content="Call Stack:" />
                    <ScrollViewer HorizontalScrollBarVisibility="Auto"
                                  VerticalScrollBarVisibility="Auto">
                        <TextBlock Text="{Binding Path=Stack}" />
                    </ScrollViewer>
                </DockPanel>
            </Grid>
        </GroupBox>

        <!--
        Solution Information
        -->
        <GroupBox Grid.Column="2"
                  Grid.Row="1"
                  Header="Solution">
            <DockPanel Style="{StaticResource DatumStyle}">
                <Label DockPanel.Dock="Top"
                       Content="Category:" />
                <ComboBox DockPanel.Dock="Top"
                          IsReadOnly="True"
                          ItemsSource="{Binding Path=Categories}"
                          SelectedItem="{Binding Path=Category, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged}"
                          BorderBrush="White" />
                <Label DockPanel.Dock="Top"
                       Content="Detail (Note: The user will read this):" />
                <TextBox AcceptsReturn="True"
                         FontFamily="Courier New"
                         TextWrapping="Wrap"
                         VerticalScrollBarVisibility="Auto"
                         HorizontalContentAlignment="Stretch"
                         Text="{Binding Path=Fix, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged}" />
                <!--Width="{Binding RelativeSource={RelativeSource FindAncestor, AncestorType={x:Type DockPanel}},Path=ActualWidth}"-->
            </DockPanel>
        </GroupBox>

        <!--
        Controls
        -->
        <StackPanel Grid.Column="2"
                    Grid.Row="2"
                    HorizontalAlignment="Center"
                    Orientation="Horizontal"
                    Style="{StaticResource DatumStyle}">
            <Button Content="Mark Fixed"
                    Margin="5"
                    Height="30"
                    Width="90"
                    FontWeight="Bold"
                    IsDefault="True"
                    Command="{Binding Path=DeleteCommand}" />
            <Button Content="Preview"
                    Margin="5"
                    Height="30"
                    Width="90"
                    FontWeight="Bold"
                    Command="{Binding Path=PreviewCommand}" />
            <Button Content="Copy Info"
                    Margin="5"
                    Height="30"
                    Width="90"
                    FontWeight="Bold"
                    Command="{Binding Path=CopyInfoCommand}" />
            <Button Content="Exit"
                    Margin="5"
                    Height="30"
                    Width="90"
                    FontWeight="Bold"
                    IsCancel="True"
                    Command="{Binding Path=CancelCommand}" />
        </StackPanel>

    </Grid>
</UserControl>

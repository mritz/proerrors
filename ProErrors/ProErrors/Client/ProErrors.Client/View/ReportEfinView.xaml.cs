﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ProErrors.Client.ViewModel;

namespace ProErrors.Client.View
{
    /// <summary>
    /// Interaction logic for ReportEfinView.xaml
    /// </summary>
    public partial class ReportEfinView : UserControl
    {
        public ReportEfinView()
        {
            InitializeComponent();
        }

        private void LineSeries_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach (EfinViewModel vm in e.AddedItems)
                efinLineSeries.SelectedItem = vm;
        }

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach (EfinViewModel vm in e.AddedItems)
                efinLineSeries.SelectedItem = vm;
        }
    }
}

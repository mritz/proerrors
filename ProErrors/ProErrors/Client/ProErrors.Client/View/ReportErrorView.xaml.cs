﻿using System;
using System.Windows;
using System.Windows.Controls;
using ProErrors.Client.Library;
using ProErrors.Client.ViewModel;

namespace ProErrors.Client.View
{
    /// <summary>
    /// Interaction logic for ReportErrorView.xaml
    /// </summary>
    public partial class ReportErrorView : UserControl
    {
        public ReportErrorView()
        {
            InitializeComponent();

            Arbiter.Instance.Register(
                (Object o) =>
                {
                    ProcedureViewModel vm = (o as ProcedureViewModel);
                    ProcedureListView.ScrollIntoView(vm);
                }, ViewModelMessage.ScrollToProcedure);
        }

        private void TreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            Arbiter.Instance.NotifyColleagues(
                ViewModelMessage.ChangeProcedureSelection, (e.NewValue as ProcedureViewModel));
        }
    }
}

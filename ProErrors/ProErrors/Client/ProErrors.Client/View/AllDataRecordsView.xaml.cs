﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using ProErrors.Client.Adorners;
using ProErrors.Client.Library;
using ProErrors.Client.ViewModel;

namespace ProErrors.Client.View
{
    /// <summary>
    /// Interaction logic for AllDataRecordsView.xaml
    /// </summary>
    public partial class AllDataRecordsView : UserControl
    {
        GridViewColumnHeader _currentColumn;
        SortAdorner _currentAdorner;

        public AllDataRecordsView()
        {
            InitializeComponent();
        }

        void AutoResizeGridViewColumns(GridView view)
        {
            Expander expander;
            if (view == null || view.Columns.Count < 1) return;
            foreach (var column in view.Columns)
            {
                if (column == view.Columns.Last())
                {
                    expander = (Expander)column.GetValue(ContentProperty);
                }
                if (double.IsNaN(column.Width))
                    column.Width = 1;
                column.Width = double.NaN;
            }
        }

        void Expander_Collapsed(object sender, RoutedEventArgs e)
        {
            var view = ListViewDataRecords.View as GridView;
            AutoResizeGridViewColumns(view as GridView);
        }

        void Expander_Expanded(object sender, RoutedEventArgs e)
        {
            var view = ListViewDataRecords.View as GridView;
            AutoResizeGridViewColumns(view as GridView);
        }

        void GridViewColumnHeader_Click(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader columnHeader = sender as GridViewColumnHeader;
            String field = columnHeader.Tag as String;

            if (_currentColumn != null)
            {
                AdornerLayer.GetAdornerLayer(_currentColumn).Remove(_currentAdorner);
                ListViewDataRecords.Items.SortDescriptions.Clear();
            }

            ListSortDirection newDir = ListSortDirection.Ascending;
            if (_currentColumn == columnHeader && _currentAdorner.Direction == newDir)
                newDir = ListSortDirection.Descending;

            _currentColumn = columnHeader;
            _currentAdorner = new SortAdorner(_currentColumn, newDir);
            AdornerLayer.GetAdornerLayer(_currentColumn).Add(_currentAdorner);
            ListViewDataRecords.Items.SortDescriptions.Add(new SortDescription(field, newDir));
        }

        private void ListViewDataRecords_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach (DataRecordViewModel item in e.RemovedItems)
                item.IsSelected = false;

            foreach (DataRecordViewModel item in e.AddedItems)
                item.IsSelected = true;
        }

        private void ListViewItem_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            Arbiter.Instance.NotifyColleagues(
                ViewModelMessage.ViewDataRecordViewModel,
                null);
        }
    }
}

﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using ProErrors.Client.ViewModel;
using System.Diagnostics;
using ProErrors.Core;
using System.Windows.Threading;
using System.IO;
using System.Threading;

namespace ProErrors.Client
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        MainWindow _view;
        MainWindowViewModel _viewModel;
        Timer _timer;

        int _currentYear;
        string _searchString;

        public App()
            : base()
        {
            AppDomain.CurrentDomain.UnhandledException +=
                new UnhandledExceptionEventHandler(
                    CurrentDomain_UnhandledException);

            Application.Current.DispatcherUnhandledException +=
                new DispatcherUnhandledExceptionEventHandler(
                    Current_DispatcherUnhandledException);

            this.Dispatcher.UnhandledException +=
                new DispatcherUnhandledExceptionEventHandler(
                    Dispatcher_UnhandledException);

            _timer = new Timer(CreateTrace, false,
                DateTime.Now.AddDays(1).Date - DateTime.Now,
                new TimeSpan(1, 0, 0, 0));
        }

        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Trace.TraceError(DateTime.Now.ToString() + ": " + e.ExceptionObject.ToString());
        }

        void Current_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            Trace.TraceError(DateTime.Now.ToString() + ": " + e.Exception.ToString());
        }

        void Dispatcher_UnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            Trace.TraceError(DateTime.Now.ToString() + ": " + e.Exception.ToString());
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            this.CreateTrace(false);

            if (e.Args != null)
            {
                if (e.Args.Count() > 0)
                    Int32.TryParse(e.Args[0], out _currentYear);

                if (e.Args.Count() > 1)
                    _searchString = e.Args[1].ToString();
            }

            base.OnStartup(e);

            _viewModel = new MainWindowViewModel();
            _view = new MainWindow();

            EventHandler handler = null;
            handler = delegate
            {
                _viewModel.RequestClose -= handler;
                _view.Close();
            };
            _viewModel.RequestClose += handler;

            _view.DataContext = _viewModel;
            _view.InputBindings.Add(new KeyBinding(_viewModel.ClearCommand, new KeyGesture(Key.F5)));
            _view.InputBindings.Add(new KeyBinding(_viewModel.CopyInfoCommand, new KeyGesture(Key.C, ModifierKeys.Control)));
            _view.InputBindings.Add(new KeyBinding(_viewModel.DeleteCommand, new KeyGesture(Key.Delete)));
            _view.InputBindings.Add(new KeyBinding(_viewModel.DeleteFixedCommand, new KeyGesture(Key.Delete, ModifierKeys.Control)));
            _view.InputBindings.Add(new KeyBinding(_viewModel.PrintCommand, new KeyGesture(Key.P, ModifierKeys.Control)));
            _view.InputBindings.Add(new KeyBinding(_viewModel.ShowErrorsCommand, new KeyGesture(Key.E, ModifierKeys.Control)));
            _view.InputBindings.Add(new KeyBinding(_viewModel.ShowSolutionsCommand, new KeyGesture(Key.S, ModifierKeys.Control)));
            _view.InputBindings.Add(new KeyBinding(_viewModel.ReportEfinCommand, new KeyGesture(Key.F9)));
            _view.InputBindings.Add(new KeyBinding(_viewModel.ReportErrorCommand, new KeyGesture(Key.F10)));
            _view.InputBindings.Add(new KeyBinding(_viewModel.ReportTimeFrameCommand, new KeyGesture(Key.F11)));
            //_view.InputBindings.Add(new KeyBinding(_viewModel.ReportYearlyTrendCommand, new KeyGesture(Key.F12)));
            _view.Show();

            if (_viewModel.TaxYears.Contains(_currentYear))
            {
                _viewModel.TaxYear = _currentYear;
                if (_viewModel.ShowErrorsCommand.CanExecute(null))
                    _viewModel.ShowErrorsCommand.Execute(this);
            }

            if (_searchString != null)
            {
                _viewModel.SearchString = _searchString;

                if (_viewModel.SearchCommand.CanExecute(null))
                    _viewModel.SearchCommand.Execute(this);
            }
        }

        protected override void OnExit(ExitEventArgs e)
        {
            _viewModel.Dispose();

            base.OnExit(e);
        }

        void CreateTrace(Object stateInfo)
        {
            Trace.Listeners.Clear();

            string root = Config.PRO_ERRORS_EXE_ROOT + @"Trace\";
            string trace = root +
                DateTime.Now.Year.ToString() + @"\" +
                DateTime.Now.Month.ToString() + @"\" +
                DateTime.Now.Day.ToString() + @"\" +
                Config.User + ".log";

            if (!Directory.Exists(Path.GetDirectoryName(trace)))
                Directory.CreateDirectory(Path.GetDirectoryName(trace));

            if (!File.Exists(trace))
                File.Create(trace);

            Trace.Listeners.Add(new TextWriterTraceListener(trace, "Listener"));
            Trace.AutoFlush = true;

            string[] Years = Directory.GetDirectories(root);
            for (int y = Years.Count() - 1; y >= 0; y--)
            {
                string[] Months = Directory.GetDirectories(Years[y]);
                for (int m = Months.Count() - 1; m >= 0; m--)
                {
                    string[] Days = Directory.GetDirectories(Months[m]);
                    for (int d = Days.Count() - 1; d >= 0; d--)
                    {
                        string[] Files = Directory.GetFiles(Days[d]);
                        for (int f = Files.Count() - 1; f >= 0; f--)
                        {
                            if (DateTime.Now - new FileInfo(Files[f]).LastAccessTime > new TimeSpan(14, 0, 0, 0))
                                File.Delete(Files[f]);
                        }
                        if (Directory.GetFiles(Days[d]).Count() == 0)
                            Directory.Delete(Days[d]);
                    }
                    if (Directory.GetDirectories(Months[m]).Count() == 0)
                        Directory.Delete(Months[m]);
                }
                if (Directory.GetDirectories(Years[y]).Count() == 0)
                    Directory.Delete(Years[y]);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using ProErrors.Core;

namespace ProErrors.Client.Converters
{
    public sealed class IsUserAdminConverter : IValueConverter
    {
        #region Constructor

        public IsUserAdminConverter()
        {
        }

        #endregion // Constructor

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string user = (string)value;

            if (Config.IsUserAdministrator())
                return System.Windows.Visibility.Visible;
            else
                return System.Windows.Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Globalization;

namespace ProErrors.Client.Converters
{
    public sealed class ForegroundConverter : IValueConverter
    {
        #region Constructors

        public ForegroundConverter()
        {
        }

        #endregion // Constructors

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool alreadyFixed = (bool)System.Convert.ChangeType(value, typeof(bool));

            return alreadyFixed;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }

        #endregion // IValueConverter Members
    }
}

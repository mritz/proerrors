﻿using System;
using System.Windows.Data;
using ProErrors.Client.Proxies;

namespace ProErrors.Client.Converters
{
    public class StateAbbreviationToFriendlyConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return null;

            UnitedState unitedState = (UnitedState)(int)(UnitedStateAbbreviation)value;
            string state = unitedState.ToString();
            int index = 1;

            while (index < state.Length)
            {
                if (Char.IsUpper(state, index))
                {
                    state = state.Insert(index, " ");
                    index++;
                }
                index++;
            }

            return state;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return null;

            string[] words = (value as string).Split(new char[] { ' ' });
            value = String.Empty;

            foreach (string word in words)
                value += word;

            return (UnitedStateAbbreviation)(int)(UnitedState)value;
        }
    }

    public enum UnitedState
    {
        Alabama,
        Alaska,
        Arizona,
        Arkansas,
        California,
        Colorado,
        Connecticut,
        Delaware,
        DistrictOfColumbia,
        Florida,
        Georgia,
        Hawaii,
        Idaho,
        Illinois,
        Indiana,
        Iowa,
        Kansas,
        Kentucky,
        Louisiana,
        Maine,
        Maryland,
        Massachusetts,
        Michigan,
        Minnesota,
        Mississippi,
        Missouri,
        Montana,
        Nebraska,
        Nevada,
        NewHampshire,
        NewJersey,
        NewMexico,
        NewYork,
        NorthCarolina,
        NorthDakota,
        Ohio,
        Oklahoma,
        Oregon,
        Pennsylvania,
        PuertoRico,
        RhodeIsland,
        SouthCarolina,
        SouthDakota,
        Tennessee,
        Texas,
        Utah,
        Vermont,
        Virginia,
        Washington,
        WestVirginia,
        Wisconsin,
        Wyoming,
    }
}

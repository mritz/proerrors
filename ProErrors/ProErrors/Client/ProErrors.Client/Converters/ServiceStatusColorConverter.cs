﻿using System;
using System.Windows.Data;
using System.Windows.Media;
using ProErrors.Client.Proxies;

namespace ProErrors.Client.Converters
{
    public class ServiceStatusColorConverter : IValueConverter
    {
        #region Constructors

        public ServiceStatusColorConverter()
        {
        }

        #endregion // Constructors

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            switch ((RepositoryStatus)value)
            {
                case RepositoryStatus.Busy:
                    return Brushes.Red;
                case RepositoryStatus.Idle:
                    return Brushes.Green;
                case RepositoryStatus.Initializing:
                case RepositoryStatus.Reconciling:
                case RepositoryStatus.Resetting:
                    return Brushes.Blue;
                case RepositoryStatus.ReceivingError:
                case RepositoryStatus.ReceivingSolution:
                    return Brushes.LimeGreen;
                default:
                    return Brushes.Black;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}

﻿using System;
using System.Globalization;
using System.ServiceModel;
using System.Windows.Data;
using System.Windows.Media;

namespace ProErrors.Client.Converters
{
    /// <summary>
    /// Converts between CommunicationState and Color.
    /// </summary>
    public class ConnectionStatusColorConverter : IValueConverter
    {
        #region Constructors

        /// <summary>
        /// Creates an instance of the class.
        /// </summary>
        public ConnectionStatusColorConverter()
        {
        }

        #endregion // Constructors

        #region IValueConverter Members

        /// <summary>
        /// Converts a CommunicationState to a Color.
        /// </summary>
        /// <param name="value">The CommunicationState</param>
        /// <param name="targetType">Unused.</param>
        /// <param name="parameter">Unused.</param>
        /// <param name="culture">Unused.</param>
        /// <returns>The converted Color.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((CommunicationState)value)
            {
                case CommunicationState.Created:
                    return Brushes.Blue;
                case CommunicationState.Opening:
                    return Brushes.LightGreen;
                case CommunicationState.Opened:
                    return Brushes.Green;
                case CommunicationState.Closing:
                    return Brushes.Yellow;
                case CommunicationState.Closed:
                    return Brushes.Red;
                case CommunicationState.Faulted:
                    return Brushes.Red;
                default:
                    return Brushes.Black;
            }
        }

        /// <summary>
        /// Converts a Color to a CommunicationState (Not implemented).
        /// </summary>
        /// <param name="value">Unused.</param>
        /// <param name="targetType">Unused.</param>
        /// <param name="parameter">Unused.</param>
        /// <param name="culture">Unused.</param>
        /// <returns>Unused.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace ProErrors.Client.Converters
{
    public class ServiceStatusConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Stack<int> indices = new Stack<int>();
            string status = value.ToString();

            for (int i = 0; i < status.Length; i++)
                if (Char.IsUpper(status[i]))
                    indices.Push(i);

            while (true)
            {
                status = status.Insert(indices.Pop(), " ");
                if (indices.Count == 0)
                    break;
            }

            return status;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
            //string status = (String)value;
            //string[] Parts = status.Split(new char[] { ' ' });
            //status = String.Empty;

            //foreach (string part in Parts)
            //    status += part;

            //return status;
        }
    }
}

﻿using System;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

namespace ProErrors.Client.Converters
{
    public sealed class BackgroundConverter : IValueConverter
    {
        #region Constructor

        public BackgroundConverter()
        {
        }

        #endregion // Constructor

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            ListViewItem item = (ListViewItem)value;
            ListView listView = ItemsControl.ItemsControlFromItemContainer(item) as ListView;

            int index = listView.ItemContainerGenerator.IndexFromContainer(item);

            if (index % 2 == 0)
                return Brushes.LightCyan;
            else
                return Brushes.LightBlue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }

        #endregion // IValueConverter Members
    }
}

﻿using System;
using System.IO;
using System.Reflection;
using ProErrors.Core;

namespace ProErrors.Client.AssemblyInspector
{
    class Program
    {
        static void Main(string[] args)
        {
            string root = Config.PRO_ERRORS_EXE_ROOT + @"ProErrors.NET\";
            string assemblyFilename = root + "ProErrors.Client.exe";

            if (assemblyFilename != null && File.Exists(assemblyFilename))
            {
                try
                {
                    var assembly = Assembly.ReflectionOnlyLoadFrom(assemblyFilename);
                    var name = assembly.GetName();

                    DirectoryInfo dirInfo = Directory.CreateDirectory(root + name.Version);

                    string[] Files = Directory.GetFiles(root, "*.*", SearchOption.TopDirectoryOnly);

                    foreach (string file in Files)
                        File.Move(file, dirInfo.FullName + file.Substring(file.LastIndexOf(@"\")));

                    string[] Directories = Directory.GetDirectories(root);

                    foreach (string directory in Directories)
                        if (directory != dirInfo.FullName)
                            RemoveIfUnused(directory);
                }
                catch (Exception) { throw; }
            }
        }

        static void RemoveIfUnused(string directory)
        {
            string[] Files = Directory.GetFiles(directory);
            bool delete = true;

            foreach (string file in Files)
                if (IsFileLocked(new FileInfo(file)))
                    delete = false;

            if (delete == true)
                Directory.Delete(directory, true);
        }

        static bool IsFileLocked(FileInfo fileInfo)
        {
            FileStream stream = null;

            try
            {
                stream = fileInfo.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            return false;
        }
    }
}

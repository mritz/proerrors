using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using ProErrors.Core;

namespace ProErrors.Client.Launcher
{
    class Program
    {
        static string _arguments = String.Empty;

        static void Main(string[] args)
        {
            foreach (string arg in args)
                _arguments += arg + " ";

            string root = Config.PRO_ERRORS_EXE_ROOT + @"ProErrors.NET\";

            string[] Directories = Directory.GetDirectories(root);

            if (Directories.Count() == 0)
                return;
            else if (Directories.Count() == 1)
                LaunchProErrors(Directories[0]);
            else
                LaunchProErrors(GetLatestVersion(Directories));
        }

        static string GetLatestVersion(string[] Directories)
        {
            List<string> versions = new List<string>(Directories);

            int[] versionSegments = { 0, 0, 0, 0 };

            for (int y = 0; y <= 3; y++)
            {
                foreach (string version in versions)
                    versionSegments[y] = Math.Max(versionSegments[y], Convert.ToInt32(version.Substring(version.LastIndexOf(@"\") + 1).Split('.')[y]));

                for (int x = versions.Count - 1; x >= 0; x--)
                    if (Convert.ToInt32(versions[x].Substring(versions[x].LastIndexOf(@"\") + 1).Split('.')[y]) != versionSegments[y])
                        versions.RemoveAt(x);

                if (versions.Count == 1)
                    break;
            }

            return versions[0];
        }

        static void LaunchProErrors(string directory)
        {
            try
            {
                Process proErrors = new Process();
                proErrors.StartInfo.FileName = directory + @"\ProErrors.Client.exe";
                proErrors.StartInfo.Arguments = _arguments.TrimEnd();
                proErrors.Start();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}

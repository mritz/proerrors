using System.Linq;
using System.Security.Principal;

namespace ProErrors.Core
{
    public static class Config
    {
        public const string DATARECORD_VALIDATION_ERROR = "Invalid Name";
        public const string DIRECTORY_SEARCH_PATTERN = "ICtrl*";
        public const int MAX_YEARS = 1;
        public const string PRO_ERRORS_DISPLAY_NAME = "Pro Errors";
#if DEBUG || TEST
        public const string PRO_ERRORS_EXE_ROOT = @"\\rfscorp\shares\TaxSlayerPro\Home\PROERRORS\Test\";
        public const string PRO_ERRORS_ROOT = @"\\rfscorp\shares\TaxSlayerPro\efiledata\Test\";
#else
        public const string PRO_ERRORS_EXE_ROOT = @"\\rfscorp\shares\TaxSlayerPro\Home\PROERRORS\";
        public const string PRO_ERRORS_ROOT = @"\\rfscorp\shares\TaxSlayerPro\efiledata\";
#endif
        public const int REPOSITORY_REFRESH = 14400000;
        public const string XML_SEARCH_PATTERN = "errlog*.xml";

        private static readonly string[] _administrators =
            new string[]
            {
                "cwallace",
                "jeff",
                "jmorrison",
                "kwanamaker",
                "mritz",
                "nathan",
                "nwanamaker",
                "wrhyner",
            };
        private static readonly string[] _programmers =
            new string[]
            {
                "chriss",
                "csavage",
                "ctressler",
                "david",
                "dpemberton",
                "deirdre",
                "dknight",
                "jeff",
                "jmorrison",
                "jstewart",
                "mlariscy",
                "mritz",
                "nathan",
                "nrusch",
                "nwanamaker",
                "trish",
                "tstarcher",
            };
        private static string _user;

        static Config()
        {
        }

        public static string User
        {
            get
            {
                if (_user == null)
                {
                    _user = WindowsIdentity.GetCurrent().Name.ToString();
                    if (_user.Contains("\\"))
                        _user = _user.Substring(_user.LastIndexOf("\\") + 1).ToLower();
                }

                return _user;
            }
        }
        public static bool IsUserAdministrator()
        {
            return _administrators.Contains(User);
        }
        public static bool IsUserProgrammer()
        {
            return _programmers.Contains(User);
        }
    }
}
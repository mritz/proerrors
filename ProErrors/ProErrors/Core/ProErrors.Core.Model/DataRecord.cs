﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ErrorHandling.Data;

namespace ProErrors.Core.Model
{
    [DataContract(Namespace = "http://ProErrors/services/2011/04")]
    public enum DataRecordType
    {
        [EnumMember]
        Error,
        [EnumMember]
        Solution,
        [EnumMember]
        Support,
    }

    [DataContract(Namespace = "http://ProErrors/services/2011/04")]
    public enum UnitedStateAbbreviation
    {
        [EnumMember]
        AL,
        [EnumMember]
        AK,
        [EnumMember]
        AZ,
        [EnumMember]
        AR,
        [EnumMember]
        CA,
        [EnumMember]
        CO,
        [EnumMember]
        CT,
        [EnumMember]
        DE,
        [EnumMember]
        DC,
        [EnumMember]
        FL,
        [EnumMember]
        GA,
        [EnumMember]
        HI,
        [EnumMember]
        ID,
        [EnumMember]
        IL,
        [EnumMember]
        IN,
        [EnumMember]
        IA,
        [EnumMember]
        KS,
        [EnumMember]
        KY,
        [EnumMember]
        LA,
        [EnumMember]
        ME,
        [EnumMember]
        MD,
        [EnumMember]
        MA,
        [EnumMember]
        MI,
        [EnumMember]
        MN,
        [EnumMember]
        MS,
        [EnumMember]
        MO,
        [EnumMember]
        MT,
        [EnumMember]
        NE,
        [EnumMember]
        NV,
        [EnumMember]
        NH,
        [EnumMember]
        NJ,
        [EnumMember]
        NM,
        [EnumMember]
        NY,
        [EnumMember]
        NC,
        [EnumMember]
        ND,
        [EnumMember]
        OH,
        [EnumMember]
        OK,
        [EnumMember]
        OR,
        [EnumMember]
        PA,
        [EnumMember]
        PR,
        [EnumMember]
        RI,
        [EnumMember]
        SC,
        [EnumMember]
        SD,
        [EnumMember]
        TN,
        [EnumMember]
        TX,
        [EnumMember]
        UT,
        [EnumMember]
        VT,
        [EnumMember]
        VA,
        [EnumMember]
        WA,
        [EnumMember]
        WV,
        [EnumMember]
        WI,
        [EnumMember]
        WY,
    }

    [DataContract(Namespace = "http://ProErrors/services/2011/04")]
    public class DataRecord
    {
        private static readonly string[] OLE_ERRORS = new string[] { "1426", "1427", "1428", "1429" };

        protected DataRecord()
        {
        }

        [DataMember]
        public string BVersion { get; set; }
        [DataMember]
        public string Category { get; set; }
        [DataMember]
        public double DotNetVersion { get; set; }
        [DataMember]
        public string Efin { get; set; }
        [DataMember]
        public string ErrNum { get; set; }
        [DataMember]
        public string FileName { get; set; }
        [DataMember]
        public string Fix { get; set; }
        [DataMember]
        public string Fixer { get; set; }
        [DataMember]
        public Guid ID { get; set; }
        [DataMember]
        public bool IsFixed { get; set; }
        [DataMember]
        public string IVersion { get; set; }
        [DataMember]
        public string LineNum { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public string OperatingSystem { get; set; }
        [DataMember]
        public string Program { get; set; }
        [DataMember]
        public string Stack { get; set; }
        [DataMember]
        public ExceptionData ExceptionData { get; set; }
        [DataMember]
        public Version ClrVersion { get; set; }
        [DataMember]
        public string ClientNotes { get; set; }
        [DataMember]
        public Nullable<UnitedStateAbbreviation> StateAbbreviation { get; set; }
        [DataMember]
        public DateTime TimeStamp { get; set; }
        [DataMember]
        public DataRecordType Type { get; set; }
        [DataMember]
        public bool UserInformed { get; set; }

        public static DataRecord CreateDataRecord(Guid id, DateTime timeStamp, string efin,
                    string iVersion, string bVersion, string program, string errNum, string lineNum,
                    string message, string stack, string opSys, double dotNetVer, Nullable<UnitedStateAbbreviation> state)
        {
            return new DataRecord
            {
                ID = id,
                TimeStamp = timeStamp,
                Efin = efin,
                IVersion = iVersion,
                BVersion = bVersion,
                Program = program,
                ErrNum = errNum,
                LineNum = lineNum,
                Message = message,
                Stack = stack,
                OperatingSystem = opSys,
                DotNetVersion = dotNetVer,
                StateAbbreviation = state,
            };
        }
        public static DataRecord CreateDataRecord(Guid id, DateTime timeStamp, string efin,
                    string iVersion, string bVersion, string program, string errNum, string lineNum,
                    string message, string stack, string opSys, double dotNetVer, Nullable<UnitedStateAbbreviation> state,
                    string category, string fix, string user)
        {
            return new DataRecord
            {
                ID = id,
                TimeStamp = timeStamp,
                Efin = efin,
                IVersion = iVersion,
                BVersion = bVersion,
                Program = program,
                ErrNum = errNum,
                LineNum = lineNum,
                Message = message,
                Stack = stack,
                OperatingSystem = opSys,
                DotNetVersion = dotNetVer,
                StateAbbreviation = state,
                Category = category,
                Fix = fix,
                Fixer = user,
            };
        }
        public static DataRecord CreateNewDataRecord()
        {
            return new DataRecord();
        }
        public static string RemoveUniqueData(string message, string error)
        {
            // Items within quotes in these types of errors are not unique to the user:
            // 12   :   Variable 'VARIABLE' is not found.
            // 13   :   Alias 'ALIAS' is not found.
            if (error == "12" || error == "13")
                return message;

            StringBuilder sb = new StringBuilder();
            bool IsInQuotes = false;
            char QuoteType = new char();

            foreach (char o in message)
            {
                // Beginning of Unique Data.
                if ((o == '\'' || o == '\"') && !IsInQuotes)
                {
                    sb.Append(o);
                    sb.Append("UNIQUE-DATA");
                    QuoteType = o;
                    IsInQuotes = true;
                    continue;
                }

                // Anything outside of quotes.
                if (!IsInQuotes)
                    sb.Append(o);

                // End of Unique Data.
                if (o == QuoteType && IsInQuotes)
                {
                    sb.Append(o);
                    QuoteType = new char();
                    IsInQuotes = false;
                }
            }

            return sb.ToString();
        }

        public bool IsEquivalent(DataRecord dataRecord)
        {
            if ((this as object) == null &&
                (dataRecord as object) == null)
                return true;

            if ((this as object) == null ||
                (dataRecord as object) == null)
                return false;

            // Must be equal for equivalence.
            if (this.BVersion != dataRecord.BVersion ||
                this.IVersion != dataRecord.IVersion ||
                this.ErrNum != dataRecord.ErrNum ||
                this.Program != dataRecord.Program ||
                RemoveUniqueData(this.Message, this.ErrNum) != RemoveUniqueData(dataRecord.Message, this.ErrNum) ||
                this.OperatingSystem != dataRecord.OperatingSystem ||
                this.DotNetVersion != dataRecord.DotNetVersion)
                return false;

            // Special cases.
            // OLE Errors.
            if (OLE_ERRORS.Contains(this.ErrNum) && this.ErrNum == dataRecord.ErrNum)
            {
                return true;
            }
            else
            {
                if (this.LineNum != dataRecord.LineNum ||
                    this.Stack.Replace("\r\n", "") != dataRecord.Stack.Replace("\r\n", ""))
                    return false;
            }

            return true;
        }
    }
}